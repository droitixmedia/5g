<?php

namespace openjobs;

use Illuminate\Database\Eloquent\Model;

class Testimony extends Model
{
       public function user()
    {
        return $this->belongsTo(User::class);
    }
    
}

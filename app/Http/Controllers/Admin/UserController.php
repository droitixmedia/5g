<?php

namespace openjobs\Http\Controllers\Admin;

use Illuminate\Http\Request;
use openjobs\User;
use openjobs\Http\Controllers\Controller;


class UserController extends Controller
{
    public function index()
    {
        $registeredusers = User::count();
        $users = \openjobs\User::orderBy('created_at', 'desc')->paginate(100);
        return view('admin.users.index')->with(compact('users','registeredusers'));
    }

    public function agents()
    
    {
       
        $agents = \openjobs\User::where('level_id', '=', 2)->orderBy('created_at', 'desc')->paginate(100);
        return view('admin.users.agents')->with(compact('agents'));
    }

     public function members()
    {
       
        $members = \openjobs\User::where('level_id', '=', 1)->orderBy('created_at', 'desc')->paginate(100);
        return view('admin.users.members')->with(compact('members'));
    }
      public function supervisors()
    {
       
        $supervisors = \openjobs\User::where('level_id', '=', 3)->orderBy('created_at', 'desc')->paginate(100);
        return view('admin.users.supervisors')->with(compact('supervisors'));
    }

     public function cordinators()
     {
       
        $cordinators = \openjobs\User::where('level_id', '=', 4)->orderBy('created_at', 'desc')->paginate(100);
        return view('admin.users.cordinators')->with(compact('cordinators'));
    }
     public function managers()
    {
       
        $managers = \openjobs\User::where('level_id', '=', 5)->orderBy('created_at', 'desc')->paginate(100);
        return view('admin.users.managers')->with(compact('managers'));
    }

     public function ambassadors()
    {
       
        $ambassadors = \openjobs\User::where('level_id', '=', 6)->orderBy('created_at', 'desc')->paginate(100);
        return view('admin.users.ambassadors')->with(compact('ambassadors'));
    }

    public function directors()

     {
       
        $directors = \openjobs\User::where('level_id', '=', 7)->orderBy('created_at', 'desc')->paginate(100);
        return view('admin.users.directors')->with(compact('directors'));
    }

     public function destroyUser($id)
    {
        User::where('id', $id)->delete();
        session()->flash('message', 'User deleted!');
        notify()->success('User Deleted');
        return redirect()->back();
    }
}

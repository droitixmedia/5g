<?php

namespace openjobs\Http\Controllers;

use Illuminate\Http\Request;

class StaticPagesController extends Controller

{

   public function smega(){
        return view('smega');
    }

    public function innbucks(){
        return view('innbucks');
    }

    public function orange(){
        return view('orange');
    }

    public function fnbbotswana(){
        return view('fnbbotswana');
    }

    public function ecocash(){
        return view('ecocash');
    }

    public function mukuru(){
        return view('mukuru');
    }

    public function fnb(){
        return view('fnb');
    }

    public function capitec(){
        return view('capitec');
    }

     public function airtel(){
        return view('airtel');
    }

     public function mtn(){
        return view('mtn');
    }






     public function companyreg(){
        return view('auth.companyreg');
    }

    public function bonus(){
        return view('pages.bonus');
    }
    
    public function usermanagement(){
        return view('pages.user_manage');
    }

    public function ordermanagement(){
        return view('pages.order_manage');
    }

      public function boost(){
        return view('pages.boost');
    }

     public function affiliate(){
        return view('pages.affiliate');
    }

      public function pay(){
        return view('pay');
    }

     public function blog(){
        return view('pages.blog1');
    }
       public function blog2(){
        return view('pages.blog2');
    }

       public function blog3(){
        return view('pages.blog3');
    }

     public function paypal(){
        return view('paypal');
    }

     public function bitcoin(){
        return view('bitcoin');
    }

     public function skrill(){
        return view('skrill');
    }

     public function bank(){
        return view('bank');
    }

      public function taken(){
        return view('taken');
    }
    public function about(){
        return view('pages.about');
    }

     public function cvwriting(){
        return view('pages.cvwriting');
    }
    public function bookacv(){
        return view('pages.bookacv');
    }
    public function terms(){
        return view('pages.terms');
    }

    public function privacy(){
        return view('pages.privacy');
    }

     public function faq(){
        return view('pages.faq');
    }

     public function test(){
        return view('pages.test');
    }




    public function contact(){
        return view('pages.contact');
    }



}

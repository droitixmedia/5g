<?php

namespace openjobs\Http\Controllers;

use Illuminate\Http\Request;
use openjobs\Jobs\UserViewedResume;
use openjobs\User;
use openjobs\Resume;
use openjobs\Area;
use openjobs\File;
use Auth;
use Image;

class UserController extends Controller
{


public function getProfile(Request $request, Area $area, Resume $resume,File $file, $email)
{
          $user = User::where('email', $email)->first();
          if (!$user) {
            abort(404);
          }

  $resumes = $user->resumes()->isLive()->latestFirst()->paginate(10);
  $files = $user->files()->paginate(10);


          return view('profile.index', compact('resumes','files','file','resume','user', $user));
}


    public function profile()
  {
    return view('profile.profile', array('user' => Auth::user()));
  }


   public function update_avatar(Request $request)
  {
      if($request->hasFile('avatar')){
        $avatar = $request->file('avatar');
        $filename = time() . '.' . $avatar->getClientOriginalExtension();
        Image::make($avatar)->resize(300, 300)->save( public_path('/uploads/avatars/' .$filename) );

        $user = Auth::user();
        $user->avatar = $filename;
        $user->save();
      }
      notify()->success('Profile picture changed!');
      return redirect()->back()->withInput();
  }
    public function update(Request $request)
  {
    $request = $request->validate([
      'email' => 'required',
      'name' => '',
      'surname' => '',
      'phone' => '',
      'bank' => '',
      'branch' => '',
      
      'account' => '',
      'accounttype' => '',
      'level_id' => '',
   
      
      'phone_number' => '',
      'paypal' => '',
      'bitcoin' => '',
      'mobile_wallet' => '',
      'mobile_wallet_number' => '',
      'skrill' => '',
   




    ]);

    $saved = false;
    $user = auth()->user();
    $user->name = $request['name'];
    $user->surname = $request['surname'];
    $user->phone = $request['phone'];
    $user->branch = $request['branch'];
    $user->phone_number = $request['phone_number'];
    $user->email = $request['email'];
    $user->bank = $request['bank'];
    $user->account = $request['account'];
   
    $user->accounttype = $request['accounttype'];

    $user->paypal = $request['paypal'];
    $user->bitcoin = $request['bitcoin'];
    $user->mobile_wallet = $request['mobile_wallet'];
    $user->mobile_wallet_number = $request['mobile_wallet_number'];
    $user->skrill = $request['skrill'];
    $user->level_id = $request['level_id'];

  



    if($user->save()) $saved = true;

    notify()->success('Information edited succesifully!');

    return view('profile.profile', compact('saved', 'user'));
  }

}

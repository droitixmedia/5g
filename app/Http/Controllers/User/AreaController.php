<?php

namespace openjobs\Http\Controllers\User;

use openjobs\Area;
use Illuminate\Http\Request;
use openjobs\Http\Controllers\Controller;

class AreaController extends Controller
{
     public function store(Area $area)
    {
        session()->put('area', $area->slug);

        return redirect()->route('category.index', [$area]);
    }
}

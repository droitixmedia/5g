<?php

namespace openjobs\Http\Controllers;


use openjobs\Testimony;
use Illuminate\Http\Request;
use openjobs\Http\Requests\StoreTestimonyFormRequest;
use Auth;

class TestimonyController extends Controller
{
      public function testify()
    {
        return view('testimony');
    }


    public function store(StoreTestimonyFormRequest $request)
    {
        $testimony = new Testimony;
        $testimony->body = $request->body;
       
     

        $testimony->user()->associate($request->user());


     
        $testimony->created_at = \Carbon\Carbon::now();
        $testimony->save();


        return redirect()
            ->route('testimony.published.index', $testimony)
            ->with('success', 'Testimony Saved!');
    }

     public function list()
    {
        $testimonies = Testimony::with(['user'])->latestFirst();

      

        return view('home', compact('testimonies'));
    }

     public function index(Request $request)
    {

        $user=Auth::user();
        $testimonies = $request->user()->testimonies()->paginate(10);


        return view('view_testimonies', compact('testimonies','user'));
    }


}

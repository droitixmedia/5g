@extends('layouts.app')

@section('title', 'Dashboard')

@section('description')

@endsection

@section('content')

<div class="content-box">
                     
                    
                     <div class="element-wrapper">
                        <h6 class="element-header">Your Downliners</h6>
                        <div class="element-box-tp">
                           <div class="table-responsive">
                              <table class="table table-padded">
                                 <thead>
                                    <tr>
                                       <th>Name</th>
                                       <th class="text-center">Level</th>
                                       <th>Country</th>
                                     
                                       <th>Contact</th>
                                       
                                      
                                       <th>Current Package</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    @foreach ($referrals as $ref)
                                    <tr>
                                      
                                       <td class="cell-with-media"> <a href="{{route('profile.index', ['email'=>$ref->email])}}"><img alt="" src="/uploads/avatars/{{ $ref->avatar }}" style="height: 25px;"><span>{{ $ref->name }} {{ $ref->surname }}</span></a></td>
                                       <td class="text-center"><a class="badge badge-success" style="background-color: {{$ref->level->color}}" href="#">{{$ref->level->name}}</a></td>
                                       <td><span>{{$ref->area->parent->name}}</span><span class="smaller lighter"></span> <span class="flag-icon flag-icon-{{$ref->area->icon}}"></span></td>
                                      
                                       <td><span>{{$ref->phone_number}}</span><span class="smaller lighter"></span></td>
                                       
                                       
                                       
                                       @foreach($ref->listings as $listing)
                                         @if($listing->type())
                                         @if($listing->matched())
                                         <td class="cell-with-media"> <img alt="" src="/assets/images/badges/{{$listing->category->parent->icon}}.png" style="height: 25px;"><span>{{ $listing->category->name }} </span></td>

                                         @endif

                                         @endif
                                          
                                       
                                 
                                         @endforeach
                                    </tr>
                                    @endforeach
                                 </tbody>
                              </table>
                           </div>
                        </div>
                     </div>
                     
                  </div>
@endsection

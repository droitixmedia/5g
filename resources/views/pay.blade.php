@extends('layouts.app')

@section('title', 'Dashboard')

@section('description')

@endsection

@section('content')

<div class="content-box">
                     
                    
                     <div class="element-wrapper">
                        <div class="element-box">
                           <h5 class="form-header">Select Your Payment Method</h5>
                           <div class="form-desc">Make sure you follow all the guidlines of the method you select</div>
                     
                                <div class="row">
                        <div class="col-sm-8">
                           <div class="element-wrapper compact pt-4">
                              
                              <h6 class="element-header">Deposit Money Through</h6>
                              <div class="element-box-tp">
                                 <div class="inline-profile-tiles">
                                    <div class="row">
                                       
                                       <div class="col-4 col-sm-3 col-xxl-2">
                                          <div class="profile-tile profile-tile-inlined">
                                             <a class="profile-tile-box" href="{{url('paypal')}}">
                                                <div class="pt-avatar-w"><img alt="" src="/img/method/Paypal.png"></div>
                                                <div class="pt-user-name">Paypal<br> </div>
                                             </a>
                                          </div>
                                       </div>
                                       <div class="col-4 col-sm-3 col-xxl-2">
                                          <div class="profile-tile profile-tile-inlined">
                                             <a class="profile-tile-box" href="{{url('bitcoin')}}">
                                                <div class="pt-avatar-w"><img alt="" src="/img/method/Bitcoin.png"></div>
                                                <div class="pt-user-name">Bitcoin<br></div>
                                             </a>
                                          </div>
                                       </div>
                                       <div class="col-4 col-sm-3 col-xxl-2">
                                          <div class="profile-tile profile-tile-inlined">
                                             <a class="profile-tile-box" href="{{url('skrill')}}">
                                                <div class="pt-avatar-w"><img alt="" src="/img/method/Skrill.png"></div>
                                                <div class="pt-user-name">Skrill<br></div>
                                             </a>
                                          </div>
                                       </div>
                                     
                                       @if(Auth::user()->area->id == 136 | 66 | 92 | 110)
                                       <div class="col-4 d-sm-none d-xxl-block col-xxl-2">
                                          <div class="profile-tile profile-tile-inlined">
                                             <a class="profile-tile-box" href="{{url('fnb')}}">
                                                <div class="pt-avatar-w"><img alt="" src="/img/method/Fnb.png"></div>
                                                <div class="pt-user-name">FNB Transfer<br></div>
                                             </a>
                                          </div>
                                       </div>
                                       @endif
                                        @if(Auth::user()->area->id == 136)
                                       <div class="col-4 d-sm-none d-xxl-block col-xxl-2">
                                          <div class="profile-tile profile-tile-inlined">
                                             <a class="profile-tile-box" href="{{url('capitec')}}">
                                                <div class="pt-avatar-w"><img alt="" src="/img/method/Capitec.png"></div>
                                                <div class="pt-user-name">Capitec Transfer<br></div>
                                             </a>
                                          </div>
                                       </div>
                                       @endif
                                           @if(Auth::user()->area->id == 34)
                                       <div class="col-4 d-sm-none d-xxl-block col-xxl-2">
                                          <div class="profile-tile profile-tile-inlined">
                                             <a class="profile-tile-box" href="{{url('orange')}}">
                                                <div class="pt-avatar-w"><img alt="" src="/img/method/Orange.jpg"></div>
                                                <div class="pt-user-name">Orange Money<br></div>
                                             </a>
                                          </div>
                                       </div>
                                       @endif
                                        @if(Auth::user()->area->id == 164)
                                       <div class="col-4 d-sm-none d-xxl-block col-xxl-2">
                                          <div class="profile-tile profile-tile-inlined">
                                             <a class="profile-tile-box" href="{{url('innbucks')}}">
                                                <div class="pt-avatar-w"><img alt="" src="/img/method/Innbucks.png"></div>
                                                <div class="pt-user-name">INNBUCKS<br></div>
                                             </a>
                                          </div>
                                       </div>
                                       @endif
                                        @if(Auth::user()->area->id == 164)
                                       <div class="col-4 d-sm-none d-xxl-block col-xxl-2">
                                          <div class="profile-tile profile-tile-inlined">
                                             <a class="profile-tile-box" href="{{url('ecocash')}}">
                                                <div class="pt-avatar-w"><img alt="" src="/img/method/Ecocash.png"></div>
                                                <div class="pt-user-name">Ecocash<br></div>
                                             </a>
                                          </div>
                                       </div>
                                       @endif
                                         @if(Auth::user()->area->id == 162)
                                       <div class="col-4 d-sm-none d-xxl-block col-xxl-2">
                                          <div class="profile-tile profile-tile-inlined">
                                             <a class="profile-tile-box" href="{{url('mtn')}}">
                                                <div class="pt-avatar-w"><img alt="" src="/img/method/Mtn.png"></div>
                                                <div class="pt-user-name">MTN Money<br></div>
                                             </a>
                                          </div>
                                       </div>
                                       @endif
                                       @if(Auth::user()->area->id == 162)
                                       <div class="col-4 d-sm-none d-xxl-block col-xxl-2">
                                          <div class="profile-tile profile-tile-inlined">
                                             <a class="profile-tile-box" href="{{url('airtel')}}">
                                                <div class="pt-avatar-w"><img alt="" src="/img/method/Airtel.jpg"></div>
                                                <div class="pt-user-name">Airtel Money<br></div>
                                             </a>
                                          </div>
                                       </div>
                                       @endif


                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="row">
                              <div class="col-12 col-xxl-8">
                                 <div class="element-wrapper compact pt-4">
                                    <div class="element-actions d-none d-sm-block">
                                      
                                    </div>
                                   
                                 </div>
                              </div>
                              
                           </div>
                        </div>
                        <div class="col-sm-4">
                           <div class="element-wrapper compact pt-4">
                              <div class="element-actions">
                                 
                              </div>
                              <h6 class="element-header">Transaction Minimums</h6>
                              <div class="element-box-tp">
                                 <table class="table table-clean">
                                    <tr>
                                       <td>
                                          <div class="value">Paypal</div>
                                          <span class="sub-value">USD</span>
                                       </td>
                                       <td class="text-right">
                                          <div class="value">$20</div>
                                          <span class="sub-value"></span>
                                       </td>
                                    </tr>
                                    <tr>
                                       <td>
                                          <div class="value">Bitcoin</div>
                                          <span class="sub-value">USD</span>
                                       </td>
                                       <td class="text-right">
                                          <div class="value">$20</div>
                                          <span class="sub-value"></span>
                                       </td>
                                    </tr>
                                    <tr>
                                       <td>
                                          <div class="value">Skrill</div>
                                          <span class="sub-value">USD</span>
                                       </td>
                                       <td class="text-right">
                                          <div class="value text-success">$20</div>
                                          <span class="sub-value"></span>
                                       </td>
                                    </tr>
                               
                                   
                                 </table>
                                
                              </div>
                           </div>
                        </div>
                     </div>


                           </div>
                        </div>
                     </div>
                     
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="floated-chat-btn"><i class="os-icon os-icon-mail-07"></i><span>Demo Chat</span></div>
                     <div class="floated-chat-w">
                        <div class="floated-chat-i">
                           <div class="chat-close"><i class="os-icon os-icon-close"></i></div>
                           <div class="chat-head">
                              <div class="user-w with-status status-green">
                                 <div class="user-avatar-w">
                                    <div class="user-avatar"><img alt="" src="img/avatar1.jpg"></div>
                                 </div>
                                 <div class="user-name">
                                    <h6 class="user-title">John Mayers</h6>
                                    <div class="user-role">Account Manager</div>
                                 </div>
                              </div>
                           </div>
                           <div class="chat-messages">
                              <div class="message">
                                 <div class="message-content">Hi, how can I help you?</div>
                              </div>
                              <div class="date-break">Mon 10:20am</div>
                              <div class="message">
                                 <div class="message-content">Hi, my name is Mike, I will be happy to assist you</div>
                              </div>
                              <div class="message self">
                                 <div class="message-content">Hi, I tried ordering this product and it keeps showing me error code.</div>
                              </div>
                           </div>
                           <div class="chat-controls">
                              <input class="message-input" placeholder="Type your message here...">
                              <div class="chat-extra"><a href="#"><span class="extra-tooltip">Attach Document</span><i class="os-icon os-icon-documents-07"></i></a><a href="#"><span class="extra-tooltip">Insert Photo</span><i class="os-icon os-icon-others-29"></i></a><a href="#"><span class="extra-tooltip">Upload Video</span><i class="os-icon os-icon-ui-51"></i></a></div>
                           </div>
                        </div>
                     </div>
                  </div>
@endsection

 
@extends('layouts.homeapp')

@section('title', 'Dashboard')

@section('description')

@endsection

@section('content')

 <main class="main-wrapper overflow-hidden">
         <section class="hero overflow-hidden">
            <div class="slider slider--active">
               <div class="slider__single slider__bg container-custom" data-bg-image="assets/images/bg/hero-bg-4.jpg">
                  <img src="assets/images/shape/slider-shape-1.png" alt="" class="slider__shape slider__shape--one" data-animation="slideInDown" data-delay=".4s" data-duration="2s"> <img src="assets/images/shape/slider-shape-2.png" alt="" class="slider__shape slider__shape--two" data-animation="slideInRight" data-delay=".5s" data-duration="2.5s">
                  <div class="container-fluid">
                     <div class="row">
                        <div class="col-xl-6 col-lg-7 col-md-8">
                           <div class="slider-content">
                              <span class="slider-content__title_small ml13" data-animation="fadeInUp" data-delay=".4s">FAST 5G ENHANCED DISTRIBUTION NETWORK</span>
                              <h2 id="ml12" class="slider-content__title_big" data-animation="fadeInUp" data-delay=".6s">Crowd driven 5G Solution and profit making network</h2>
                              <p class="slider-content__text" data-animation="fadeInUp" data-delay=".8s">The fastest way to earn commission and generate income.</p>
                              <a href="{{url('/boost-package')}}" class="btn btn--common btn--primary rt-button-animation-out" data-animation="fadeInUp" data-delay="1s">
                                 Get Boost Package
                                 <svg width="34px" height="16px" viewBox="0 0 34.53 16" xml:space="preserve">
                                    <rect class="rt-button-line" y="7.6" width="34" height=".4"></rect>
                                    <g class="rt-button-cap-fake">
                                       <path class="rt-button-cap" d="M25.83.7l.7-.7,8,8-.7.71Zm0,14.6,8-8,.71.71-8,8Z"></path>
                                    </g>
                                 </svg>
                              </a>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               
               
            </div>
         </section>
         <section class="about about--area about--padding">
            <div class="container">
               <div class="row align-items-center">
                  <div class="col-lg-5 mb-30">
                     <div class="section-heading mb-40">
                        <span class="section-heading__title_small wow fadeInUp" data-wow-delay=".2s" data-wow-duration=".4s">About Evolution 5G</span>
                        <h2 class="section-heading__title_big wow fadeInUp" data-wow-delay=".3s" data-wow-duration=".5s">We are a 5G based high speed distribution network</h2>
                     </div>
                     <div class="about-content">
                        <h3 class="about-content__title wow fadeInUp" data-wow-delay=".4s" data-wow-duration=".7s">Over <span class="primary-text-color">2,500+</span> Active Customers</h3>
                        <p class="about-content__text wow fadeInUp" data-wow-delay=".5s" data-wow-duration=".9s"> Evolution 5G takes advantage of the crowd to sell and distribute 5G based products and services, in turn our agents and those who are part of the network will benefit greatly in commisions and shareholder returns.</p>
                     </div>
                  </div>
                  <div class="col-lg-6 offset-lg-1 mb-30 text-md-center">
                     <div class="about-content__image about-content__image_right position-relative">
                        <img width="690" height="617" src="assets/images/home1/celltower.jpg" alt="About Our RadiusTheme Company" class="about-img wow fadeInRight" data-wow-delay="1s" data-wow-duration="1.2s">
                        <div class="about-content__experience">
                           <div class="about-content__experience_years">10</div>
                           <div class="about-content__experience_title">YEAR’S <span>MAKING 5G NETWORKING AND DISTRIBUTION AWESOME</span></div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </section>
        
         <section class="services-key services-key--padding">
            <div class="container">
               <div class="row">
                  <div class="col-lg-10 offset-lg-1">
                     <div class="section-heading text-center mb-30">
                        <span class="section-heading__title_small"> We offer the products and services</span>
                        <h2 class="section-heading__title_big">Join the network today</h2>
                     </div>
                  </div>
               </div>
              
          
            </div>
         </section>
         <div class="support support--bg support--padding secondary-bg-color pt-95 parallaxie" data-bg-image="assets/images/shape/support-bg-shape.png">
            <div class="container">
               <div class="row">
                  <div class="col-sm-12"></div>
               </div>
            </div>
         </div>
         <section class="case case--minus pt-100 pb-125">
            <div class="container">
               <div class="row align-items-center">
                  <div class="col-lg-4">
                     <div class="section-heading section-heading--style3 mb-50">
                        <span class="section-heading__title_small wow fadeInUp" data-wow-delay=".3s" data-wow-duration=".5s">SERVICES OFFERED</span>
                        <h2 class="section-heading__title_big text-white wow fadeInUp" data-wow-delay=".5s" data-wow-duration=".7s">Services</h2>
                     </div>
                  </div>
                  <div class="col-lg-8">
                     <div class="section-button mb-50">
                        <a href="{{url('about')}}" class="btn btn--common btn--primary rt-button-animation-out wow fadeInRight" data-wow-delay=".9s" data-wow-duration=".7s">
                           See More 
                           <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="34px" height="16px" viewBox="0 0 34.53 16" xml:space="preserve">
                              <rect class="rt-button-line" y="7.6" width="34" height=".4"></rect>
                              <g class="rt-button-cap-fake">
                                 <path class="rt-button-cap" d="M25.83.7l.7-.7,8,8-.7.71Zm0,14.6,8-8,.71.71-8,8Z"></path>
                              </g>
                           </svg>
                        </a>
                     </div>
                  </div>
               </div>
            </div>
            <div class="container-fluid">
               <div class="case-wrapper">
                  <div class="row case-active">
                     <div class="col">
                        <div class="case-item wow fadeInUp" data-wow-delay=".3s" data-wow-duration=".5s">
                           <div class="case-item__image"><a href="#"><img width="712" height="763" src="assets/images/case/case-image-1.jpg" alt="Case Studies"></a></div>
                           <div class="case-item__content secondary-bg-color text-center">
                              <h5 class="text-white"><a href="#">Online Shop</a></h5>
                              <span>Products Distribution</span>
                           </div>
                        </div>
                     </div>
                     <div class="col">
                        <div class="case-item wow fadeInUp" data-wow-delay=".3s" data-wow-duration=".5s">
                           <div class="case-item__image"><a href="#"><img width="712" height="763" src="assets/images/case/case-image-1.jpg" alt="Case Studies"></a></div>
                           <div class="case-item__content secondary-bg-color text-center">
                              <h5 class="text-white"><a href="#">On-demand Streaming Services</a></h5>
                              <span>Video Streaming</span>
                           </div>
                        </div>
                     </div>
                     <div class="col">
                        <div class="case-item wow fadeInUp" data-wow-delay=".5s" data-wow-duration=".7s">
                           <div class="case-item__image"><a href="#"><img width="712" height="763" src="assets/images/case/case-image-2.jpg" alt="Case Studies"></a></div>
                           <div class="case-item__content secondary-bg-color text-center">
                              <h5 class="text-white"><a href="#">5G Installations</a></h5>
                              <span>On-site Installation</span>
                           </div>
                        </div>
                     </div>
                     
                     
                    
                     
                  </div>
               </div>
            </div>
         </section>
        
         <section class="review review--bg review--padding position-relative">
            <div class="container">
               <div class="row align-items-center">
                  <div class="col-lg-6">
                     <div class="section-heading mb-55 wow fadeInUp" data-wow-delay=".3s" data-wow-duration=".5s">
                        <span class="section-heading__title_small">TESTIMONIALS</span>
                        <h2 class="section-heading__title_big">What our network users are saying</h2>
                     </div>
                  </div>
                  <div class="col-lg-6 wow fadeInRight" data-wow-delay=".5s" data-wow-duration=".7s">
                     <div class="section-button mb-55">
                        <a href="javascript:void(0);" class="btn btn--common btn--primary rt-button-animation-out">
                           See More 
                           <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="34px" height="16px" viewBox="0 0 34.53 16" xml:space="preserve">
                              <rect class="rt-button-line" y="7.6" width="34" height=".4"></rect>
                              <g class="rt-button-cap-fake">
                                 <path class="rt-button-cap" d="M25.83.7l.7-.7,8,8-.7.71Zm0,14.6,8-8,.71.71-8,8Z"></path>
                              </g>
                           </svg>
                        </a>
                     </div>
                  </div>
               </div>
            </div>
            <div class="review-wrapper">
               <div class="container-fluid">
                  <div class="row review--active">
                       @foreach($testimonies as $testimony)
                     <div class="col-lg-4 mb-30 wow fadeInLeft" data-wow-delay=".3s" data-wow-duration=".5s">
                      
                        <div class="review-block">
                           <div class="review-block__content">

                              <img class="review-block__quote_icon" src="assets/images/icon/left-quote.png" alt="Quotes Left">
                             
                              <p>{{$testimony->body}}</p>
                           </div>
                           <div class="review-block__auth">

                              <div class="review-block__auth_image"><a href="javascript:void(0);"><img  src="/uploads/avatars/{{ $testimony->user->avatar }}" alt="Review Author"></a></div>
                              <div class="review-block__auth_info">
                                 <a href="{{route('profile.index', ['email'=>$testimony->user->email])}}"><h3 style="color:blue;" class="review-block__auth_name">{{$testimony->user->name}} {{$testimony->user->surname}}</h3></a>
                                 <span class="review-block__auth_title">{{$testimony->user->level->name}}</span>
                                   <span class="review-block__auth_title"><div>{{$testimony->user->area->parent->name}} <span class="flag-icon flag-icon-{{$testimony->user->area->icon}}"></span></div></span>
                              </div>
                           </div>
                        </div>
                        
                     </div>
                    @endforeach
                  </div>
               </div>
            </div>
         </section>
        
         
         <section class="blog blog--padding">
            <div class="container">
               <div class="row align-items-center">
                  <div class="col-lg-6">
                     <div class="section-heading mb-55">
                        <span class="section-heading__title_small">Blogs & news</span>
                        <h2 class="section-heading__title_big">Blog Topics</h2>
                     </div>
                  </div>
                  <div class="col-lg-6">
                     <div class="section-button d-flex mb-55">
                        <a href="grid-layout.html" class="btn btn--common btn--primary rt-button-animation-out">
                           See More 
                           <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="34px" height="16px" viewBox="0 0 34.53 16" xml:space="preserve">
                              <rect class="rt-button-line" y="7.6" width="34" height=".4"></rect>
                              <g class="rt-button-cap-fake">
                                 <path class="rt-button-cap" d="M25.83.7l.7-.7,8,8-.7.71Zm0,14.6,8-8,.71.71-8,8Z"></path>
                              </g>
                           </svg>
                        </a>
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-sm-12">
                     <div class="blog--wrapper">
                        <div class="row g-0">
                           <div class="col-lg-4 col-md-6 blog-item__column mb-30">
                              <div class="blog-item blog-item--one">
                                 <header class="blog-item__header mb-35"><a href="{{url('blog-article1')}}" class="blog-item__img_link"><img width="551" height="395" class="blog-item__img" src="assets/images/blog/blog-thumb-1.jpg" alt="Questions every Business"> </a><a href="{{url('blog-article1')}}" class="blog-item__tags">Agents</a></header>
                                 <div class="blog-item__article">
                                    <span class="blog-item__date">22 Oct, 2021</span>
                                    <h3 class="blog-item__title"><a href="{{url('blog-article1')}}">Become a Marketing agent today.</a></h3>
                                    <p class="blog-item__text">Join the most competitive and highly rewarding Network platform, with sales commissions up to 25% on select products.</p>
                                    <ul class="blog-item__meta">
                                       <li class="blog-item__list"><i class="far fa-user blog-item__list_icon"></i> <span class="blog-item__list_name">By Mark Leuw</span></li>
                                      
                                    </ul>
                                 </div>
                              </div>
                           </div>
                           <div class="col-lg-4 col-md-6 blog-item__column mb-30">
                              <div class="blog-item blog-item--two">
                                 <header class="blog-item__header mb-35"><a href="{{url('blog-article1')}}" class="blog-item__img_link"><img width="551" height="395" class="blog-item__img" src="assets/images/blog/blog-thumb-2.jpg" alt="Questions every Business"> </a><a href="{{url('blog-article1')}}" class="blog-item__tags">FN23 5G Routers</a></header>
                                 <div class="blog-item__article">
                                    <span class="blog-item__date">22 April, 2021</span>
                                    <h3 class="blog-item__title"><a href="{{url('blog-article1')}}">Fast Network speeds achieved</a></h3>
                                    <p class="blog-item__text">Achieve the fastest network speeds with the latest FN23 5G capable router.Its capable of delivering 500Mbps in remote unserviced areas.</p>
                                    <ul class="blog-item__meta">
                                       <li class="blog-item__list"><i class="far fa-user blog-item__list_icon"></i> <span class="blog-item__list_name">By Martha Cullen</span></li>
                                      
                                 </div>
                              </div>
                           </div>
                           <div class="col-lg-4 col-md-6 blog-item__column mb-30">
                              <div class="blog-item blog-item--three">
                                 <header class="blog-item__header mb-35"><a href="{{url('blog-article1')}}" class="blog-item__img_link"><img width="551" height="395" class="blog-item__img" src="assets/images/blog/blog-thumb-3.jpg" alt="Questions every Business"> </a><a href="{{url('blog-article1')}}" class="blog-item__tags">Online Shop</a></header>
                                 <div class="blog-item__article">
                                    <span class="blog-item__date">27 April, 2021</span>
                                    <h3 class="blog-item__title"><a href="{{url('blog-article1')}}">Get Your products delivered in a few days</a></h3>
                                    <p class="blog-item__text">Shop online and get your products delivered at your door-step anywhere in africa.</p>
                                    <ul class="blog-item__meta">
                                       <li class="blog-item__list"><i class="far fa-user blog-item__list_icon"></i> <span class="blog-item__list_name">By James Mkaya</span></li>
                                      
                                    </ul>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </section>
         <section class="contact contact--bg contact--padding" data-bg-image="assets/images/shape/contact-bg-shape.png">
            <div class="container">
               <div class="row align-items-center">
                  <div class="col-lg-5 mb-30">
                     <div class="contact-address contact-address--bg" data-bg-image="assets/images/bg/contact-address-bg.jpg">
                        <ul>
                           <li class="contact-address__item mb-20">
                              <div class="contact-address__item_icon"><i class="fas fa-map-marker-alt"></i></div>
                              <div class="contact-address__item_text">
                                 <h3 class="text-white">Our Location</h3>
                                 <p class="text-white">10 Grampian Rd, Hong Kong</p>
                              </div>
                           </li>
                           <li class="contact-address__item mb-20">
                              <div class="contact-address__item_icon"><i class="fas fa-envelope-open-text"></i></div>
                              <div class="contact-address__item_text">
                                 <h3 class="text-white">Email Address</h3>
                                 <p class="text-white">support@evolution5g.org</p>
                                 <p class="text-white">hiring@evolution5g.org</p>
                              </div>
                           </li>
                           <li class="contact-address__item mb-40">
                              <div class="contact-address__item_icon"><i class="fab fa-whatsapp"></i></div>
                              <div class="contact-address__item_text">
                                 <h3 class="text-white">Whatsapp Us </h3>
                                 <p class="text-white"><i></i></p>
                              </div>
                           </li>
                           <li class="contact-address__item">
                              <form class="contact-address__form" action="javascript:void(0);"><input class="contact-address__form_input" type="text" placeholder="Find Your Solution"> <button class="contact-address__form_button"><i class="fas fa-arrow-right"></i></button></form>
                           </li>
                        </ul>
                     </div>
                  </div>
                  <div class="col-lg-7 mb-30">
                     <form action="javascript:void(0);" class="contact-form" id="contact-form">
                        <div class="row">
                           <div class="col-sm-12">
                              <div class="section-heading mb-45">
                                 <span class="section-heading__title_small">Get In Touch</span>
                                 <h2 class="section-heading__title_big">Begin Your 5G Journey Now.</h2>
                              </div>
                           </div>
                           <div class="col-md-6"><input name="name" type="text" class="contact-form__input" placeholder="Name *"></div>
                           <div class="col-md-6"><input name="email" type="text" class="contact-form__input" placeholder="Email *"></div>
                           <div class="col-md-6"><input name="phone" type="text" class="contact-form__input" placeholder="Phone"></div>
                           <div class="col-md-6">
                              <select class="contact-form__input" name="subject">
                                 <option value="1">Select your subject</option>
                              </select>
                           </div>
                           <div class="col-md-12"><textarea class="contact-form__input contact-form__input_textarea" name="message" placeholder="Message"></textarea></div>
                           <div class="col-md-12"><button type="submit" class="contact-form__button">Send Message</button></div>
                        </div>
                        <div class="form-response"></div>
                     </form>
                  </div>
               </div>
            </div>
         </section>
      </main>
 

@endsection
@extends('layouts.app')

@section('title', 'Dashboard')

@section('description')

@endsection

@section('content')
@if($listing->recommit())

<div class="content-box">
                     
                    
                     <div class="element-wrapper">
                        <div class="element-box">
                           <h5 class="form-header">BONUS WITHDRAWAL APPROVAL</h5>
                           <div class="form-desc">Admin</div>
                       @php ($withdrawn = 0)

                 @foreach(Auth::user()->listings as $listing)
                            @if($listing->recommit())


                            @php ($withdrawn += $listing->amount)

                           @if ($loop->last)

                           @endif

                           @else

                         @endif
                     @endforeach
                  <div class="row">
                        <div class="col-sm-12">
                           <div class="element-wrapper compact pt-4">
                               <div class="col-lg-6 col-xxl-7">
                           <div class="element-wrapper">
                              <div class="element-box">
                                 <form class="form" method="post" action="{{ route('listings.update', $listing) }}">
                                    <h5 class="element-box-header">BONUS PANEL</h5>
                                    <h5 class="element-box-header"> {{ Auth::user()->area->unit }} {{\openjobs\Bonus::where('referer_id', auth()->user()->id)->sum('bonus_amount')+\openjobs\Bonus::where('second_gen_id', auth()->user()->id)->sum('second_gen_amount')+\openjobs\Bonus::where('third_gen_id', auth()->user()->id)->sum('third_gen_amount')-($withdrawn)}}.00</h5>
                                    <div class="row">
                                       <div class="col-sm-5">
                                          <div class="form-group">
                                             <label class="lighter" for="">Select Package</label>
                                          
                                             </div>
                                         </div>
                                       

                                           
                                     <input type="hidden" class="form-control" name="amount" id="type" value="{{$listing->amount}}">
                                       
                                  <input type="hidden" class="form-control" name="category_id" id="period" value="2">      
                               <input type="hidden" class="form-control" name="value" id="period" value="0">
                               <input type="hidden" class="form-control" name="current" id="period" value="0">
                          
                                 <input type="hidden" class="form-control" name="period" id="period" value="28">
                                  <input type="hidden" class="form-control" name="type" id="period" value="1">
                                    <input type="hidden" class="form-control" name="matched" id="period" value="0">
                                         
                                 
                                  
                                    <input type="hidden" class="form-control" name="maturityamount" id="matched" value="{{$listing->maturityamount}}">
                                    <input type="hidden" class="form-control" name="recommit" id="recommit" value="1">
                                           <input type="hidden" class="form-control" name="percent" id="percent" value="1.35">
                                         <input type="hidden" class="form-control" name="area_id" id="area" value="5">
                                         <input type="hidden" class="form-control" name="days" id="area" value="30">
                                         
                                       
                                    </div>
                                    <div class="form-buttons-w text-right compact"><button class="btn btn-primary" type="submit" ><span>Approve Withdrawal</span><i class="os-icon os-icon-grid-18"></i></button></div>
                                                      {{ csrf_field() }}
                        {{ method_field('PATCH') }}
                                 </form>
                              </div>
                           </div>
                        </div>
                           </div>
                           <div class="row">
                              <div class="col-12 col-xxl-8">
                                 <div class="element-wrapper compact pt-4">
                                    <div class="element-actions d-none d-sm-block">
                                      
                                    </div>
                                   
                                 </div>
                              </div>
                              
                           </div>
                        </div>
                       
                     </div>


                           </div>
                        </div>
                     </div>
                     
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="floated-chat-btn"><i class="os-icon os-icon-mail-07"></i><span>Demo Chat</span></div>
                     <div class="floated-chat-w">
                        <div class="floated-chat-i">
                           <div class="chat-close"><i class="os-icon os-icon-close"></i></div>
                           <div class="chat-head">
                              <div class="user-w with-status status-green">
                                 <div class="user-avatar-w">
                                    <div class="user-avatar"><img alt="" src="img/avatar1.jpg"></div>
                                 </div>
                                 <div class="user-name">
                                    <h6 class="user-title">John Mayers</h6>
                                    <div class="user-role">Account Manager</div>
                                 </div>
                              </div>
                           </div>
                           <div class="chat-messages">
                              <div class="message">
                                 <div class="message-content">Hi, how can I help you?</div>
                              </div>
                              <div class="date-break">Mon 10:20am</div>
                              <div class="message">
                                 <div class="message-content">Hi, my name is Mike, I will be happy to assist you</div>
                              </div>
                              <div class="message self">
                                 <div class="message-content">Hi, I tried ordering this product and it keeps showing me error code.</div>
                              </div>
                           </div>
                           <div class="chat-controls">
                              <input class="message-input" placeholder="Type your message here...">
                              <div class="chat-extra"><a href="#"><span class="extra-tooltip">Attach Document</span><i class="os-icon os-icon-documents-07"></i></a><a href="#"><span class="extra-tooltip">Insert Photo</span><i class="os-icon os-icon-others-29"></i></a><a href="#"><span class="extra-tooltip">Upload Video</span><i class="os-icon os-icon-ui-51"></i></a></div>
                           </div>
                        </div>
                     </div>
                  </div>





@else

<div class="content-box">
                     
                    
                     <div class="element-wrapper">
                        <div class="element-box">
                           <h5 class="form-header">Admin Approve Deposit</h5>
                           <div class="form-desc">Admin</div>
                     
                  <div class="row">
                        <div class="col-sm-12">
                           <div class="element-wrapper compact pt-4">
                               <div class="col-lg-6 col-xxl-7">
                           <div class="element-wrapper">
                              <div class="element-box">
                                 <form class="form" method="post" action="{{ route('listings.update', $listing) }}">
                                    <h5 class="element-box-header">Approval Panel</h5>
                                    <h5 class="element-box-header">{{Auth::user()->area->unit}} @if(Auth::user()->area->pin==1)
               {{$listing->category->usd}}
               @else
              {{$listing->category->size}}
               @endif {{$listing->category->name}}</h5>
                                    <div class="row">
                                       <div class="col-sm-5">
                                          <div class="form-group">
                                             <label class="lighter" for="">Select Package</label>
                                          <select name="category_id" id="category" class="form-control">
                                              <option value="14">Member Boost Package</option>
                                             <option value="2">Starter Boost Package</option>
                                              <option value="4">Bronze Boost Package</option>
                                                <option value="6">Silver Boost Package</option>
                                                <option value="8">Gold Boost Package</option>
                                                <option value="10">Black Card</option>
                                                <option value="12">5G Ultimate</option>

                                          </select>
                                             </div>
                                         </div>
                                       <div class="col-sm-7">
                                          <div class="form-group">
                                             <label class="lighter" for="">Transaction Type</label>
                                             <select name="type" class="form-control">
                                              <option value="1">Live Package</option>
                                              <option value="0">Closed Transaction</option>
                                         
                                               

                                             </select>
                                          </div>
                                       </div>
                             
                                        @if(Auth::user()->area->pin==1)
                                       <input type="hidden" class="form-control" name="amount" id="type" value="{{$listing->category->dollar}}">
                                       @else

                                       <input type="hidden" class="form-control" name="amount" id="type" value="{{$listing->category->size}}">
                                       @endif
                                  
                                       
                                        
                               <input type="hidden" class="form-control" name="value" id="period" value="1">
                               <input type="hidden" class="form-control" name="current" id="period" value="0">
                          
                                 <input type="hidden" class="form-control" name="period" id="period" value="28">
                                         
                                 
                                   <input type="hidden" class="form-control" name="matched" id="matched" value="1">
                                    <input type="hidden" class="form-control" name="maturityamount" id="matched" value="{{$listing->maturityamount}}">
                                    <input type="hidden" class="form-control" name="recommit" id="recommit" value="0">
                                           <input type="hidden" class="form-control" name="percent" id="percent" value="1.35">
                                         <input type="hidden" class="form-control" name="area_id" id="area" value="5">
                                         <input type="hidden" class="form-control" name="days" id="area" value="30">
                                         
                                       
                                    </div>
                                    <div class="form-buttons-w text-right compact"><button class="btn btn-primary" type="submit" ><span>Approve Transaction</span><i class="os-icon os-icon-grid-18"></i></button></div>
                                                      {{ csrf_field() }}
                        {{ method_field('PATCH') }}
                                 </form>
                              </div>
                           </div>
                        </div>
                           </div>
                           <div class="row">
                              <div class="col-12 col-xxl-8">
                                 <div class="element-wrapper compact pt-4">
                                    <div class="element-actions d-none d-sm-block">
                                      
                                    </div>
                                   
                                 </div>
                              </div>
                              
                           </div>
                        </div>
                       
                     </div>


                           </div>
                        </div>
                     </div>
                     
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="floated-chat-btn"><i class="os-icon os-icon-mail-07"></i><span>Demo Chat</span></div>
                     <div class="floated-chat-w">
                        <div class="floated-chat-i">
                           <div class="chat-close"><i class="os-icon os-icon-close"></i></div>
                           <div class="chat-head">
                              <div class="user-w with-status status-green">
                                 <div class="user-avatar-w">
                                    <div class="user-avatar"><img alt="" src="img/avatar1.jpg"></div>
                                 </div>
                                 <div class="user-name">
                                    <h6 class="user-title">John Mayers</h6>
                                    <div class="user-role">Account Manager</div>
                                 </div>
                              </div>
                           </div>
                           <div class="chat-messages">
                              <div class="message">
                                 <div class="message-content">Hi, how can I help you?</div>
                              </div>
                              <div class="date-break">Mon 10:20am</div>
                              <div class="message">
                                 <div class="message-content">Hi, my name is Mike, I will be happy to assist you</div>
                              </div>
                              <div class="message self">
                                 <div class="message-content">Hi, I tried ordering this product and it keeps showing me error code.</div>
                              </div>
                           </div>
                           <div class="chat-controls">
                              <input class="message-input" placeholder="Type your message here...">
                              <div class="chat-extra"><a href="#"><span class="extra-tooltip">Attach Document</span><i class="os-icon os-icon-documents-07"></i></a><a href="#"><span class="extra-tooltip">Insert Photo</span><i class="os-icon os-icon-others-29"></i></a><a href="#"><span class="extra-tooltip">Upload Video</span><i class="os-icon os-icon-ui-51"></i></a></div>
                           </div>
                        </div>
                     </div>
                  </div>

                  @endif
@endsection

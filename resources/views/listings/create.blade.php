@extends('layouts.app')

@section('title', 'Dashboard')

@section('description')

@endsection

@section('content')

<div class="content-box">
                     
                    
                     <div class="element-wrapper">
                        <div class="element-box">
                           <h5 class="form-header">Create Deposit Approval Request</h5>
                           <div class="form-desc">Enter the amount you deposited so that the accounts department can approve your deposit. </div>
                     
                  <div class="row">
                        <div class="col-sm-12">
                           <div class="element-wrapper compact pt-4">
                               <div class="col-lg-6 col-xxl-7">
                           <div class="element-wrapper">
                              <div class="element-box">
                                 <form class="form" method="post" action="{{ route('listings.store', [$area]) }}">
                                    <h5 class="element-box-header">Deposit Request</h5>
                                    <div class="row">
                                       <div class="col-sm-5">
                                          <div class="form-group">
                                             <label class="lighter" for="">Amount</label>
                                             <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                                                <input name="amount"  class="form-control" placeholder="Enter Amount" >
                                                <div class="input-group-append">
                                                   <div class="input-group-text">USD</div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="col-sm-7">
                                          <div class="form-group">
                                             <label class="lighter" for="">Method Used</label>
                                             <select class="form-control" name="mode">
                                                <option value="Paypal">Paypal</option>
                                                <option value="Bitcoin">Bitcoin</option>
                                                <option value="Bank Transfer">Bank Transfer</option>
                                                <option value="Skrill">Skrill</option>
                                                <option value="Mobile Money">Mobile Money</option>
                                                

                                             </select>
                                          </div>
                                       </div>
                                       <input type="hidden" class="form-control" name="value" id="value" value="0.045">
                                       <input type="hidden" class="form-control" name="period" id="period" value="28">
                                        <input type="hidden" class="form-control" name="current" id="current" value="0">
                                          <input type="hidden" class="form-control" name="recommit" id="period" value="0">
                                   <input type="hidden" class="form-control" name="type" id="type" value="1">
                                           <input type="hidden" class="form-control" name="percent" id="percent" value="1.35">
                                         <input type="hidden" class="form-control" name="area_id" id="area" value="5">
                                         <input type="hidden" class="form-control" name="category_id" id="area" value="2">
                                    </div>
                                    <div class="form-buttons-w text-right compact"><button class="btn btn-primary" type="submit" ><span>Create Deposit</span><i class="os-icon os-icon-grid-18"></i></button></div>
                                    {{ csrf_field() }} 
                                 </form>
                              </div>
                           </div>
                        </div>
                           </div>
                           <div class="row">
                              <div class="col-12 col-xxl-8">
                                 <div class="element-wrapper compact pt-4">
                                    <div class="element-actions d-none d-sm-block">
                                      
                                    </div>
                                   
                                 </div>
                              </div>
                              
                           </div>
                        </div>
                       
                     </div>


                           </div>
                        </div>
                     </div>
                     
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="floated-chat-btn"><i class="os-icon os-icon-mail-07"></i><span>Demo Chat</span></div>
                     <div class="floated-chat-w">
                        <div class="floated-chat-i">
                           <div class="chat-close"><i class="os-icon os-icon-close"></i></div>
                           <div class="chat-head">
                              <div class="user-w with-status status-green">
                                 <div class="user-avatar-w">
                                    <div class="user-avatar"><img alt="" src="img/avatar1.jpg"></div>
                                 </div>
                                 <div class="user-name">
                                    <h6 class="user-title">John Mayers</h6>
                                    <div class="user-role">Account Manager</div>
                                 </div>
                              </div>
                           </div>
                           <div class="chat-messages">
                              <div class="message">
                                 <div class="message-content">Hi, how can I help you?</div>
                              </div>
                              <div class="date-break">Mon 10:20am</div>
                              <div class="message">
                                 <div class="message-content">Hi, my name is Mike, I will be happy to assist you</div>
                              </div>
                              <div class="message self">
                                 <div class="message-content">Hi, I tried ordering this product and it keeps showing me error code.</div>
                              </div>
                           </div>
                           <div class="chat-controls">
                              <input class="message-input" placeholder="Type your message here...">
                              <div class="chat-extra"><a href="#"><span class="extra-tooltip">Attach Document</span><i class="os-icon os-icon-documents-07"></i></a><a href="#"><span class="extra-tooltip">Insert Photo</span><i class="os-icon os-icon-others-29"></i></a><a href="#"><span class="extra-tooltip">Upload Video</span><i class="os-icon os-icon-ui-51"></i></a></div>
                           </div>
                        </div>
                     </div>
                  </div>
@endsection

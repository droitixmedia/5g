@extends('layouts.app')

@section('title', 'Dashboard')

@section('description')

@endsection

@section('content')

<div class="content-box">
                     
                    
                     <div class="element-wrapper">
                        <div class="element-box">
                           <h5 class="form-header">Withdraw</h5>
                           <div class="form-desc">Withdraw Cash </div>
                     
                  <div class="row">
                        <div class="col-sm-12">
                           <div class="element-wrapper compact pt-4">
                               <div class="col-lg-6 col-xxl-7">
                           <div class="element-wrapper">
                              <div class="element-box">
                                @if($errors->any())
<h5 style="color:red">{{$errors->first()}}</h5>
@endif
                                            @if(session('success'))
    <h5 style="color:green">{{session('success')}}</h5>

@endif
                                   @php ($withdrawn = 0)

                 @foreach(Auth::user()->listings as $listing)
                            @if($listing->recommit())


                            @php ($withdrawn += $listing->amount)

                           @if ($loop->last)

                           @endif

                           @else

                         @endif
                     @endforeach

                                <h5 class="element-box-header">BONUS BALANCE: {{ Auth::user()->area->unit }} {{\openjobs\Bonus::where('referer_id', auth()->user()->id)->sum('bonus_amount')+\openjobs\Bonus::where('second_gen_id', auth()->user()->id)->sum('second_gen_amount')+\openjobs\Bonus::where('third_gen_id', auth()->user()->id)->sum('third_gen_amount')-($withdrawn)}}.00 </h5>
                                 <form class="form" method="post" action="{{ route('listings.store', [$area]) }}">
                                    <h5 class="element-box-header">Withdrawal Request</h5>
                                    <input type="hidden" class="form-control{{ $errors->has('amount') ? ' is-invalid' : '' }}" name="amount" value="{{\openjobs\Bonus::where('referer_id', auth()->user()->id)->sum('bonus_amount')+\openjobs\Bonus::where('second_gen_id', auth()->user()->id)->sum('second_gen_amount')+\openjobs\Bonus::where('third_gen_id', auth()->user()->id)->sum('third_gen_amount')-($withdrawn)}}">
                                      
                                        
                                          <input type="hidden" class="form-control" name="value" id="value" value="1">
                                           <input type="hidden" class="form-control" name="current" id="value" value="1">
                                         <input type="hidden" class="form-control" name="recommit" id="period" value="1">
                                         <input type="hidden" class="form-control" name="period" id="period" value="1">
                                   <input type="hidden" class="form-control" name="type" id="type" value="1">
                                           <input type="hidden" class="form-control" name="percent" id="percent" value="1.35">
                                         <input type="hidden" class="form-control" name="area_id" id="area" value="5">
                                         <input type="hidden" class="form-control" name="category_id" id="area" value="2">
                                    <div class="form-buttons-w text-left compact"><button class="btn btn-primary" type="submit" ><i class="os-icon os-icon-log-out"></i><span>Withdraw</span></button></div>
                                    {{ csrf_field() }} 
                                 </form>
                              </div>
                           </div>
                        </div>
                           </div>
                           <div class="row">
                              <div class="col-12 col-xxl-8">
                                 <div class="element-wrapper compact pt-4">
                                    <div class="element-actions d-none d-sm-block">
                                      
                                    </div>
                                   
                                 </div>
                              </div>
                              
                           </div>
                        </div>
                       
                     </div>


                           </div>
                        </div>
                     </div>
                     
                              </div>
                           </div>
                        </div>
                     </div>
                    
                  </div>
@endsection

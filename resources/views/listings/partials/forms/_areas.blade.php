<div class="form-group{{ $errors->has('area_id') ? ' has-error' : '' }}">
    <label for="area" class="control-label">Select Your Country</label>
    <select name="area_id" id="area" class="form-control">
        @foreach($areas as $country)
         
                @foreach ($country->children as $state)
                   
                        <option value="{{ $state->id }}" flag-icon flag-icon-{{$state->icon}}>{{ $state->parent->name }}</option>
                       
                   
                @endforeach
           
        @endforeach
    </select>

    @if ($errors->has('area_id'))
        <span class="help-block">
            {{ $errors->first('area_id') }}
        </span>
    @endif
</div>

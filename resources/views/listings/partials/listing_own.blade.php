@if(!$listing->type())
<!-- content @s -->       
<div class="nk-block">
<div class="row g-gs">
   <div class="col-md-6">
      <div class="card card-bordered pricing">
         <div class="pricing-head">
            <div class="pricing-title">
               <h4 class="card-title title">Contract Expired</h4>
               <p class="sub-text">This investment was stopped</p>
            </div>
            <div class="card-text">
               <div class="row">
                  <div class="col-6">
                     <span class="h4 fw-500"></span>
                     <span class="sub-text">Daily Interest</span>
                  </div>
                  <div class="col-6">
                     <span class="h4 fw-500"></span>
                     <span class="sub-text">Contract Balance</span>
                  </div>
               </div>
            </div>
         </div>
         <div class="pricing-body">
            <ul class="pricing-features">
            </ul>
            @if (session()->has('impersonate'))
            <div style="margin-top: 20px">
               <form action="{{ route('listings.destroy', $listing) }}" method="post" id="listings-destroy-form-{{ $listing->id }}">
                  {{ csrf_field() }}
                  {{ method_field('DELETE') }}
               </form>
               <li>
                  <div class="pricing-action"><a href="#" class="btn_1 gray delete" class="btn_1 gray delete"  onclick="event.preventDefault(); document.getElementById('listings-destroy-form-{{ $listing->id }}').submit();"><i class="fas fa-times-circle"></i> Admin Delete this request</a></div>
               </li>
               @endif
            </div>
         </div>
      </div>
      <!-- .col -->
   </div>
   <!-- .nk-block -->
</div>
@else
@if($listing->recommit())
<div class="col-md-4">
   <div class="ecommerce-customer-info">
      <div class="ecommerce-customer-main-info">
         <div class="ecc-avatar" style="background-image: url(/img/method/Bonus.png)"></div>
         <div class="ecc-name">Bonus Request</div>
      </div>
      <h4 style="color:red">Contact 0777969289 To Release Withdrawal </h4>
      <div class="ecommerce-customer-sub-info">
         <div class="ecc-sub-info-row">
            <div class="sub-info-label">AMOUNT WITHDRAWN</div>
            <div class="sub-info-value">{{Auth::user()->area->unit}}{{$listing->amount}}</div>
         </div>
         <div class="ecc-sub-info-row">
          
            <div class="ecc-sub-info-row">
                @if($listing->value =='0')
               <a href="#" class="btn btn-success" >BONUS APPROVED!</a>
               @endif
               @if (session()->has('impersonate'))
               @if($listing->value =='0')
             
               @else
               <a href="{{ route('listings.edit', $listing) }}" class="btn btn-warning" >Approve Withdrawal</a>
               @endif
               @endif
            </div>
            <br>
           
            <br>
           
            
         </div>
      </div>
      
      <div class="os-tabs-controls">
         <ul class="nav nav-tabs">
            <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#tab_overview"> Status</a></li>
         </ul>
      </div>
      <div class="tab-content">
         <div class="ecc-sub-info-row">
              @if($listing->value =='0')
            <div class="sub-info-label"> APPROVED AND MONEY SENT!</div>
            @else

            <div class="sub-info-label"> PENDING APPROVAL</div>
            @endif
         </div>
         @if($listing->value =='0')


         @else
         <form action="{{ route('listings.destroy', $listing) }}" method="post" id="listings-destroy-form-{{ $listing->id }}">
            {{ csrf_field() }}
            {{ method_field('DELETE') }}
         </form>
         <div class="ecc-sub-info-row">
            <a href="#" class="btn btn-danger" onclick="event.preventDefault(); document.getElementById('listings-destroy-form-{{ $listing->id }}').submit();">Delete Request</a>
         </div>
         @endif
      </div>
   </div>
</div>
@else
@if(!$listing->matched())
<div class="col-md-4">
   <div class="ecommerce-customer-info">
      <div class="ecommerce-customer-main-info">
         <div class="ecc-avatar" style="background-image: url(/img/method/{{$listing->paymentmethod}}.png)"></div>
         <div class="ecc-name">{{$listing->paymentmethod}} Payment Order</div>
      </div>
      <div class="ecommerce-customer-sub-info">
         <div class="ecc-sub-info-row">
            <div class="sub-info-label">AMOUNT TO BE PAID</div>
            <div class="sub-info-value">{{Auth::user()->area->unit}}
               @if(Auth::user()->area->pin==1)
               {{$listing->category->usd}}
               @else
              {{$listing->category->size}}
               @endif

            </div>
         </div>
         <div class="ecc-sub-info-row">
            <div class="sub-info-value">{{$listing->category->name}}</div>
            <hr>
            <div class="ecc-sub-info-row">
               @if (session()->has('impersonate'))
               <a href="{{ route('listings.edit', $listing) }}" class="btn btn-success" >Confirm Payment</a>
               @endif
            </div>
            <br>
            <div class="sub-info-label">PAY {{Auth::user()->area->unit}} @if(Auth::user()->area->pin==1)
               {{$listing->category->usd}}
               @else
              {{$listing->category->size}}
               @endif TO:</div>
            <div class="sub-info-value">{{$listing->payment_method_account}}</div>
            <br>
             @if($listing->paymentmethod =='Bitcoin')
            <div class="onboarding-media"><img alt="" src="/img/method/btcqr.png" width="200px"></div><br>
            @endif
            <div class="sub-info-label">UPLOAD PROOF OF PAYMENT BELOW</div>
            <div style="color:red" class="sub-info-label">IF PAID, WHATSAPP 0777969289 FOR APPROVAL</div>
            <form action="/uploaddocs" method="post" enctype="multipart/form-data">
               {{ csrf_field() }}
               <div class="form-group">
                  <input type="hidden" name="name" value="{{$listing->paymentmethod}}" class="form-control">
               </div>
               <br />
               <input type="file" class="form-control" name="cvs[]" multiple />
               <br />
               <input type="submit" class="btn  btn-success" value="Upload" />
            </form>
         </div>
      </div>
      <div class="ecc-sub-info-row">
         <div class="sub-info-value"><a href="{{url('payment-proof')}}">view proof of payment</a></div>
      </div>
      <div class="os-tabs-controls">
         <ul class="nav nav-tabs">
            <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#tab_overview"> Status</a></li>
         </ul>
      </div>
      <div class="tab-content">
         <div class="ecc-sub-info-row">
            <div class="sub-info-label"> PENDING APPROVAL</div>
         </div>
         <form action="{{ route('listings.destroy', $listing) }}" method="post" id="listings-destroy-form-{{ $listing->id }}">
            {{ csrf_field() }}
            {{ method_field('DELETE') }}
         </form>
         <div class="ecc-sub-info-row">
            <a href="#" class="btn btn-danger" onclick="event.preventDefault(); document.getElementById('listings-destroy-form-{{ $listing->id }}').submit();">Delete Order</a>
         </div>
      </div>
   </div>
</div>
@else
<div class="col-md-4">
   <div class="ecommerce-customer-info">
      <div class="ecommerce-customer-main-info">
         <div class="ecc-avatar" style="background-image: url(/img/packages/{{$listing->category->parent->icon}}.png)"></div>
         <div class="ecc-name">{{$listing->category->name}}</div>
      </div>
      @php ($sum = 0)
            @foreach($listing->comments as $comment)
            @php ($sum += $comment->split)
            @if ($loop->last)
            @endif
            @endforeach
            @php ($now = \Carbon\Carbon::now())
            @php($days = \Carbon\Carbon::parse($listing->updated_at)->diffInDays($now))
            @php($percentage = $listing->category->parent->percent)
            @php($multiplier = ($listing->amount))
      <div class="ecommerce-customer-sub-info">
         <div class="ecc-sub-info-row">
            <div class="sub-info-label">PACKAGE BALANCE</div>
            <div class="sub-info-value" style="text-align: center;">
               <h2>{{ Auth::user()->area->unit }} {{($listing->amount-$sum)+(($multiplier)*($percentage)*$days)}}</h2>
            </div>
             @if($listing->comments->count())
            @foreach($listing->comments as $comment)
            @if (!$comment->approvals->count())
             <div class="sub-info-label" style="color: red;">{{ Auth::user()->area->unit }} {{$comment->split}} WITHDRAWAL PROCESSING...</div>
             <div class="sub-info-label" style="color: green;">INBOX 0777969289 FOR WITHDRAWAL RELEASING</div>  
            @else
            @endif
                @if (session()->has('impersonate'))
            @if ($comment->approvals->count())
            @else
            <form action="{{ route('approvals.store', [$comment->id]) }}" method="post">
               <input type="hidden" class="form-control" name="body" id="body" value="Approved">
               <div class="pricing-action">
                 
                  <button type="submit" class="btn btn-primary">{{ Auth::user()->area->unit }} {{$comment->split}} Approve Withdrawal</button>
               </div>
               {{ csrf_field() }}
            </form>
            @endif
            @endif 
            @endforeach
            @endif
         </div>
         <div class="ecc-sub-info-row">
            <a href="{{ route('listings.apply', $listing) }}" class="btn btn-success">Withdraw Earnings</a>
         </div>
         <div class="ecc-sub-info-row">
            <div class="sub-info-value">PACKAGE EARNS: {{$listing->category->parent->percent*100}}% Daily</div>
            <hr>
            <div class="ecc-sub-info-row">
               @if (session()->has('impersonate'))
               <a href="{{ route('listings.edit', $listing) }}" class="btn btn-warning" >Edit Package</a>
               @endif
            </div>
            <br>
            <div class="sub-info-label">PACKAGE STARTED: {{$listing->updated_at->diffForHumans()}} </div>
            <br>
            <div class="sub-info-label">DIRECT BONUS EARNED BY <a href="{{route('profile.index', ['email'=>Auth::user()->referrer->email])}}">{{Auth::user()->referrer->name}}</a> </div>
            <br>
         </div>
      </div>
      <div class="os-tabs-controls">
         <ul class="nav nav-tabs">
            <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#tab_overview"> NOTES</a></li>
         </ul>
      </div>
      <div class="tab-content">
         <div class="ecc-sub-info-row">
            <div class="sub-info-label"> ACTIVE PACKAGE</div>
         </div>
         <div class="ecc-sub-info-row">
          @if($listing->comments->count())
            @foreach($listing->comments as $comment)
            @if ($comment->approvals->count())
             <div class="sub-info-label" style="color: green;">{{ Auth::user()->area->unit }} {{$comment->split}} WITHDRAWN SUCCESSFULLY on {{$comment->created_at}}</div> 
            @else
            @endif
            @endforeach
            @endif
        </div>
         <form action="{{ route('listings.destroy', $listing) }}" method="post" id="listings-destroy-form-{{ $listing->id }}">
            {{ csrf_field() }}
            {{ method_field('DELETE') }}
         </form>
         @if (session()->has('impersonate'))
         <div class="ecc-sub-info-row">
            <a href="#" class="btn btn-danger" onclick="event.preventDefault(); document.getElementById('listings-destroy-form-{{ $listing->id }}').submit();">Delete Order</a>
         </div>
         @endif
      </div>
   </div>
</div>
@endif
@endif
@endif
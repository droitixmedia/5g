<!DOCTYPE html>
<html>
   <!-- Mirrored from light.pinsupreme.com/auth_register.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 04 Nov 2021 14:01:30 GMT -->
   <head>
      <title>Create Account - Evolution 5G</title>
      <meta charset="utf-8">
      <meta content="ie=edge" http-equiv="x-ua-compatible">
      <meta content="template language" name="keywords">
      <meta content="Evolution 5G" name="author">
      <meta content="Evolution 5G" name="description">
      <meta content="width=device-width,initial-scale=1" name="viewport">
      <link href="favicon.png" rel="shortcut icon">
      <link href="apple-touch-icon.png" rel="apple-touch-icon">
      <link href="../fast.fonts.net/cssapi/487b73f1-c2d1-43db-8526-db577e4c822b.css" rel="stylesheet">
      <link href="/bower_components/select2/dist/css/select2.min.css" rel="stylesheet">
      <link href="/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
      <link href="/bower_components/dropzone/dist/dropzone.css" rel="stylesheet">
      <link href="/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
      <link href="/bower_components/fullcalendar/dist/fullcalendar.min.css" rel="stylesheet">
      <link href="/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css" rel="stylesheet">
      <link href="/bower_components/slick-carousel/slick/slick.css" rel="stylesheet">
      <link href="/css/main5739.css?version=4.5.0" rel="stylesheet">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/4.1.5/css/flag-icons.min.css" integrity="sha512-UwbBNAFoECXUPeDhlKR3zzWU3j8ddKIQQsDOsKhXQGdiB5i3IHEXr9kXx82+gaHigbNKbTDp3VY/G6gZqva6ZQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
   </head>
   <body>
      <div class="all-wrapper menu-side with-pattern">
         <div class="auth-box-w wider">
            <div class="logo-w"><a href="{{url('/')}}"><img alt="" src="/img/logo-big.png"></a></div>
            <h4 class="auth-header">Create new account</h4>
            <form id="login" action="{{ route('register') }}" method="post" >
            @csrf
               <div class="row">
                  <div class="col-sm-6">
                     <div class="form-group">
                        <label for=""> Name</label><input class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus placeholder="Name" type="text">
                        <div class="pre-icon os-icon os-icon-user"></div>
                         @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                        @endif
                     </div>
                  </div>
               
                  <div class="col-sm-6">
                     <div class="form-group">
                        <label for=""> Surname</label><input class="form-control{{ $errors->has('surname') ? ' is-invalid' : '' }}" name="surname" value="{{ old('surname') }}" required autofocus placeholder="Last Name" type="text">
                      
                         @if ($errors->has('surname'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('surname') }}</strong>
                                    </span>
                        @endif
                     </div>
                  </div>
               </div>
               <div class="form-group">
                  <label for=""> Email address</label><input class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" value="{{ old('email') }}" name="email"  placeholder="Email" type="email">
                  <div class="pre-icon os-icon os-icon-email-2-at2"></div>
                   @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                        @endif
               </div>
               
               @include('listings.partials.forms._areas')

               <div class="form-group">
                  <label for="phone"> Phone</label><input class="form-control{{ $errors->has('phone_number') ? ' is-invalid' : '' }}" value="{{ old('phone_number') }}" placeholder="Phone Number" name="phone_number" type="text">
                  <div class="pre-icon os-icon os-icon-phone"></div>
                   @if ($errors->has('phone_number'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('phone_number') }}</strong>
                                    </span>
                   @endif
               </div>

               <div class="row">
                  <div class="col-sm-6">
                     <div class="form-group">
                        <label for=""> Password</label><input class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" type="password" name="password" required placeholder="Choose a password">
                        <div class="pre-icon os-icon os-icon-fingerprint"></div>
                           @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                     </div>
                  </div>

                  <div class="col-sm-6">
                     <div class="form-group"><label for="">Confirm Password</label><input id="password-confirm" type="password" class="form-control" name="password_confirmation" required placeholder="Type password again"></div>
                  </div>
               </div>
               <div class="buttons-w"><button class="btn btn-primary" type="submit">Register Now</button></div><br>
               <p>Already have an account? <a href="{{url('login')}}">Login</a></p>
            </form>
         </div>
      </div>
   </body>

</html>
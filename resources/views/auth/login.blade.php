<!DOCTYPE html>
<html>
   <!-- Mirrored from light.pinsupreme.com/auth_register.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 04 Nov 2021 14:01:30 GMT -->
   <head>
      <title>Login - Evolution 5G</title>
      <meta charset="utf-8">
      <meta content="ie=edge" http-equiv="x-ua-compatible">
      <meta content="template language" name="keywords">
      <meta content="Evolution 5G" name="author">
      <meta content="Evolution 5G" name="description">
      <meta content="width=device-width,initial-scale=1" name="viewport">
      <link href="favicon.png" rel="shortcut icon">
      <link href="apple-touch-icon.png" rel="apple-touch-icon">
      <link href="../fast.fonts.net/cssapi/487b73f1-c2d1-43db-8526-db577e4c822b.css" rel="stylesheet">
      <link href="/bower_components/select2/dist/css/select2.min.css" rel="stylesheet">
      <link href="/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
      <link href="/bower_components/dropzone/dist/dropzone.css" rel="stylesheet">
      <link href="/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
      <link href="/bower_components/fullcalendar/dist/fullcalendar.min.css" rel="stylesheet">
      <link href="/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css" rel="stylesheet">
      <link href="/bower_components/slick-carousel/slick/slick.css" rel="stylesheet">
      <link href="/css/main5739.css?version=4.5.0" rel="stylesheet">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/4.1.5/css/flag-icons.min.css" integrity="sha512-UwbBNAFoECXUPeDhlKR3zzWU3j8ddKIQQsDOsKhXQGdiB5i3IHEXr9kXx82+gaHigbNKbTDp3VY/G6gZqva6ZQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
   </head>
   <body>
      <div class="all-wrapper menu-side with-pattern">
         <div class="auth-box-w wider">
            <div class="logo-w"><a href="{{url('/')}}"><img alt="" src="/img/logo-big.png"></a></div>
            <h4 class="auth-header">Login</h4>
            <form id="login" action="{{ route('login') }}" method="post" >
            @csrf
             
        
               <div class="form-group">
                  <label for=""> Email address</label><input class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" value="{{ old('email') }}" name="email"  placeholder="Email" type="email">
                  <div class="pre-icon os-icon os-icon-email-2-at2"></div>
                   @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                        @endif
               </div>
         
               <div class="form-group">
                      <label for=""> Password</label><input class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" type="password" name="password" required placeholder="Enter Password">
                        <div class="pre-icon os-icon os-icon-fingerprint"></div>
                           @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                </div>    
              
               <div class="buttons-w">
                  <button class="btn btn-primary">Log me in</button>
                  <div class="form-check-inline"><p><a href="{{ route('password.request') }}">Forgot password?</a></p></div>
               </div>
                <div class="form-check-inline"><p><a href="{{ route('register') }}">Create Account</a></p></div>
            </form>

         </div>
      </div>
   </body>

</html>
    <div class="col-lg-4 col-12">
                    <div class="box">
                        <div class="box-body">
                            <div class="text-center">
                                <img src="../images/svg-icon/sidebar-menu/mailbox.svg" class="rounded svg-icon w-70 mx-auto" alt="">
                                <h3 class="mb-0">{{$file->name}} Proof</h3>
                                <p class="mt-20 mb-5">{{$file->created_at}}</p>
                               
                            </div>
                        </div>
                        <div class="box-footer text-center">
                            <a href="{{ asset('storage/'.$file->filename) }}" class="btn btn-primary"><i class="fa fa-download me-5"></i>View Payment proof</a>
                        </div>
                    </div>
                </div>
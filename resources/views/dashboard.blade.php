@extends('layouts.app')
@section('title', 'Dashboard')
@section('description')
@endsection
@section('content')
<div class="content-box">
   <div class="os-tabs-w">
      <div class="os-tabs-controls os-tabs-complex">
         <ul class="nav nav-tabs">
            <li class="nav-item"><a aria-expanded="false" class="nav-link active" data-toggle="tab" href="#tab_overview"><span class="tab-label">{{Auth::user()->level->name}} <span class="flag-icon flag-icon-{{Auth::user()->area->icon}}"></span></span><span class="badge badge-warning"><i class="os-icon os-icon-plus"></i><span></span></span></a></li>
            <li class="nav-item nav-actions d-none d-sm-block"><a class="btn btn-grey" href="#"><i class="os-icon os-icon-plus-circle"></i><span>Add Wallet</span></a></li>
         </ul>
      </div>
   </div>
   <div class="row">
      @php ($sum = 0)
      @foreach(Auth::user()->listings as $listing)
      @if($listing->type())
      @if($listing->matched())
      @php ($now = \Carbon\Carbon::now())
      @php($days = \Carbon\Carbon::parse($listing->updated_at)->diffInDays($now))
      @php($percentage = $listing->category->parent->percent)
      @php($multiplier = ($listing->amount))
      @php ($sum += ($multiplier)+(($multiplier)*($percentage*$days)))
      @if ($loop->last)
      @endif
      @else
      @endif
      @endif
      @endforeach
      @php ($diff = 0)
      @foreach(Auth::user()->listings as $listing)
      @if($listing->type())
      @if($listing->matched())
      @foreach($listing->comments as $comment)
      @php ($diff += $comment->split)
      @endforeach
      @else
      @endif
      @endif
      @endforeach
      @php ($balance = $sum-$diff)
      <div class="col-sm-12 col-lg-8 col-xxl-6">
         <div class="element-balances justify-content-between mobile-full-width">
            <div class="balance balance-v2">
               <div class="balance-title">Account Balance</div>
               <div class="balance-value"><span class="d-xxl-none">{{ Auth::user()->area->unit }} <?php echo number_format((float)$balance, 2, '.', ''); ?></span><span class="d-none d-xxl-inline-block">{{ Auth::user()->area->unit }} <?php echo number_format((float)$balance, 2, '.', ''); ?></span></div>
            </div>
         </div>
         <div class="element-wrapper pb-4 mb-4 border-bottom">
            <div class="element-box-tp">

               <a class="btn btn-primary" href="{{url('pay')}}"><i class="os-icon os-icon-plus-circle"></i><span>Deposit Money</span></a>

               

               <a class="btn btn-grey" href="{{ route('listings.published.index') }}"><i class="os-icon os-icon-log-out"></i><span>Withdraw</span></a>
           

             


            </div>
         </div>
            <div class="element-wrapper pb-4 mb-4 border-bottom">
            <div class="element-box-tp">

                   <a class="btn btn-warning" href="{{ route('listings.published.index') }}"><i class="os-icon os-icon-money"></i><span> My Transactions</span></a>

               <a class="btn btn-danger" href="{{url('/bonus')}}"><i class="os-icon os-icon-money"></i><span>Bonus Earned</span></a>

              
              
            </div>
         </div>

          <div class="element-wrapper pb-4 mb-4 border-bottom">
            <div class="element-box-tp">

                <a class="btn btn-secondary" href="{{url('/referrals')}}"><i class="os-icon os-icon-money"></i><span>Downliners</span></a>


            

               <a class="btn btn-success" href="{{ route('listings.bcreate', [$area]) }}"><i class="os-icon os-icon-money"></i><span>Withdraw Bonus</span></a>
            </div>
         </div>
          <div class="element-wrapper pb-4 mb-4 border-bottom">
            <div class="element-box-tp">
              <h5>{{$referralink}}</h5>
            </div>
         </div>
         <div class="element-wrapper compact">
            <div class="element-box-tp">
               <h6 class="element-box-header">Balance History</h6>
            </div>
         </div>
      </div>
      <div 
         class="col-sm-2 d-none d-xxl-block">
         <div class="element-box less-padding">
            <h6 class="element-box-header text-center">AFFILIATES</h6>
            <div class="el-chart-w">
               <canvas height="120" id="donutChart1" width="120"></canvas>
               <div class="inside-donut-chart-label"><strong>{{ Auth::user()->referrals()->count()}}</strong><span>Users</span></div>
            </div>
            <div class="el-legend condensed">
               <div class="row">
               </div>
            </div>
         </div>
      </div>
      <div class="col-sm-4 d-none d-lg-block">
         <div class="cta-w cta-with-media purple">
            <div class="cta-content">
               <div class="highlight-header">Example</div>
               <h3 class="cta-header">Download our app on Google Play Store</h3>
               <a class="store-google-btn" href="#"><img alt="" src="img/market-google-play.png"></a>
            </div>
            <div class="cta-media"><img alt="" src="img/phone1.png"></div>
         </div>
      </div>
   </div>
   <div class="row pt-2">
      @php ($diff = 0)
      @foreach(Auth::user()->listings as $listing)
      @if($listing->matched())
      @foreach($listing->comments as $comment)
      @php ($diff += $comment->split)
      @endforeach
      @else
      @endif
      @endforeach
      <div class="col-6 col-sm-3 col-xxl-2">
         <a class="element-box el-tablo centered trend-in-corner smaller" href="#">
            <div class="label">Withdrawal Total</div>
            <div class="value text-danger">{{ Auth::user()->area->unit }} {{$diff}}.00</div>
         </a>
      </div>
      @php ($withdrawn = 0)
      @foreach(Auth::user()->listings as $listing)
      @if($listing->recommit())
      @php ($withdrawn += $listing->amount)
      @if ($loop->last)
      @endif
      @else
      @endif
      @endforeach
      <div class="col-6 col-sm-3 col-xxl-2">
         <a class="element-box el-tablo centered trend-in-corner smaller" href="#">
            <div class="label">Bonus Balance</div>
            <div class="value text-success">{{ Auth::user()->area->unit }} {{\openjobs\Bonus::where('referer_id', auth()->user()->id)->sum('bonus_amount')+\openjobs\Bonus::where('second_gen_id', auth()->user()->id)->sum('second_gen_amount')+\openjobs\Bonus::where('third_gen_id', auth()->user()->id)->sum('third_gen_amount')-($withdrawn)}}.00</div>
         </a>
      </div>
      <div class="col-6 col-sm-3 col-xxl-2">
         <a class="element-box el-tablo centered trend-in-corner smaller" href="#">
            <div class="label">Pay Level</div>
            <div class="value">${{ Auth::user()->level->pay}}</div>
         </a>
      </div>
      <div class="col-6 col-sm-3 col-xxl-2">
         <a class="element-box el-tablo centered trend-in-corner smaller" href="#">
            <div class="label">Downliners</div>
            <div class="value"> {{ Auth::user()->referrals()->count()}}</div>
         </a>
      </div>
   </div>
   <div class="row">
      <div class="col-sm-8">
         <div class="element-wrapper compact pt-4">
            <div class="element-actions d-none d-sm-block"><a class="btn btn-success btn-sm" href="{{url('referrals')}}"><i class="os-icon os-icon-grid-10"></i><span>View All </span></a></div>
            <h6 class="element-header">Users Under You</h6>
            <div class="element-box-tp">
               <div class="inline-profile-tiles" >
                  <div class="row">
                     @foreach ($referrals as $ref)
                     <div class="col-4 col-sm-3 col-xxl-2">
                           <a class="profile-tile-box" href="{{route('profile.index', ['email'=>$ref->email])}}" style="color: {{$ref->level->color}};">
                           
                        <div class="profile-tile profile-tile-inlined"  href="{{route('profile.index', ['email'=>$ref->email])}}">
                              <div class="pt-avatar-w"  ><img alt="" src="/uploads/avatars/{{ $ref->avatar }}"></div>

                              <div style="color: black;">{{$ref->name}} {{$ref->surname}}</div>
                              <span class="flag-icon flag-icon-{{$ref->area->icon}}"></span>
                              <hr>
                              <div style="color: black;">
                                 {{$ref->level->name}}
                              </div>
                            </a>
                        </div>

                     </div>
                     @endforeach
                     <div class="col-4 col-sm-3 col-xxl-2">
                        <div class="profile-tile profile-tile-inlined">
                           <a class="profile-tile-box faded" href="{{url('referrals')}}">
                              <div class="pt-new-icon"><i class="os-icon os-icon-eye"></i></div>
                              <div class="pt-user-name">View<br> All</div>
                           </a>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
  
</div>
@include('layouts.partials.rightsidebar')
@endsection
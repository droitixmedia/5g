 
@extends('layouts.homeapp')

@section('title', 'Dashboard')

@section('description')

@endsection

@section('content')


<section class="faq pt-125 pb-100">
            <div class="container">
              <div class="row">
                <div class="col-lg-6 offset-lg-3">
                  <div class="section-heading text-center mb-50">
                    <span class="section-heading__title_small">
                      Your Testimonies
                    </span>
                    
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-lg-6 mb-30">
                  <div class="faq-accordion">
                    <div class="accordion" id="accordionExample">
                       @foreach($testimonies as $testimony)
                      <div class="accordion-item">
                        <h2 class="accordion-header" id="headingOne">
                         
                          <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                           {{$testimony->body}}
                          </button>

                        </h2>
                        
                      </div>
                     @endforeach
                    </div>
                  </div>
                </div>
                
              </div>
            </div>
          </section>             

@endsection
 
@extends('layouts.homeapp')

@section('title', 'Dashboard')

@section('description')

@endsection

@section('content')

<section class="contact contact--agency" data-bg-image="/assets/images/bg/counter-area-bg-1.jpg">
                  <div class="container">
                     <div class="row">
                        <div class="col-lg-6 offset-lg-3">
                           <div class="contact__agency">
                               <h2 class="heading text-white">About Us</h2>
                              <span class="title text-white">Evolution 5G</span>
                           
                              <a href="contact.html" class="btn btn--common btn--default btn--primary">Join Us</a>
                           </div>
                        </div>
                     </div>
                  </div>
               </section>
<section class="services services--details pt-130 pb-100">
                  <div class="container">
                     <div class="row">
                        <div class="col-lg-8 mb-30">
                           <div class="services__details">
                              <div class="image image--big"><img src="/assets/images/home1/celltower.jpg" alt="IT Management"> <i class="flaticon flaticon-management icon wow fadeInLeft animated" data-wow-delay=".3s" data-wow-durations="1.5s"></i></div>
                              <div class="content mb-30">
                                 <h2 class="heading heading--big">About 5G Evolution</h2>
                                 <p class="paragraph">Founded in 1998 as Evolution Networks and rebranded in 2014 to become Evolution 5G ,Evolution 5G is a leading global provider of 5G Network and communications technology (ICT) infrastructure and smart devices. We have approximately 100,000 customers and we operate in over 170 countries and regions, serving more than three 3 Million people around the world.</p>
                                 <p class="paragraph">Evolution 5G’s mission is to bring 5G to every person, home and organization for a fully connected, intelligent world. 

 </p>

 <p class="paragraph">To this end, we are a leader in portable connectivity and promote equal access to networks to lay the foundation for the intelligent world.We offer consumers more personalized and intelligent experiences across all scenarios, including home, travel, office, entertainment, and fitness & health.</p>
                                 
                              </div>
                              <div class="content mb-15">
                                 <h2 class="heading heading--small">Evolution 5G Shareholder Ecosystem</h2>
                                 <p class="paragraph">Evolution 5G has a profit sharing shareholder program, that allows someone to become a shareholder by buying a  Boost Package and end up benefiting from our operations worldwide.</p>
                                 <div class="row">
                                    <div class="col-lg-6 mb-30">
                                       <div class="image image--small"><img src="/assets/images/thumbs/services-image-small-1.jpg" alt="IT Management"></div>
                                    </div>
                                    
                                 </div>
                              </div>
                            
                           </div>
                        </div>
                        <div class="col-lg-4 mb-30">
                           <div class="services__sidebar sidebar">
                              <div class="widget">
                                 
                                 <div class="brouchers mb-30">
                                    <h3 class="heading">Documents</h3>
                                    <ul>
                                       <li class="item">
                                          <div class="brouchers-icon"><i class="flaticon flaticon-pdf-file"></i></div>
                                          <div class="brouchers-text">
                                             <h6 class="heading">Download Agent Application Form</h6>
                                             <a class="link" href="/docs/Evolution 5G Application Form.pdf">DOWNLOAD</a>
                                          </div>
                                       </li>
                                       <li class="item">
                                          <div class="brouchers-icon"><i class="flaticon flaticon-pdf-file"></i></div>
                                          <div class="brouchers-text">
                                             <h6 class="heading">Download Positions and Benefits</h6>
                                             <a class="link" href="/docs/5G EVOLUTION POSITIONS AND BENEFITS.pdf">DOWNLOAD</a>
                                          </div>
                                       </li>
                                       <li class="item">
                                          <div class="brouchers-icon"><i class="flaticon flaticon-file"></i></div>
                                          <div class="brouchers-text">
                                             <h6 class="heading">Boost Packages</h6>
                                             <a class="link" href="/docs/BOOST PACKAGES.pdf">DOWNLOAD</a>
                                          </div>
                                       </li>
                                    </ul>
                                 </div>
                                 <div class="contact-address contact-address--bg" data-bg-image="/assets/images/home1/celltower.jpg">
                                    <div class="person">
                                     
                                       <h3 class="heading text-white">Contact Us</h3>
                                       <span class="phone text-white">+27642971130</span> <span class="email text-white">Mail: support@evolution5g.org</span>
                                       <div class="social">
                                          <ul>
                                             <li><a class="facebook" href="javascript:void(0);"><i class="fab fa-facebook-f"></i></a></li>
                                             <li><a class="twitter" href="javascript:void(0);"><i class="fab fa-twitter"></i></a></li>
                                             <li><a class="instagram" href="javascript:void(0);"><i class="fab fa-instagram"></i></a></li>
                                             <li><a class="linkedin" href="javascript:void(0);"><i class="fab fa-linkedin-in"></i></a></li>
                                          </ul>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </section>               

@endsection
@extends('layouts.app')

@section('title', 'Dashboard')

@section('description')

@endsection

@section('content')

<div class="content-box">
                     
                    
                     <div class="element-wrapper">
                        <h6 class="element-header">Detailed Bonus</h6>
                        <div class="element-box-tp">
                           <div class="table-responsive">
                              <table class="table table-padded">
                                 <thead>
                                    <tr>
                                       
                                       <th>Name</th>
                                       <th>Bonus Type</th>
                                       
                                     
                                       <th>Amount</th>
                                       <th>Date</th>
                                        
                                      
                                     
                                    </tr>
                                 </thead>
                                 <tbody>
                                   @foreach(openjobs\Bonus::where('referer_id', auth()->user()->id)->get() as $bonus) 
                                    <tr>
                                       <td class="cell-with-media"> <a href="{{route('profile.index', ['email'=>$bonus->email])}}"><img alt="" src="/uploads/avatars/{{ $bonus->avatar }}" style="height: 25px;"><span>{{ $bonus->name }} {{ $bonus->surname }}</span></a></td>
                                      <td>Direct Bonus</td>
                                       
                                      
                                         <td class="text-left bolder nowrap"><span class="text-success">+ {{ Auth::user()->area->unit }}{{$bonus->bonus_amount}}</span></td>
                                         <td class="text-left bolder nowrap"><span class="text-warning">{{$bonus->created_at}}</span></td>
                                       </tr>
                                      @endforeach

                                       @foreach(openjobs\Bonus::where('second_gen_id', auth()->user()->id)->get() as $bonus) 
                                    <tr>
                                       <td class="cell-with-media"> <a href="{{route('profile.index', ['email'=>$bonus->email])}}"><img alt="" src="/uploads/avatars/{{ $bonus->avatar }}" style="height: 25px;"><span>{{ $bonus->name }} {{ $bonus->surname }}</span></a></td>
                                      <td>Second Generation</td>
                                       
                                      
                                         <td class="text-left bolder nowrap"><span class="text-success">+ {{ Auth::user()->area->unit }}{{$bonus->second_gen_amount}}</span></td>
                                         <td class="text-left bolder nowrap"><span class="text-warning">{{$bonus->created_at}}</span></td>
                                       </tr>
                                      @endforeach
                                       @foreach(openjobs\Bonus::where('third_gen_id', auth()->user()->id)->get() as $bonus) 
                                    <tr>
                                       <td class="cell-with-media"> <a href="{{route('profile.index', ['email'=>$bonus->email])}}"><img alt="" src="/uploads/avatars/{{ $bonus->avatar }}" style="height: 25px;"><span>{{ $bonus->name }} {{ $bonus->surname }}</span></a></td>
                                      <td>Third Generation</td>
                                       
                                      
                                         <td class="text-left bolder nowrap"><span class="text-success">+ {{ Auth::user()->area->unit }}{{$bonus->third_gen_amount}}</span></td>
                                         <td class="text-left bolder nowrap"><span class="text-warning">{{$bonus->created_at}}</span></td>
                                       </tr>
                                      @endforeach
                                   
                                   
                                   
                                 </tbody>
                              </table>
                           </div>
                        </div>
                     </div>
                     
                  </div>
@endsection

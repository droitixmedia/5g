 
@extends('layouts.homeapp')

@section('title', 'Dashboard')

@section('description')

@endsection

@section('content')


<section class="services services--details pt-130 pb-100">
                  <div class="container">
                     <div class="row">
                        <div class="row">
                        <div class="col-lg-4 col-md-6 mb-30">
                           <div class="contact-item">
                              <img class="icon" style="font-size: 50px;" src="assets/images/icon/phone-call.png" alt="">
                              <h3 class="title title--heading">Call</h3>
                              <p class="address"><a class="link" href="tel:"></a></p>
                              
                           </div>
                        </div>
                        <div class="col-lg-4 col-md-6 mb-30">
                           <div class="contact-item">
                              <img class="icon" style="font-size: 50px;" src="assets/images/icon/whatsapp.png" alt="">
                              <h3 class="title title--heading">WhatsApp Us</h3>
                              <p class="address"><a class="link" href="tel:"></a></p>
                             

                           </div>
                        </div>
                        <div class="col-lg-4 col-md-6 mb-30">
                           <div class="contact-item">
                              <img class="icon" src="assets/images/icon/contact-email.png" alt="">
                              <h3 class="title title--heading">Email Us</h3>
                              <p class="address"><a class="link" href="email:support@evolution5g.org">support@evolution5g.org</a></p>
                              
                           </div>
                        </div>
                        <div class="col-lg-4 col-md-6 mb-30">
                           <div class="contact-item">
                              <img class="icon" src="assets/images/icon/telegram.png" alt="">
                              <h3 class="title title--heading">Telegram Channel</h3>
                              <p class="address"><a href="contact.html" class="btn btn--common  btn--warning rt-button-animation-out">
                      Join Telegram Channel
                    </a></p>
                              
                           </div>
                        </div>
                         <div class="col-lg-4 col-md-6 mb-30">
                           <div class="contact-item">
                              <img class="icon" src="assets/images/icon/instagram.png" alt="">
                              <h3 class="title title--heading">Instagram </h3>
                              <p class="address"><a href="https://www.instagram.com/evolution5g/" class="btn btn--common  btn--warning rt-button-animation-out">
                      Follow Us
                    </a></p>
                              
                           </div>
                            
                    
                       
                     </div>
                     <div class="col-lg-4 col-md-6 mb-30">
                           <div class="contact-item">
                              <img class="icon" src="assets/images/icon/facebook.png" alt="">
                              <h3 class="title title--heading">Facebook </h3>
                              <p class="address"><a href="https://www.facebook.com/Evolution5G-102650622290267/" class="btn btn--common  btn--warning rt-button-animation-out">
                      Like our page
                    </a></p>
                              
                           </div>
                            
                    
                       
                     </div>
                      <div class="col-lg-4 col-md-6 mb-30">
                           <div class="contact-item">
                              <img class="icon" src="assets/images/icon/youtube.png" alt="">
                              <h3 class="title title--heading">Youtube </h3>
                              <p class="address"><a href="https://www.youtube.com/channel/UC6E2XiKjVf0iVwq5GrTSp5Q" class="btn btn--common  btn--warning rt-button-animation-out">
                      Subscribe
                    </a></p>
                              
                           </div>
                            
                    
                       
                     </div>
                        <div class="col-lg-4 col-md-6 mb-30">
                           <div class="contact-item">
                              <img class="icon" src="assets/images/icon/twitter.png" alt="">
                              <h3 class="title title--heading">Twitter</h3>
                              <p class="address"><a href="contact.html" class="btn btn--common  btn--warning rt-button-animation-out">
                      Follow Us
                    </a></p>
                              
                           </div>
                            
                    
                       
                     </div>
                      <div class="col-lg-4 col-md-6 mb-30">
                           <div class="contact-item">
                              <img class="icon" src="assets/images/icon/tiktok.png" alt="">
                              <h3 class="title title--heading">Tiktok</h3>
                              <p class="address"><a href="contact.html" class="btn btn--common  btn--warning rt-button-animation-out">
                      Follow Us
                    </a></p>
                              
                           </div>
                            
                    
                       
                     </div> 
                     </div>
                  </div>
               </section>               

@endsection
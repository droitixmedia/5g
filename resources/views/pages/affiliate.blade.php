 
@extends('layouts.homeapp')

@section('title', 'Dashboard')

@section('description')

@endsection

@section('content')

             
               <section class="services services--padding section-bg-common">
                  <div class="container">
                     <div class="row align-items-center">
                        <div class="col-lg-6 offset-lg-3">
                           <div class="section-heading mb-45 text-center">
                              <span class="section-heading__title_small">Levels</span>
                              <h2 class="section-heading__title_big">Levels of Commision<br>and Benefits</h2>
                           </div>
                        </div>
                     </div>
                     <div class="row">
                        
                        <div class="col-lg-4 col-md-6 wow fadeInUp" data-wow-delay=".2s">
                           <div class="services__item mb-50">
                              <div class="services__item_overlay"></div>
                              <header class="services__item_header d-flex align-items-center">
                                <img src="/img/levels/agent.png" alt="vip" width="150" height="150">
                                 <h3 class="services__item_title"><a href="services-details.html">Agent</a></h3>
                              </header>
                              <div class="services__item_content">
                                 <p class="services__item_paragraph">1.Activate your account by buying a boost package.<br>
2.Have at least 5 active members in your structure.<br>
3.Earn 10% unlimited referral bonus every PRODUCT/ BOOSTER PACKAGE you sell.<br>
4.Contact your Supervisor for further assistance.</p>
                              </div>
                               
                           </div>
                        </div>
                        <div class="col-lg-4 col-md-6 wow fadeInUp" data-wow-delay=".4s">
                           <div class="services__item mb-50">
                              <div class="services__item_overlay"></div>
                              <header class="services__item_header d-flex align-items-center">
                                 <img src="/img/levels/supervisor.png" alt="vip" width="150" height="150">
                                 <h3 class="services__item_title"><a href="services-details.html">Supervisor</a></h3>
                              </header>
                              <div class="services__item_content">
                                 <p class="services__item_paragraph">1.Your account must have a minimum of $100.<br>
2.Have a minimum of 20 active AGENTS.<br>
3.Do zoom meetings wth your team a minimum of 2 days per week. All recordings to be
submited to your COORDINATOR.<br>
4.Free EVOLUTION SUPERVISOR T-Shirt and CUP will be courried to you.<br>
5.Earn 12% unlimited referral bonus per every PRODUCT/BOOSTER PACKAGE you sell.<br></p>
                              </div>
                              
                           </div>
                        </div>
                        <div class="col-lg-4 col-md-6 wow fadeInUp" data-wow-delay=".6s">
                           <div class="services__item mb-50">
                              <div class="services__item_overlay"></div>
                              <header class="services__item_header d-flex align-items-center">
                                   <img src="/img/levels/coordinator.png" alt="vip" width="150" height="150">
                                 <h3 class="services__item_title"><a href="services-details.html">Cordinator</a></h3>
                              </header>
                              <div class="services__item_content">
                                 <p class="services__item_paragraph">1.A minimum of $200 in your account.<br>
2.Earn 15% unlimited bonus per every PRODUCT/BOOSTER PACKAGE you sell.<br>
3.Free EVOLUTION COORDINATOR T-Shirt and CUP will be courried to you.<br>
4.Have at least 50 active Agents in your team.<br>
5.You must have 5 supervisors in your team.<br>
6.Educate your team about the business.<br>
7.Do follow ups and assist your team.<br>
8.You qualify for Provincial vacatons once a month, all expenses paid oﬀ with the company.</p>
                              </div>
                             
                           </div>
                        </div>
                        <div class="col-lg-4 col-md-6 wow fadeInUp" data-wow-delay=".8s">
                           <div class="services__item mb-50">
                              <div class="services__item_overlay"></div>
                              <header class="services__item_header d-flex align-items-center">
                                   <img src="/img/levels/manager.png" alt="vip" width="150" height="150">
                                 <h3 class="services__item_title"><a href="services-details.html">Manager</a></h3>
                              </header>
                              <div class="services__item_content">
                                 <p class="services__item_paragraph">1.A minimum of $300 in your account.<br>
2.Earn 20% unlimited bonus per every PRODUCT/BOOSTER PACKAGE.<br>
3.Must Have 150 Active Agents in your team.<br>
4.Must have 10 COORDINATORS in your team .<br>
5.Free EVOLUTION CELL PHONE.<br>
6.Qualifying for all Province vacatons once per month, all expenses paid of with the
company.<br>
7.Free EVOLUTION MANAGER T-Shirt and CUP will be courried to you.<br>
8.Educate your team about the business.<br></p>
                              </div>
                           
                           </div>
                        </div>
                        <div class="col-lg-4 col-md-6 wow fadeInUp" data-wow-delay="1s">
                           <div class="services__item mb-50">
                              <div class="services__item_overlay"></div>
                              <header class="services__item_header d-flex align-items-center">
                                 <img src="/img/levels/ambassador.png" alt="vip" width="150" height="150">
                                 <h3 class="services__item_title"><a href="services-details.html">Brand Ambassador</a></h3>
                              </header>
                              <div class="services__item_content">
                                 <p class="services__item_paragraph">1.A minimum of $500 in your account.<br>
2.Earn unlimited 25% referral bonus per every PRODUCT/BOOSTER PACKAGE you sell.<br>
3.Free EVOLUTION LAPTOP T-Shirt and CUP will be courried to you.<br>
4.You qualify for vacations out of the country, all expenses paid of by the company .<br>
5.A minimum of 300 AGENTS and 20 MANAGERS in your team.</p>
                              </div>
                           
                           </div>
                        </div>
                       
                     </div>
                  </div>
               </section>
              
              
        

@endsection
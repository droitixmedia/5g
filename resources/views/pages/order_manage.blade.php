@extends('layouts.app')

@section('title', 'Dashboard')

@section('description')

@endsection

@section('content')

<div class="content-box">
                     
                    
                    <div class="element-wrapper">
   <div class="element-actions">
      
   </div>
   <h6 class="element-header">Manage Orders</h6>
   <div class="element-content">
      <div class="row">
         <div class="col-sm-4 col-xxxl-3">
            <a class="element-box el-tablo" href="{{ url('admin/listings')}}">
             
               <div class="value">Unpaid Requests</div>
               
            </a>
         </div>
         <div class="col-sm-4 col-xxxl-3">
            <a class="element-box el-tablo" href="{{ url('admin/investments')}}">
              
               <div class="value">Paid Requests</div>
              
            </a>
         </div>
         <div class="col-sm-4 col-xxxl-3">
            <a class="element-box el-tablo" href="{{ url('admin/withdrawals')}}">
              
               <div class="value">Withdrawal Requests</div>
              
            </a>
         </div>
         <div class="col-sm-4 col-xxxl-3">
            <a class="element-box el-tablo" href="{{ url('admin/bonus')}}">
              
               <div class="value">Bonus Withdrawal</div>
              
            </a>
         </div>
         
      </div>
   </div>
</div>
                     
                  </div>
@endsection

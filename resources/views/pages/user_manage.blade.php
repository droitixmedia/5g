@extends('layouts.app')

@section('title', 'Dashboard')

@section('description')

@endsection

@section('content')

<div class="content-box">
                     
                    
                    <div class="element-wrapper">
   <div class="element-actions">
      
   </div>
   <h6 class="element-header">Manage Users</h6>
   <div class="element-content">
      <div class="row">
         <div class="col-sm-4 col-xxxl-3">
            <a class="element-box el-tablo" href="{{url('/admin/users')}}">
             
               <div class="value">All Users</div>
               
            </a>
         </div>
         <div class="col-sm-4 col-xxxl-3">
            <a class="element-box el-tablo" href="{{url('/admin/members')}}">
              
               <div class="value">Members</div>
              
            </a>
         </div>
         <div class="col-sm-4 col-xxxl-3">
            <a class="element-box el-tablo" href="{{url('/admin/agents')}}">
              
               <div class="value">Agents</div>
              
            </a>
         </div>
         <div class="col-sm-4 col-xxxl-3">
            <a class="element-box el-tablo" href="{{url('/admin/supervisors')}}">
              
               <div class="value">Supervisors</div>
              
            </a>
         </div>
         <div class="col-sm-4 col-xxxl-3">
            <a class="element-box el-tablo" href="{{url('/admin/cordinators')}}">
              
               <div class="value">Coordinators</div>
              
            </a>
         </div>
         <div class="col-sm-4 col-xxxl-3">
            <a class="element-box el-tablo" href="{{url('/admin/managers')}}">
              
               <div class="value">Managers</div>
              
            </a>
         </div>
         
         <div class="col-sm-4 col-xxxl-3">
            <a class="element-box el-tablo" href="{{url('/admin/ambassadors')}}">
              
               <div class="value">Ambassadors</div>
              
            </a>
         </div>
         <div class="col-sm-4 col-xxxl-3">
            <a class="element-box el-tablo" href="{{url('/admin/directors')}}">
              
               <div class="value">Directors</div>
              
            </a>
         </div>
      </div>
   </div>
</div>
                     
                  </div>
@endsection

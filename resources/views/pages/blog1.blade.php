 
@extends('layouts.homeapp')

@section('title', 'Dashboard')

@section('description')

@endsection

@section('content')

<section class="contact contact--agency" data-bg-image="/assets/images/bg/counter-area-bg-1.jpg">
                  <div class="container">
                     <div class="row">
                        <div class="col-lg-6 offset-lg-3">
                           <div class="contact__agency">
                              <span class="title text-white">Fast network for convinience</span>
                              <h2 class="heading text-white">5G Evolution</h2>
                              <a href="contact.html" class="btn btn--common btn--default btn--primary">Join Us</a>
                           </div>
                        </div>
                     </div>
                  </div>
               </section>
<section class="services services--details pt-130 pb-100">
                  <div class="container">
                     <div class="row">
                        <div class="col-lg-8 mb-30">
                           <div class="services__details">
                              <div class="image image--big"><img src="/assets/images/blog/blog-thumb-1.jpg" alt="IT Management"> <i class="flaticon flaticon-management icon wow fadeInLeft animated" data-wow-delay=".3s" data-wow-durations="1.5s"></i></div>
                              <div class="content mb-30">
                                 <h2 class="heading heading--big">Agents</h2>
                                 <p class="paragraph">Join today and become a 5G evolution Commission agent and start eraning commision whenever those you registered buy a product or participate in the 5G evolution Boost Ecosystem Package.</p>
                                 <p class="paragraph">All 5G evolution Agents positions are upgradable up to the Ambassador stage, which allows you to controll a whole products destribution line.Stages are as below</p>
                                 <div class="list">
                                    <ul class="clearfix">
                                       <li><i class="fas fa-check-circle"></i>Agent</li>
                                       <li><i class="fas fa-check-circle"></i>Supervisor</li>
                                       <li><i class="fas fa-check-circle"></i>Manager</li>
                                       <li><i class="fas fa-check-circle"></i>Cordinator</li>
                                       <li><i class="fas fa-check-circle"></i>Brand Ambassador</li>
                                       
                                    </ul>
                                 </div>
                              </div>
                              <div class="content mb-15">
                                 <h2 class="heading heading--small">Benefits of being an Agent</h2>
                                 <p class="paragraph">Grursus mal suada faci lisis Lorem ipsum dolarorit more ametion consectetur elit. Vesti at bulum nec odio aea the dumm ipsumm ipsum that dolocons rsus mal suada and fadolorit to the consectetur elit. All the mor Lorem Ipsum generators on the Internet tend to repeat that predefined chunks as necessary, making this the first true dummy generator on the Internet.</p>
                                 <div class="row">
                                    <div class="col-lg-6 mb-30">
                                       <div class="image image--small"><img src="assets/images/thumbs/services-image-small-1.jpg" alt="IT Management"></div>
                                    </div>
                                    <div class="col-lg-6 mb-30">
                                       <div class="image image--small"><img src="assets/images/thumbs/services-image-small-2.jpg" alt="IT Management"></div>
                                    </div>
                                 </div>
                              </div>
                            
                           </div>
                        </div>
                        <div class="col-lg-4 mb-30">
                           <div class="services__sidebar sidebar">
                              <div class="widget">
                                 
                                 <div class="brouchers mb-30">
                                    <h3 class="heading">Documents</h3>
                                    <ul>
                                       <li class="item">
                                          <div class="brouchers-icon"><i class="flaticon flaticon-pdf-file"></i></div>
                                          <div class="brouchers-text">
                                             <h6 class="heading">Download Agent Application Form</h6>
                                             <a class="link" href="#">DOWNLOAD</a>
                                          </div>
                                       </li>
                                       <li class="item">
                                          <div class="brouchers-icon"><i class="flaticon flaticon-file"></i></div>
                                          <div class="brouchers-text">
                                             <h6 class="heading">Agents Commission Paper</h6>
                                             <a class="link" href="#">DOWNLOAD</a>
                                          </div>
                                       </li>
                                    </ul>
                                 </div>
                                 <div class="contact-address contact-address--bg" data-bg-image="/assets/images/home1/celltower.jpg">
                                    <div class="person">
                                     
                                       <h3 class="heading text-white">Contact Us</h3>
                                       <span class="phone text-white">+27877355361</span> <span class="email text-white">Mail: support@evolution5g.org</span>
                                       <div class="social">
                                          <ul>
                                             <li><a class="facebook" href="javascript:void(0);"><i class="fab fa-facebook-f"></i></a></li>
                                             <li><a class="twitter" href="javascript:void(0);"><i class="fab fa-twitter"></i></a></li>
                                             <li><a class="instagram" href="javascript:void(0);"><i class="fab fa-instagram"></i></a></li>
                                             <li><a class="linkedin" href="javascript:void(0);"><i class="fab fa-linkedin-in"></i></a></li>
                                          </ul>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </section>               

@endsection
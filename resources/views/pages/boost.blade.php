 
@extends('layouts.homeapp')

@section('title', 'Dashboard')

@section('description')

@endsection

@section('content')


<section class="services services--details pt-130 pb-100">
                  <div class="container">
                     <div class="row">
                        <div class="col-lg-8 mb-30">
                           <div class="services__details">
                             
                              <div class="content mb-30">
                                 <h2 class="heading heading--big">1.Starter Package</h2>
                                 <p class="paragraph">
                                   -5% Daily Returns<br>
                                   -For R500 or $25 minimum

                                 </p>
                                 <h2 class="heading heading--big">2.Bronze Package</h2>
                                 <p class="paragraph">
                                   -6% Daily Returns<br>

                                   -For R2000 or $100 minimum

                                 </p>
                                 <h2 class="heading heading--big">3.Silver Package</h2>
                                 <p class="paragraph">
                                   -7% Daily Returns<br>

                                   -For R5000 or $300 minimum

                                 </p>
                                 <h2 class="heading heading--big">4.Gold Package</h2>
                                 <p class="paragraph">
                                   -8% Daily Returns<br>

                                   -For R10000 or $625 minimum

                                 </p>
                                 <h2 class="heading heading--big">5.Black Card</h2>
                                 <p class="paragraph">
                                   -9% Daily Returns<br>

                                   -For R20000 or $1200 minimum

                                 </p>
                                 <h2 class="heading heading--big">6.5G Ultimate</h2>
                                 <p class="paragraph">
                                   -10% Daily Returns<br>

                                   -For R55000 or $3500 minimum

                                 </p>
                               



                              </div>
                            
                           </div>
                        </div>
                        <div class="col-lg-4 mb-30">
                           <div class="services__sidebar sidebar">
                              <div class="widget">
                                 
                                 <div class="brouchers mb-30">
                                    <h3 class="heading">Documents</h3>
                                    <ul>
                                       <li class="item">
                                          <div class="brouchers-icon"><i class="flaticon flaticon-pdf-file"></i></div>
                                          <div class="brouchers-text">
                                             <h6 class="heading">Download Agent Application Form</h6>
                                             <a class="link" href="#">DOWNLOAD</a>
                                          </div>
                                       </li>
                                       <li class="item">
                                          <div class="brouchers-icon"><i class="flaticon flaticon-file"></i></div>
                                          <div class="brouchers-text">
                                             <h6 class="heading">Agents Commission Paper</h6>
                                             <a class="link" href="#">DOWNLOAD</a>
                                          </div>
                                       </li>
                                    </ul>
                                 </div>
                                 <div class="contact-address contact-address--bg" data-bg-image="/assets/images/home1/celltower.jpg">
                                    <div class="person">
                                     
                                       <h3 class="heading text-white">Contact Us</h3>
                                       <span class="phone text-white">+27877355361</span> <span class="email text-white">Mail: support@evolution5g.org</span>
                                       <div class="social">
                                          <ul>
                                             <li><a class="facebook" href="https://www.facebook.com/Evolution5G-102650622290267/"><i class="fab fa-facebook-f"></i></a></li>
                                             <li><a class="twitter" href="javascript:void(0);"><i class="fab fa-twitter"></i></a></li>
                                             <li><a class="instagram" href="https://www.instagram.com/evolution5g/"><i class="fab fa-instagram"></i></a></li>
                                             <li><a class="linkedin" href="javascript:void(0);"><i class="fab fa-linkedin-in"></i></a></li>
                                          </ul>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </section>               

@endsection
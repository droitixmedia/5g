<!DOCTYPE html>
<html lang="en">
<head>

   @include('layouts.partials.homehead')

</head>
 <body>
      <div class="loader">
         <div class="cssload-loader">
            <div class="cssload-inner cssload-one"></div>
            <div class="cssload-inner cssload-two"></div>
            <div class="cssload-inner cssload-three"></div>
         </div>
      </div>
       
 
  <div id="app">
   



   @include('layouts.partials.homenavigation')




       @yield('content')


      

    
        </div>
      
    </div>

   @include('layouts.partials.homefooter')
      
   <div class="progress-wrap">
         <svg class="progress-circle svg-content" width="100%" height="100%" viewBox="-1 -1 102 102">
            <path d="M50,1 a49,49 0 0,1 0,98 a49,49 0 0,1 0,-98"/>
         </svg>
      </div>
      <script src="assets/js/jquery.min.js"></script><script src="assets/dependencies/bootstrap/js/bootstrap.bundle.min.js"></script><script src="assets/dependencies/slick/slick.min.js"></script><script src="assets/dependencies/animation/wow.min.js"></script><script src="assets/dependencies/parallax/parallaxie.js"></script><script src="assets/dependencies/greensock/TweenMax.min.js"></script><script src="assets/dependencies/counter-up/jquery.counterup.min.js"></script><script src="assets/dependencies/waypoints/jquery.waypoints.min.js"></script><script src="assets/dependencies/niceselect/jquery.nice-select.min.js"></script><script src="assets/dependencies/anime/anime.min.js"></script><script src="assets/dependencies/imagesloaded/imagesloaded.pkgd.min.js"></script><script src="assets/dependencies/tilt/vanilla-tilt.min.js"></script><script src="assets/dependencies/fancybox/jquery.fancybox.min.js"></script><script src="../../../../unpkg.com/swiper%407.2.0/swiper-bundle.min.js"></script><script src="assets/dependencies/isotope/isotope.pkgd.min.js"></script><script src="assets/dependencies/isotope/imagesloaded.pkgd.min.js"></script><script src="assets/js/main.js"></script><script src="assets/js/custom.js"></script>
      <!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/61df1e50f7cf527e84d1d2d6/1fp7osrtf';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->
    
</body>
</html>

<!DOCTYPE html>
<html>
 
   <head>
      <title>Evolution 5G | Account</title>
      <meta content="ie=edge" http-equiv="x-ua-compatible">
      <meta content="template language" name="keywords">
      <meta content="Tamerlan Soziev" name="author">
      <meta content="Admin dashboard html template" name="description">
      <meta content="width=device-width,initial-scale=1" name="viewport">
      <link href="/img/logo-big.png" rel="shortcut icon">
      <link href="apple-touch-icon.png" rel="apple-touch-icon">
      <link href="/fast.fonts.net/cssapi/487b73f1-c2d1-43db-8526-db577e4c822b.css" rel="stylesheet">
      <link href="/bower_components/select2/dist/css/select2.min.css" rel="stylesheet">
      <link href="/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
      <link href="/bower_components/dropzone/dist/dropzone.css" rel="stylesheet">
      <link href="/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
      <link href="/bower_components/fullcalendar/dist/fullcalendar.min.css" rel="stylesheet">
      <link href="/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css" rel="stylesheet">
      <link href="/bower_components/slick-carousel/slick/slick.css" rel="stylesheet">
      <link href="/css/main5739.css?version=4.5.0" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/4.1.5/css/flag-icons.min.css" integrity="sha512-UwbBNAFoECXUPeDhlKR3zzWU3j8ddKIQQsDOsKhXQGdiB5i3IHEXr9kXx82+gaHigbNKbTDp3VY/G6gZqva6ZQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
   </head>
   <body class="menu-position-side menu-side-left full-screen with-content-panel ">
      <div class="all-wrapper with-side-panel solid-bg-all color-scheme-dark">
        
         <div class="layout-w">

           

            @include('layouts.partials.sidebar')
           
            <div class="content-w">

              @include('layouts.partials.navigation')
              
              @include('layouts.partials.mobilenavigation')

             

               <div class="content-panel-toggler"><i class="os-icon os-icon-grid-squares-22"></i><span>Sidebar</span></div>
               <div class="content-i">

                @yield('content')
                 
                  

               </div>
            </div>
         </div>
         <div class="display-type"></div>
      </div>
       <script src="/bower_components/jquery/dist/jquery.min.js"></script><script src="/bower_components/popper.js/dist/umd/popper.min.js"></script><script src="/bower_components/moment/moment.js"></script><script src="/bower_components/chart.js/dist/Chart.min.js"></script><script src="/bower_components/select2/dist/js/select2.full.min.js"></script><script src="/bower_components/jquery-bar-rating/dist/jquery.barrating.min.js"></script><script src="/bower_components/ckeditor/ckeditor.js"></script><script src="/bower_components/bootstrap-validator/dist/validator.min.js"></script><script src="/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script><script src="/bower_components/ion.rangeSlider/js/ion.rangeSlider.min.js"></script><script src="/bower_components/dropzone/dist/dropzone.js"></script><script src="/bower_components/editable-table/mindmup-editabletable.js"></script><script src="/bower_components/datatables.net/js/jquery.dataTables.min.js"></script><script src="/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script><script src="/bower_components/fullcalendar/dist/fullcalendar.min.js"></script><script src="/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js"></script><script src="/bower_components/tether/dist/js/tether.min.js"></script><script src="/bower_components/slick-carousel/slick/slick.min.js"></script><script src="/bower_components/bootstrap/js/dist/util.js"></script><script src="/bower_components/bootstrap/js/dist/alert.js"></script><script src="/bower_components/bootstrap/js/dist/button.js"></script><script src="/bower_components/bootstrap/js/dist/carousel.js"></script><script src="/bower_components/bootstrap/js/dist/collapse.js"></script><script src="/bower_components/bootstrap/js/dist/dropdown.js"></script><script src="/bower_components/bootstrap/js/dist/modal.js"></script><script src="/bower_components/bootstrap/js/dist/tab.js"></script><script src="/bower_components/bootstrap/js/dist/tooltip.js"></script><script src="/bower_components/bootstrap/js/dist/popover.js"></script><script src="/js/demo_customizer5739.js?version=4.5.0"></script><script src="/js/main5739.js?version=4.5.0"></script>
       <!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/61df1e50f7cf527e84d1d2d6/1fp7osrtf';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->
   </body>
  
</html>
 <div class="top-bar color-scheme-bright">
                  <div class="fancy-selector-w">
                     <div class="fancy-selector-current">

                  
                      
                     
                        
                @php ($sum = 0)

                 @foreach(Auth::user()->listings as $listing)
                 @if($listing->type())
                            @if($listing->matched())
                               @php ($now = \Carbon\Carbon::now())
                                 @php($days = \Carbon\Carbon::parse($listing->updated_at)->diffInDays($now))
                                       @php($percentage = $listing->category->parent->percent)

                                        @php($multiplier = ($listing->amount))
 
                             
                            @php ($sum += ($multiplier)+(($multiplier)*($percentage*$days)))

                           @if ($loop->last)

                           @endif

                           @else

                         @endif
                         @endif
                     @endforeach

                     @php ($diff = 0)

                       @foreach(Auth::user()->listings as $listing)
                       @if($listing->type())
                           @if($listing->matched())

                            @foreach($listing->comments as $comment)


                            @php ($diff += $comment->split)



                            @endforeach
                            @else

                         @endif
                         @endif
                      @endforeach
                    

                        @php ($balance = $sum-$diff)


                        <div class="fs-main-info">
                           <div class="fs-name"><span>{{Auth::user()->level->name}} </span><strong></strong></div>

                           <div class="fs-sub"><span>Acc Balance:</span><strong>{{ Auth::user()->area->unit }} <?php echo number_format((float)$balance, 2, '.', ''); ?></strong></div>
                        </div>
                        <div class="fs-selector-trigger"><i class="os-icon os-icon-arrow-down4"></i></div>
                     </div>
                    
                  </div>
                  <div class="top-menu-controls">
                    
                    
                    
                     <div class="logged-user-w">
                        <div class="logged-user-i">
                           <div class="avatar-w"><img alt="" src="/uploads/avatars/{{ Auth::user()->avatar }}"></div>
                           <div class="logged-user-menu color-style-bright">
                              <div class="logged-user-avatar-info">
                                 <div class="avatar-w"><img alt="" src="/uploads/avatars/{{ Auth::user()->avatar }}"></div>
                                 <div class="logged-user-info-w">
                                    <div class="logged-user-name">{{Auth::user()->name}} {{Auth::user()->surname}}</div>
                                    <div class="logged-user-role">{{Auth::user()->level->name}}</div>
                                 </div>
                              </div>
                              <div class="bg-icon"><i class="os-icon os-icon-wallet-loaded"></i></div>
                              <ul>

                                
                                
                                 <li><a href="{{url('profile')}}"><i class="os-icon os-icon-user-male-circle2"></i><span>Profile Details</span></a></li>
                                 @role('admin')
                                  <li><a href="{{url('admin/impersonate')}}"><i class="os-icon os-icon-user-male-circle2"></i><span>Impersonate</span></a></li>
                                    <li><a href="{{url('admin/users')}}"><i class="os-icon os-icon-user-male-circle2"></i><span>Users</span></a></li>
                                      <li><a href="{{url('admin/listings')}}"><i class="os-icon os-icon-user-male-circle2"></i><span>Listings</span></a></li>
                                  @endrole
                                  @if (session()->has('impersonate'))
                                       <li class="selected ">
                                        <a onclick="event.preventDefault(); document.getElementById('impersonating').submit();" class="nk-menu-link">
                                         <div class="icon-w">
                           <div class="os-icon os-icon-ui-46"></div>
                        </div>
                        <span>Stop Impersonating</span>
                                        </a>
                                               <form action="{{ route('admin.impersonate') }}" class="hidden" method="POST" id="impersonating">
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                        </form>
                                    </li>
                                    @endif
                                 <li><a href="{{url('profile')}}"><i class="os-icon os-icon-coins-4"></i><span>Banking Details</span></a></li>
                              
                                 <li><a href="{{ route('logout') }}" class="nav-link"
                                           onclick="event.preventDefault();
                                                         document.getElementById('logout-form').submit();"><i class="os-icon os-icon-signs-11"></i><span>Logout</span></a>
                                             <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            @csrf
                                        </form>
                                                      </li>
                              </ul>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
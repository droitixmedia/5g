  <footer class="footer footer--bg position-relative overflow-hidden">
         <img class="footer-shape footer-shape--left wow fadeInLeft" data-wow-delay="1.5s" src="assets/images/shape/footer-shape-1.png" alt="Shape"> <img class="footer-shape footer-shape--right wow fadeInRight" data-wow-delay="1.5s" src="assets/images/shape/footer-shape-2.png" alt="Shape">
         <div class="footer--top footer--padding">
            <div class="container">
               <div class="row">
                  <div class="col-lg-3 col-md-6 mb-30">
                     <div class="footer-widget">
                        <div class="footer-widget__log mb-30"><a href="{{url('/')}}"><img src="/assets/images/logo-light.png" alt=""></a></div>
                        <div class="footer-widget__text">
                           <p class="footer-widget__text_paragraph">The fastest way to connect yourself nomatter where you are and at a very low cost.</p>
                        </div>
                        <div class="footer-widget__social">
                           <ul>
                              <li class="footer-widget__social_list"><a class="footer-widget__social_list--link facebook" href="javascript:void(0);"><i class="fab fa-facebook-f"></i></a></li>
                              <li class="footer-widget__social_list"><a class="footer-widget__social_list--link twitter" href="javascript:void(0);"><i class="fab fa-twitter"></i></a></li>
                              <li class="footer-widget__social_list"><a class="footer-widget__social_list--link instagram" href="javascript:void(0);"><i class="fab fa-instagram"></i></a></li>
                              <li class="footer-widget__social_list"><a class="footer-widget__social_list--link linkedin" href="javascript:void(0);"><i class="fab fa-linkedin-in"></i></a></li>
                              <li class="footer-widget__social_list"><a class="footer-widget__social_list--link pinterest" href="javascript:void(0);"><i class="fab fa-pinterest-p"></i></a></li>
                           </ul>
                        </div>
                     </div>
                  </div>
                  <div class="col-lg-2 col-md-6 mb-25">
                     <div class="footer-widget">
                        <div class="footer-widget__title">
                           <h3 class="footer-widget__title_heading">General</h3>
                        </div>
                        <div class="footer-widget__menu">
                           <ul>
                              <li class="footer-widget__menu_list"><a href="{{url('/')}}">Home</a></li>
                              <li class="footer-widget__menu_list"><a href="{{url('dashboard')}}">Dashboard</a></li>
                              
                           </ul>
                        </div>
                     </div>
                  </div>
                  <div class="col-lg-2 col-md-6 offset-lg-1 mb-25">
                     <div class="footer-widget">
                        <div class="footer-widget__title">
                           <h3 class="footer-widget__title_heading">Important Links</h3>
                        </div>
                        <div class="footer-widget__menu">
                           <ul>
                              <li class="footer-widget__menu_list"><a href="{{url('about')}}">About Us</a></li>
                              <li class="footer-widget__menu_list"><a href="{{url('#')}}">Meet Our Team</a></li>
                              
                           </ul>
                        </div>
                     </div>
                  </div>
                  <div class="col-lg-2 col-md-6 offset-lg-1 mb-25">
                     <div class="footer-widget">
                        <div class="footer-widget__title">
                           <h3 class="footer-widget__title_heading">Support</h3>
                        </div>
                        <div class="footer-widget__menu">
                           <ul>
                              <li class="footer-widget__menu_list"><a href="{{url('contact')}}">Contact Us</a></li>
                              <li class="footer-widget__menu_list"><a href="{{url('#')}}">Agents Page</a></li>
                              
                              
                           </ul>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="footer--bottom footer--bottom-padding footer--bottom-border">
            <div class="container">
               <div class="row">
                  <div class="col-sm-12">
                     <div class="footer__copyright">
                        <p class="footer__copyright_text">© 2021 <a href="{{url('/')}}">Evolution 5G</a>. All Rights Reserved<a href="https://www.radiustheme.com/" rel="nofollow"></a></p>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </footer>
  <div class="menu-w color-scheme-light color-style-transparent menu-position-side menu-side-left menu-layout-compact sub-menu-style-over sub-menu-color-bright selected-menu-color-light menu-activated-on-hover menu-has-selected-link">
               <div class="logo-w">
                  <a class="logo" href="{{url('/dashboard')}}">
                     <img src="/img/logo-big.png">
                     <div class="logo-label">Evolution 5G</div>
                  </a>
               </div>
               <div class="logged-user-w avatar-inline">
                  <div class="logged-user-i">
                     <div class="avatar-w"><img alt="" src="/uploads/avatars/{{ Auth::user()->avatar }}"></div>
                     <div class="logged-user-info-w">
                        <div class="logged-user-name">{{Auth::user()->name}} {{Auth::user()->surname}}</div>
                        <div class="logged-user-role">{{Auth::user()->accounttype}}</div>
                     </div>
                     <div class="logged-user-toggler-arrow">
                        <div class="os-icon os-icon-chevron-down"></div>
                     </div>
                     <div class="logged-user-menu color-style-bright">
                        <div class="logged-user-avatar-info">
                           <div class="avatar-w"><img alt="" src="/uploads/avatars/{{ Auth::user()->avatar }}"></div>
                           <div class="logged-user-info-w">
                              <div class="logged-user-name">{{Auth::user()->name}} {{Auth::user()->surname}}</div>
                              <div class="logged-user-role">{{Auth::user()->accounttype}}</div>
                           </div>
                        </div>
                        <div class="bg-icon"><i class="os-icon os-icon-wallet-loaded"></i></div>
                        <ul>
                            <li><a href="{{url('profile')}}"><i class="os-icon os-icon-user-male-circle2"></i><span>Profile Details</span></a></li>
                                 <li><a href="users_profile_small.html"><i class="os-icon os-icon-coins-4"></i><span>Billing Details</span></a></li>
                                 <li><a href="#"><i class="os-icon os-icon-others-43"></i><span>Notifications</span></a></li>
                                 <li><a href="{{ route('logout') }}" class="nav-link"
                                           onclick="event.preventDefault();
                                                         document.getElementById('logout-form').submit();"><i class="os-icon os-icon-signs-11"></i><span>Logout</span></a>
                                             <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            @csrf
                                        </form>
                                                      </li>
                        </ul>
                     </div>
                  </div>
               </div>
               
              
               <h1 class="menu-page-header">Page Header</h1>
               <ul class="main-menu">
                  <li class="sub-header"><span>General</span></li>
                  <li class="selected ">
                     <a href="{{url('/dashboard')}}">
                        <div class="icon-w">
                           <div class="os-icon os-icon-layout"></div>
                        </div>
                        <span>Dashboard</span>
                     </a>
                     
                  </li>
                   <li class="selected ">
                     <a href="{{ route('listings.published.index') }}">
                        <div class="icon-w">
                           <div class="os-icon os-icon-refresh-ccw"></div>
                        </div>
                        <span>Transactions</span>
                     </a>
                     
                  </li>
                   <li class="selected ">
                     <a href="{{url('/pay')}}">
                        <div class="icon-w">
                           <div class="os-icon os-icon-refresh-ccw"></div>
                        </div>
                        <span>Deposit</span>
                     </a>
                     
                  </li>

                   <li class="selected ">
                     <a href="{{ route('listings.published.index') }}">
                        <div class="icon-w">
                           <div class="os-icon os-icon-log-out"></div>
                        </div>
                        <span>Withdraw</span>
                     </a>
                     
                  </li>
                  
                  <li class="sub-header"><span>Affiliate</span></li>
                    <li class="selected ">
                     <a href="{{url('/referrals')}}">
                        <div class="icon-w">
                           <div class="os-icon os-icon-users"></div>
                        </div>
                        <span>Affiliated Users</span>
                     </a>
                     
                  </li>

                  <li class="selected ">
                     <a href="{{url('/bonus')}}">
                        <div class="icon-w">
                           <div class="os-icon os-icon-coins-4"></div>
                        </div>
                        <span>Bonus Earned</span>
                     </a>
                     
                  </li>

                  <li class="selected ">
                     <a href="{{ route('listings.bcreate') }}">
                        <div class="icon-w">
                           <div class="os-icon os-icon-email-forward"></div>
                        </div>
                        <span>Withdraw Bonus</span>
                     </a>
                     
                  </li>
                   @role('admin')
                  <li class="sub-header"><span>Adminstration</span></li>
                  @endrole
                     @if (session()->has('impersonate'))
                                       <li class="selected ">
                                        <a onclick="event.preventDefault(); document.getElementById('impersonating').submit();" class="nk-menu-link">
                                         <div class="icon-w">
                           <div class="os-icon os-icon-ui-46"></div>
                        </div>
                        <span>Stop Impersonating</span>
                                        </a>
                                               <form action="{{ route('admin.impersonate') }}" class="hidden" method="POST" id="impersonating">
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                        </form>
                                    </li>
                                    @else
                                    @role('admin')
                    <li class="selected ">
                     <a href="{{url('/admin/impersonate')}}">
                        <div class="icon-w">
                           <div class="os-icon os-icon-ui-46"></div>
                        </div>
                        <span>Impersonate</span>
                     </a>
                     
                  </li>
                  @endrole
                  @endif
                      @role('admin')
                  <li class="selected ">
                     <a href="{{url('/user-management')}}">
                        <div class="icon-w">
                           <div class="os-icon os-icon-ui-46"></div>
                        </div>
                        <span>User Management</span>
                     </a>
                     
                  </li>
                    <li class="selected ">
                     <a href="{{url('/order-management')}}">
                        <div class="icon-w">
                           <div class="os-icon os-icon-ui-46"></div>
                        </div>
                        <span>Order Management</span>
                     </a>
                     
                  </li>
                    
                 @endrole 
               </ul>
               <div class="side-menu-magic">
                  <h4>Evolution 5G</h4>
                  <p>Home of fast and portable Network</p>
                  <div class="btn-w"><a class="btn btn-white btn-rounded" href="https://themeforest.net/item/light-admin-clean-bootstrap-dashboard-html-template/19760124?ref=Osetin" target="_blank">Shop Now</a></div>
               </div>
            </div>
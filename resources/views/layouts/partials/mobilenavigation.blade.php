
 <div class="menu-mobile menu-activated-on-click color-scheme-dark">
               <div class="mm-logo-buttons-w">
                  <a class="mm-logo" href="{{url('/dashboard')}}"><img src="/img/logo-white.png"><span>Evolution 5G</span></a>
                  
               </div>
               <div class="menu-and-user">
                  <div class="logged-user-w">
                     <div class="avatar-w"><img alt="" src="/uploads/avatars/{{ Auth::user()->avatar }}"></div>
                     <div class="logged-user-info-w">
                        <div class="logged-user-name">{{Auth::user()->name}} {{Auth::user()->surname}}</div>
                        <div class="logged-user-role">{{Auth::user()->level->name}}</div>
                     </div>
                  </div>
                  <ul class="main-menu">
                     <li class="">
                        <a href="{{url('/dashboard')}}">
                           <div class="icon-w">
                              <div class="os-icon os-icon-layout"></div>
                           </div>
                           <span>Dashboard</span>
                        </a>
                        
                     </li>
                      <li class="">
                        <a href="{{url('/dashboard')}}">
                           <div class="icon-w">
                              <div class="os-icon os-icon-refresh-ccw"></div>
                           </div>
                           <span>Deposit</span>
                        </a>
                        
                     </li>
                      <li class="">
                        <a href="{{url('/dashboard')}}">
                           <div class="icon-w">
                              <div class="os-icon os-icon-log-out"></div>
                           </div>
                           <span>Withdraw Cash</span>
                        </a>
                        
                     </li>
                       <li class="">
                        <a href="{{url('/dashboard')}}">
                           <div class="icon-w">
                              <div class="os-icon os-icon-users"></div>
                           </div>
                           <span>Affiliated Users</span>
                        </a>
                        
                     </li>
                       <li class="">
                        <a href="{{url('/dashboard')}}">
                           <div class="icon-w">
                              <div class="os-icon os-icon-coins-4"></div>
                           </div>
                           <span>Bonus Generated</span>
                        </a>
                        
                     </li>

                       <li class="">
                        <a href="{{url('/dashboard')}}">
                           <div class="icon-w">
                              <div class="os-icon os-icon-email-forward"></div>
                           </div>
                           <span>Withdraw Bonus</span>
                        </a>
                        
                     </li>
                    
                  </ul>
                  <div class="mobile-menu-magic">
                     <h4>Evolution 5G</h4>
                     <p>Fastest 5G network distributor</p>
                     <div class="btn-w"><a class="btn btn-white btn-rounded" href="{{url('/')}}" target="_blank">Shop</a></div>
                  </div>
               </div>
            </div>
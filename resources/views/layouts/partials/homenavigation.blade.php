 <header class="header d-none d-lg-block sticky-on">
         <div id="sticky-placeholder"></div>
         <div id="topbar-wrap" class="header-top header-top__border_bottom header-top__padding container-custom">
            <div class="container-fluid">
               <div class="row align-items-center">
                  <div class="col-lg-9">
                     <div class="header-top__info">
                        <div class="header-top__info_item header-top__info_time">
                           <div class="header-top__info_icon"><i class="flaticon flaticon-clock"></i></div>
                           <div class="header-top__info_text">
                              <p class="header-top__info_text--small">Mon - Friday:</p>
                              <span class="header-top__info_text--big">8am - 9pm</span>
                           </div>
                        </div>
                        <div class="header-top__info_item header-top__info_phone">
                           <div class="header-top__info_icon"><i class="flaticon flaticon-phone-call"></i></div>
                           <div class="header-top__info_text">
                              <p class="header-top__info_text--small">Whatsapp Support</p>
                              <span class="header-top__info_text--big"></span>
                           </div>
                        </div>
                        <div class="header-top__info_item header-top__info_email">
                           <div class="header-top__info_icon"><i class="flaticon flaticon-envelope"></i></div>
                           <div class="header-top__info_text">
                              <p class="header-top__info_text--small">Mail to us</p>
                              <span class="header-top__info_text--big">support@evolution5g.org</span>
                           </div>
                        </div>
                        <div class="header-top__info_item header-top__info_address">
                           <div class="header-top__info_icon"><i class="flaticon flaticon-location"></i></div>
                           <div class="header-top__info_text">
                              <p class="header-top__info_text--small">Our Address:</p>
                              <span class="header-top__info_text--big">10 Grampian Rd,

Hong Kong</span>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-lg-3 d-none d-lg-block">
                     <div class="header-top__social">
                        <ul>
                           <li class="header-top__social_list"><a class="header-top__social_list--link facebook" href="javascript:void(0);"><i class="fab fa-facebook-f"></i></a></li>
                           <li class="header-top__social_list"><a class="header-top__social_list--link twitter" href="javascript:void(0);"><i class="fab fa-twitter"></i></a></li>
                           <li class="header-top__social_list"><a class="header-top__social_list--link instagram" href="javascript:void(0);"><i class="fab fa-instagram"></i></a></li>
                           <li class="header-top__social_list"><a class="header-top__social_list--link linkedin" href="javascript:void(0);"><i class="fab fa-linkedin-in"></i></a></li>
                           <li class="header-top__social_list"><a class="header-top__social_list--link pinterest" href="javascript:void(0);"><i class="fab fa-pinterest-p"></i></a></li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div id="navbar-wrap" class="header-bottom container-custom navbar-wrap white-bg">
            <div class="container-fluid">
               <div class="header-bottom__row">
                  <div class="header-bottom__col">
                     <div class="logo"><a href="{{url('/')}}" class="logo__link"><img src="assets/images/logodarker.png" height="150" width="200" alt=""></a></div>
                  </div>
                  <div class="header-bottom__col">
                     <div class="main-menu">
                        <nav class="main-menu__nav">
                           <ul>
                              <li class="list"><a class="animation" href="{{url('/')}}">Home</a></li>
                              <li class="list"><a class="animation" href="{{url('/about')}}">About Us</a></li>
                              <li class="list"><a class="animation" href="https://shop.evolution5g.org/">Shop</a></li>
                              <li class="list"><a class="animation" href="{{url('/boost-package')}}">Boost Packages</a></li>
                              <li class="list"><a class="animation" href="{{url('/affiliate-levels')}}">Affiliate Levels</a></li>
                               <li class="list"><a class="animation" href="{{url('/testify')}}">Testify</a></li>
                              <li class="list"><a class="animation" href="{{url('/contact')}}">Contact Us</a></li>
                           </ul>
                        </nav>
                     </div>
                  </div>
                  @guest
                  <a href="{{url('/login')}}" class="btn btn--common btn--warning rt-button-animation-out" data-animation="fadeInUp" data-delay="1s">
                                Login
                                 <svg width="34px" height="16px" viewBox="0 0 34.53 16" xml:space="preserve">
                                    <rect class="rt-button-line" y="7.6" width="34" height=".4"></rect>
                                    <g class="rt-button-cap-fake">
                                       <path class="rt-button-cap" d="M25.83.7l.7-.7,8,8-.7.71Zm0,14.6,8-8,.71.71-8,8Z"></path>
                                    </g>
                                 </svg>
                              </a>

                           @else

                           <a href="{{url('/dashboard')}}" class="btn btn--common btn--warning rt-button-animation-out" data-animation="fadeInUp" data-delay="1s">
                                Dashboard
                                 <svg width="34px" height="16px" viewBox="0 0 34.53 16" xml:space="preserve">
                                    <rect class="rt-button-line" y="7.6" width="34" height=".4"></rect>
                                    <g class="rt-button-cap-fake">
                                       <path class="rt-button-cap" d="M25.83.7l.7-.7,8,8-.7.71Zm0,14.6,8-8,.71.71-8,8Z"></path>
                                    </g>
                                 </svg>
                              </a>
                           @endguest
               </div>
            </div>
         </div>
      </header>
         <div class="rt-header-menu mean-container position-relative" id="meanmenu">
         <div class="mean-bar">
            <a href="{{url('/')}}"><img class="logo-small" alt="Techkit" src="assets/images/logodarker.png"></a>
            <form class="header-bottom__col">
               <div class="header-search"><input placeholder="Search" type="text" class="header-search__input"> <button class="header-search__button"><i class="fas fa-search"></i></button></div>
            </form>
            <span class="sidebarBtn"><span class="fa fa-bars"></span></span>
         </div>
         <div class="rt-slide-nav">
            <div class="offscreen-navigation">
               <nav class="menu-main-primary-container">
                  <ul class="menu">
                     <li class="list menu-item-parent"><a class="animation" href="{{url('/')}}">Home</a></li>

                              
                              <li class="list"><a class="animation" href="{{url('/about')}}">About Us</a></li>
                              <li class="list"><a class="animation" href="https://shop.evolution5g.org/">Shop</a></li>
                              <li class="list"><a class="animation" href="{{url('/boost-package')}}">Boost Packages</a></li>
                              <li class="list"><a class="animation" href="{{url('/affiliate-levels')}}">Affiliate Levels</a></li>
                               <li class="list"><a class="animation" href="{{url('/testify')}}">Testify</a></li>
                             
                     <li class="list menu-item-parent"><a class="animation" href="{{url('contact')}}">Contact Us</a></li>
                     @guest
                         <a href="{{url('/login')}}" class="btn btn--common btn--warning rt-button-animation-out" data-animation="fadeInUp" data-delay="1s">
                                 Login
                                 <svg width="34px" height="16px" viewBox="0 0 34.53 16" xml:space="preserve">
                                    <rect class="rt-button-line" y="7.6" width="34" height=".4"></rect>
                                    <g class="rt-button-cap-fake">
                                       <path class="rt-button-cap" d="M25.83.7l.7-.7,8,8-.7.71Zm0,14.6,8-8,.71.71-8,8Z"></path>
                                    </g>
                                 </svg>
                              </a>
                              @else
                               <a href="{{url('/dashboard')}}" class="btn btn--common btn--warning rt-button-animation-out" data-animation="fadeInUp" data-delay="1s">
                                 Dashboard
                                 <svg width="34px" height="16px" viewBox="0 0 34.53 16" xml:space="preserve">
                                    <rect class="rt-button-line" y="7.6" width="34" height=".4"></rect>
                                    <g class="rt-button-cap-fake">
                                       <path class="rt-button-cap" d="M25.83.7l.7-.7,8,8-.7.71Zm0,14.6,8-8,.71.71-8,8Z"></path>
                                    </g>
                                 </svg>
                              </a>

                              @endguest
                  </ul>
               </nav>
            </div>
         </div>

      </div>
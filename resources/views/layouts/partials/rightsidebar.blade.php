<div class="content-panel">
                     <div class="content-panel-close"><i class="os-icon os-icon-close"></i></div>
                     <div class="element-wrapper">
                        <h6 class="element-header">Your Manager</h6>
                        <div class="element-box-tp">
                          <div class="profile-tile">
                              <a class="profile-tile-box" href="{{route('profile.index', ['email'=>Auth::user()->referrer->email])}}">
                                 <div class="pt-avatar-w"><img alt="" src="/uploads/avatars/{{ Auth::user()->referrer->avatar }}"></div>
                                 <div class="pt-user-name">@isset( Auth::user()->referrer)

{{Auth::user()->referrer->name}} {{Auth::user()->referrer->surname}}</div>
                              </a>
                              <div class="profile-tile-meta">
                                 <ul>
                                    <li>Contact:<strong>{{Auth::user()->referrer->phone_number}}</strong></li>
                                    <li>Country:{{Auth::user()->referrer->area->parent->name}} <span class="flag-icon flag-icon-{{Auth::user()->area->icon}}"></span><strong><a href="#"></a></strong></li>
                                    <li>Affiliates:<strong>{{Auth::user()->referrer->referrals()->count()}}</strong></li>
                                 </ul>
                                 <div class="pt-btn"><a class="btn btn-success btn-sm" href="#">{{Auth::user()->referrer->level->name}}</a></div>
                                 @endisset
                              </div>
                           </div>
                        </div>
                     </div>
                     <h6 class="element-header">
                 
                    
                  </div>
 
@extends('layouts.homeapp')

@section('title', 'Dashboard')

@section('description')

@endsection

@section('content')


<section class="services services--details pt-130 pb-100">
                  <div class="container">
                     <div class="row">
                        <div class="row">
                        <div class="col-lg-6">
                  <form class="contact-form--main"  method="post" action="{{ route('testimony.store') }}">
                    <div class="contact-form p-0">
                      <div class="row">
                       <h4>Hie {{Auth::user()->name}} Say something about Evolution 5G</h4>
                        <div class="col-md-12">
                          <textarea name="body" class="contact-form__input contact-form__input_textarea" placeholder="Testimony"></textarea>
                        </div>
                        <div class="col-md-12">
                          <button type="submit" class="contact-form__button">
                            Testify
                          </button>
                        </div>
                      </div>
                    </div>
                    <div class="form-response mt-10"></div>
                    {{ csrf_field() }} 
                  </form>
                       
                     </div> 
                     </div>
                  </div>
               </section>               

@endsection
@extends('layouts.app')
@section('title', 'Profile')
@section('description')
@endsection
@section('content')
<div class="content-w">
   <ul class="breadcrumb">
      <li class="breadcrumb-item"><a href="{{url('dashboard')}}">Dashboard</a></li>
      <li class="breadcrumb-item"><a href="#">{{Auth::user()->name}} {{Auth::user()->surname}}</a></li>
   </ul>
   <div class="content-i">
      <div class="content-box">
         <div class="row">
            <div class="col-lg-7">
               <div class="element-wrapper">
                  <div class="element-box">
                     <div class="element-info">
                        <div class="element-info-with-icon">
                           <div class="element-info-icon">
                              <div class="os-icon os-icon-wallet-loaded"></div>
                           </div>
                           <div class="element-info-text">
                              <h5 class="element-inner-header">Profile Settings</h5>
                              <div class="element-inner-desc">Edit your profile. </div>
                           </div>
                        </div>
                     </div>
                     <legend><span>PROFILE PHOTO</span></legend>
                     <div class="row">
                        <div class="profile-tile">
                           <a class="profile-tile-box" href="users_profile_small.html">
                              <div class="pt-avatar-w"><img alt="" src="/uploads/avatars/{{ $user->avatar }}"></div>
                           </a>
                        </div>
                     </div>
                     <br>
                     <div class="row">
                        <form enctype="multipart/form-data" action="{{route('profile.update.avatar')}}" method="POST">
                           <input  type="file" style="margin-bottom: 20px;" name="avatar">
                           <input type="hidden" name="_token" value="{{ csrf_token() }}">
                           <input type="submit" class=" btn btn-sm btn-success">
                        </form>
                     </div>
                     <br>



                     <form method="POST"  action="{{route('profile.update')}}">
                        @foreach ($errors->all() as $error)
                        <p class="text-danger">{{ $error }}</p>
                        @endforeach
                        @csrf
                        <legend><span>PERSONAL INFO</span></legend>
                        @if (session()->has('impersonate'))
                        <div class="form-group">
                           <label for="level" style="color:red;"> Admin Change User Level</label>
                           <select class="form-control" name="level_id">
                              <option value="{{$user->level_id}}">{{$user->level->name}}</option>
                              <option value="1">MEMBER</option>
                              <option value="2">AGENT</option>
                              <option value="3">SUPERVISOR</option>
                              <option value="4">COORDINATOR</option>
                              <option value="5">MANAGER</option>
                              <option value="6">AMBASSADOR</option>
                           </select>
                        </div>
                        @else
                          <input type="hidden" name="level_id" value="{{$user->level_id}}">
                        @endif
                        <div class="row">
                           <div class="col-sm-6">

                              <div class="form-group">
                                 <label for=""> First Name</label><input class="form-control" data-error="Please input your First Name" placeholder="First Name" required="required" name="name" value="{{$user->name}}">
                                 <div class="help-block form-text with-errors form-control-feedback"></div>
                              </div>
                           </div>

                           <div class="col-sm-6">
                              <div class="form-group">
                                 <label for="">Last Name</label><input class="form-control" data-error="Please input your Last Name" placeholder="Last Name" required="required" name="surname" value="{{$user->surname}}">
                                 <div class="help-block form-text with-errors form-control-feedback"></div>
                              </div>
                           </div>
                        </div>

                        <div class="form-group">
                           <label for=""> Email address</label><input class="form-control" data-error="Your email address is invalid" placeholder="Enter email" required="required" type="email" name="email" value="{{$user->email}}">
                           <div class="help-block form-text with-errors form-control-feedback"></div>
                        </div>

                        <div class="form-group">
                           <label for=""> Phone Number</label><input class="form-control" data-error="Your email address is invalid" placeholder="Phone Number" required="required" type="text" name="phone_number" value="{{$user->phone_number}}">
                           <div class="help-block form-text with-errors form-control-feedback"></div>
                        </div>

                        <fieldset class="form-group">
                        <legend><span>BANKING DETAILS</span></legend>
                        <div class="row">

                           <div class="col-sm-6">
                              <div class="form-group">
                                 <label for="">Bitcoin Address</label>
                                 <div class="input-group">
                                    <div class="input-group-prepend">
                                       <div class="input-group-text">Bitcoin</div>
                                    </div>
                                    <input value="{{$user->bitcoin}}" name="bitcoin"  class="form-control" placeholder="Bitcoin">
                                 </div>
                              </div>
                           </div>

                           <div class="col-sm-6">
                              <div class="form-group">
                                 <label for="">Paypal</label>
                                 <div class="input-group">
                                    <div class="input-group-prepend">
                                       <div class="input-group-text">Paypal</div>
                                    </div>
                                    <input name="paypal" value="{{$user->paypal}}"  class="form-control" placeholder="Paypal">
                                 </div>
                              </div>
                           </div>
                        </div>

                        <div class="row">
                           <div class="col-sm-6">
                              <div class="form-group">
                                 <label for="">Mobile Money</label>
                                 <div class="input-group">
                                    <div class="input-group-prepend">
                                       <div class="input-group-text">Mobile Money</div>
                                    </div>
                                    <input value="{{$user->mobile_wallet}}" name="mobile_wallet"  class="form-control" placeholder="Mobile Money Name">
                                 </div>
                              </div>
                           </div>

                           <div class="col-sm-6">
                              <div class="form-group">
                                 <label for="">Mobile Money Number</label>
                                 <div class="input-group">
                                    <div class="input-group-prepend">
                                       <div class="input-group-text">Number</div>
                                    </div>
                                    <input name="mobile_wallet_number" value="{{$user->mobile_wallet_number}}"  class="form-control" placeholder="Mobile Money Number">
                                 </div>
                              </div>
                           </div>
                        </div>

                        <div class="row">
                           <div class="col-sm-6">
                              <div class="form-group">
                                 <label for="">Bank Name</label>
                                 <div class="input-group">
                                    <div class="input-group-prepend">
                                       <div class="input-group-text">Bank</div>
                                    </div>
                                    <input name="bank" value="{{$user->bank}}"  class="form-control" placeholder="Bank Name">
                                 </div>
                              </div>
                           </div>
                           <div class="col-sm-6">
                              <div class="form-group">
                                 <label for="">Skrill</label>
                                 <div class="input-group">
                                    <div class="input-group-prepend">
                                       <div class="input-group-text">Skrill</div>
                                    </div>
                                    <input name="skrill" value="{{$user->skrill}}"  class="form-control" placeholder="Skrill">
                                 </div>
                              </div>
                           </div>
                           
                           <div class="col-sm-6">
                              <div class="form-group">
                                 <label for="">Account Number</label>
                                 <div class="input-group">
                                    <div class="input-group-prepend">
                                       <div class="input-group-text">Acc Number</div>
                                    </div>
                                    <input name="account" value="{{$user->account}}"  class="form-control" placeholder="Account Number">
                                 </div>
                              </div>
                           </div>
                        </div>
                        <input type="hidden" name="accounttype" value="na">
                        
                      
                        <input type="hidden" name="branch" value="na">
                        <input type="hidden" name="phone" value="na">
                 
                        <button class="btn btn-primary" type="submit"> Submit</button>
                     </form>
                  </div>
               </div>
            </div>
         </div>
      </div>
      @include('layouts.partials.rightsidebar')
   </div>
</div>
@endsection
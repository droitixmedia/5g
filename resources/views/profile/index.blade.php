 
@extends('layouts.app')

@section('title', 'Dashboard')

@section('description')

@endsection

@section('content')

  <div class="content-w">
               <div class="top-bar color-scheme-transparent">
                 
               </div>
               <ul class="breadcrumb">
                  <li class="breadcrumb-item"><a href="{{url('dashboard')}}">Dashboard</a></li>
                  <li class="breadcrumb-item"><a href="#">{{$user->name}} {{$user->surname}}</a></li>
                 
               </ul>
               <div class="content-panel-toggler"><i class="os-icon os-icon-grid-squares-22"></i><span>Sidebar</span></div>
               <div class="content-i">
                  <div class="content-box">
                     <div class="element-wrapper">
                        <div class="user-profile">
                           <div class="up-head-w" style="background-image:url(/img/banners/{{$user->level->banner}}.jpg)">
                              <div class="up-social"><a href="#"><i class="os-icon os-icon-twitter"></i></a><a href="#"><i class="os-icon os-icon-facebook"></i></a></div>
                              <div class="up-main-info">
                                 <div class="user-avatar-w">
                                    <div class="user-avatar"><img alt="" src="/uploads/avatars/{{ $user->avatar }}"></div>
                                 </div>
                                 <h1 class="up-header">{{$user->name}} {{$user->surname}}</h1>
                                 <h5 class="up-sub-header">{{$user->level->name}}</h5>
                                 <h6 class="up-sub-header">{{$user->area->parent->name}} <span class="flag-icon flag-icon-{{$user->area->parent->icon}}"></span></h6>
                              </div>
                              <svg class="decor" width="842px" height="219px" viewBox="0 0 842 219" preserveAspectRatio="xMaxYMax meet" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                 <g transform="translate(-381.000000, -362.000000)" fill="#08A045">
                                    <path class="decor-path" d="M1223,362 L1223,581 L381,581 C868.912802,575.666667 1149.57947,502.666667 1223,362 Z"></path>
                                 </g>
                              </svg>
                           
                           </div>
                           <div class="up-controls">
                              <div class="row">
                                 <div class="col-lg-6">
                                    <div class="value-pair">
                                       <div class="label">Status:</div>
                                       <div class="value badge badge-pill badge-success">Active Account </div>
                                    </div>
                                    <div class="value-pair">
                                       <div class="label">Joined:</div>
                                       <div class="value">{{ $user->created_at->diffForHumans() }}</div>
                                    </div>
                                 </div>
                                
                              </div>
                           </div>
                           <div class="up-contents">
                              <h5 class="element-header"> @foreach($user->listings as $listing)
                                         @if($listing->type())
                                         @if($listing->matched())
                                         <td class="cell-with-media"> <img alt="" src="/assets/images/badges/{{$listing->category->parent->icon}}.png" style="height: 25px;"><span>{{ $listing->category->name }} </span></td>

                                         @endif

                                         @endif
                                          
                                       
                                 
                                         @endforeach</h5>
                              <div class="row m-b">
                                 <div class="col-lg-6">
                                    <div class="row">
                                       <div class="col-sm-6 b-r b-b">
                                          <div class="el-tablo centered padded">
                                             <div class="value">{{ $user->referrals()->count()}}</div>
                                             <div class="label">Downliners</div>
                                          </div>
                                       </div>
                                        @php ($withdrawn = 0)
      @foreach($user->listings as $listing)
      @if($listing->recommit())
      @php ($withdrawn += $listing->amount)
      @if ($loop->last)
      @endif
      @else
      @endif
      @endforeach
                                       <div class="col-sm-6 b-b b-r">
                                          <div class="el-tablo centered padded">
                                             <div class="value">{{ $user->area->unit }} {{\openjobs\Bonus::where('referer_id', $user->id)->sum('bonus_amount')+\openjobs\Bonus::where('second_gen_id', $user->id)->sum('second_gen_amount')+\openjobs\Bonus::where('third_gen_id', $user->id)->sum('third_gen_amount')-($withdrawn)}}.00</div>
                                             <div class="label">Bonus Earned</div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="row">
                                       <div class="col-sm-6 b-r">
                                          <div class="el-tablo centered padded">
                                             <div class="value">${{$user->level->pay}}</div>
                                             <div class="label">Current Salary</div>
                                          </div>
                                       </div>
                                       <div class="col-sm-6 b-r">
                                          <div class="el-tablo centered padded">
                                             <div class="value">0</div>
                                             <div class="label">Awards</div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 
                              </div>
                             
                           </div>
                        </div>
                     </div>
                     <div class="floated-colors-btn second-floated-btn">
                        <div class="os-toggler-w">
                           <div class="os-toggler-i">
                              <div class="os-toggler-pill"></div>
                           </div>
                        </div>
                        <span>Dark </span><span>Colors</span>
                     </div>
                     <div class="floated-customizer-btn third-floated-btn">
                        <div class="icon-w"><i class="os-icon os-icon-ui-46"></i></div>
                        <span>Customizer</span>
                     </div>
                     <div class="floated-customizer-panel">
                        <div class="fcp-content">
                           <div class="close-customizer-btn"><i class="os-icon os-icon-x"></i></div>
                           <div class="fcp-group">
                              <div class="fcp-group-header">Menu Settings</div>
                              <div class="fcp-group-contents">
                                 <div class="fcp-field">
                                    <label for="">Menu Position</label>
                                    <select class="menu-position-selector">
                                       <option value="left">Left</option>
                                       <option value="right">Right</option>
                                       <option value="top">Top</option>
                                    </select>
                                 </div>
                                 <div class="fcp-field">
                                    <label for="">Menu Style</label>
                                    <select class="menu-layout-selector">
                                       <option value="compact">Compact</option>
                                       <option value="full">Full</option>
                                       <option value="mini">Mini</option>
                                    </select>
                                 </div>
                                 <div class="fcp-field with-image-selector-w">
                                    <label for="">With Image</label>
                                    <select class="with-image-selector">
                                       <option value="yes">Yes</option>
                                       <option value="no">No</option>
                                    </select>
                                 </div>
                                 <div class="fcp-field">
                                    <label for="">Menu Color</label>
                                    <div class="fcp-colors menu-color-selector">
                                       <div class="color-selector menu-color-selector color-bright selected"></div>
                                       <div class="color-selector menu-color-selector color-dark"></div>
                                       <div class="color-selector menu-color-selector color-light"></div>
                                       <div class="color-selector menu-color-selector color-transparent"></div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="fcp-group">
                              <div class="fcp-group-header">Sub Menu</div>
                              <div class="fcp-group-contents">
                                 <div class="fcp-field">
                                    <label for="">Sub Menu Style</label>
                                    <select class="sub-menu-style-selector">
                                       <option value="flyout">Flyout</option>
                                       <option value="inside">Inside/Click</option>
                                       <option value="over">Over</option>
                                    </select>
                                 </div>
                                 <div class="fcp-field">
                                    <label for="">Sub Menu Color</label>
                                    <div class="fcp-colors">
                                       <div class="color-selector sub-menu-color-selector color-bright selected"></div>
                                       <div class="color-selector sub-menu-color-selector color-dark"></div>
                                       <div class="color-selector sub-menu-color-selector color-light"></div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="fcp-group">
                              <div class="fcp-group-header">Other Settings</div>
                              <div class="fcp-group-contents">
                                 <div class="fcp-field">
                                    <label for="">Full Screen?</label>
                                    <select class="full-screen-selector">
                                       <option value="yes">Yes</option>
                                       <option value="no">No</option>
                                    </select>
                                 </div>
                                 <div class="fcp-field">
                                    <label for="">Show Top Bar</label>
                                    <select class="top-bar-visibility-selector">
                                       <option value="yes">Yes</option>
                                       <option value="no">No</option>
                                    </select>
                                 </div>
                                 <div class="fcp-field">
                                    <label for="">Above Menu?</label>
                                    <select class="top-bar-above-menu-selector">
                                       <option value="yes">Yes</option>
                                       <option value="no">No</option>
                                    </select>
                                 </div>
                                 <div class="fcp-field">
                                    <label for="">Top Bar Color</label>
                                    <div class="fcp-colors">
                                       <div class="color-selector top-bar-color-selector color-bright selected"></div>
                                       <div class="color-selector top-bar-color-selector color-dark"></div>
                                       <div class="color-selector top-bar-color-selector color-light"></div>
                                       <div class="color-selector top-bar-color-selector color-transparent"></div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="floated-chat-btn"><i class="os-icon os-icon-mail-07"></i><span>Demo Chat</span></div>
                     <div class="floated-chat-w">
                        <div class="floated-chat-i">
                           <div class="chat-close"><i class="os-icon os-icon-close"></i></div>
                           <div class="chat-head">
                              <div class="user-w with-status status-green">
                                 <div class="user-avatar-w">
                                    <div class="user-avatar"><img alt="" src="img/avatar1.jpg"></div>
                                 </div>
                                 <div class="user-name">
                                    <h6 class="user-title">John Mayers</h6>
                                    <div class="user-role">Account Manager</div>
                                 </div>
                              </div>
                           </div>
                           <div class="chat-messages">
                              <div class="message">
                                 <div class="message-content">Hi, how can I help you?</div>
                              </div>
                              <div class="date-break">Mon 10:20am</div>
                              <div class="message">
                                 <div class="message-content">Hi, my name is Mike, I will be happy to assist you</div>
                              </div>
                              <div class="message self">
                                 <div class="message-content">Hi, I tried ordering this product and it keeps showing me error code.</div>
                              </div>
                           </div>
                           <div class="chat-controls">
                              <input class="message-input" placeholder="Type your message here...">
                              <div class="chat-extra"><a href="#"><span class="extra-tooltip">Attach Document</span><i class="os-icon os-icon-documents-07"></i></a><a href="#"><span class="extra-tooltip">Insert Photo</span><i class="os-icon os-icon-others-29"></i></a><a href="#"><span class="extra-tooltip">Upload Video</span><i class="os-icon os-icon-ui-51"></i></a></div>
                           </div>
                        </div>
                     </div>
                  </div>
            
               </div>
            </div>


@include('layouts.partials.rightsidebar')
 
@endsection
@extends('layouts.app')

@section('title', 'Dashboard')

@section('description')

@endsection

@section('content')

<div class="content-box">
                     
                    
                     <div class="element-wrapper">
                        <h6 class="element-header">All Withdrawals </h6>
                        <div class="element-box-tp">
                           <div class="table-responsive">
                              <table class="table table-padded">
                                 <thead>
                                    <tr>
                                       <th>Name</th>

                                       <th>Amount</th>
                                        <th>Withdrawal Method</th>

                                       <th class="text-center">Status</th>
                                       <th>Country</th>
                                     
                                       <th>Email</th>
                                        <th>Date</th>
                                        <th>Action</th>
                                      
                                       <th>Package</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                 @foreach ($listings as $listing)
                                    @if ($listing->matched())
                                   
                                        @foreach ($listing->comments as $comment)
                                           <tr>
                                             <td class="cell-with-media">{{$comment->id}} <a href="{{route('profile.index', ['email'=>$comment->user->email])}}"><img alt="" src="/uploads/avatars/{{ $comment->user->avatar }}" style="height: 25px;"><span>{{ $comment->user->name }} {{ $comment->user->surname }}</span></a></td>
                                             <td>{{ $listing->user->area->unit}}{{$comment->split}}</td>
                                                <td> <img alt="" src="/img/method/{{$comment->listing->paymentmethod}}.png" style="height: 25px;"> {{$comment->listing->paymentmethod}}</td>
                                                
                                                <td>@if ($comment->approvals->count())
                                                  <button type="submit" class="btn btn-success">Paid</button>
                                                @else
                                                      <button type="submit" class="btn btn-warning">Not Paid</button>
                                                @endif

                                            </td>
                                              <td>{{ $comment->user->area->parent->name}} <span class="flag-icon flag-icon-{{$comment->user->area->icon}}"></span></td> 
                                               <td><span>{{$comment->user->email}}</span><span class="smaller lighter"></span></td>
                                               
                                                <td>{{$comment->created_at}}</td>
                                               
                                            <td>
                                              <a href="#" class="btn btn-sm bg-danger-light"
                                        onclick="event.preventDefault(); document.getElementById('comment-destroy-form-{{ $comment->id }}').submit();"
                            data-toggle="tooltip" data-placement="bottom" title="Reallocate Coins"><i class="fe fe-trash"></i>Reallocate</a></li>

                             <form action="{{route('admin.comment.destroy', [$comment->id])}}" method="post" id="comment-destroy-form-{{ $comment->id }}">
                                        {{ csrf_field() }}
                                        {{ method_field('DELETE') }}
                                    </form>


                                            </td>
                                            <td class="cell-with-media"> <img alt="" src="/assets/images/badges/{{$comment->listing->category->parent->icon}}.png" style="height: 25px;"><span>{{ $comment->listing->category->name }} </span></td>
                                             </tr>
                                           @endforeach
                                        @else



                
                                   @endif

                                        @endforeach

                                 </tbody>
                              </table>
                           </div>
                        </div>
                     </div>
                     
                  </div>
@endsection

@extends('layouts.app')

@section('title', 'Dashboard')

@section('description')

@endsection

@section('content')

<div class="content-box">
                     
                    
                     <div class="element-wrapper">
                        <h6 class="element-header">Paid Orders</h6>
                        <div class="element-box-tp">
                           <div class="table-responsive">
                              <table class="table table-padded">
                                 <thead>
                                    <tr>
                                       <th>Name</th>

                                       <th>Amount</th>
                                        <th>Payment Method</th>

                                       <th class="text-center">Status</th>
                                       <th>Country</th>
                                     
                                       <th>Email</th>
                                        <th>Date</th>
                                        <th>Action</th>
                                      
                                       <th>Package</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                 @foreach ($listings as $listing)
                                    @if ($listing->matched())
                                    <tr>
                                      
                                       <td class="cell-with-media">{{$listing->id}} <a href="{{route('profile.index', ['email'=>$listing->user->email])}}"><img alt="" src="/uploads/avatars/{{ $listing->user->avatar }}" style="height: 25px;"><span>{{ $listing->user->name }} {{ $listing->user->surname }}</span></a></td>
                                       <td><span>{{ $listing->user->area->unit}}{{$listing->amount}}</span><span class="smaller lighter"></span></td>
                                       <td> <img alt="" src="/img/method/{{$listing->paymentmethod}}.png" style="height: 25px;"> {{$listing->paymentmethod}}</td>
                                       <td class="text-center"><a class="badge badge-success" style="background-color: green" href="#">Paid</a></td>
                                          <td>{{ $listing->user->area->parent->name}} <span class="flag-icon flag-icon-{{$listing->user->area->icon}}"></span></td> 
                                      
                                       <td><span>{{$listing->user->email}}</span><span class="smaller lighter"></span></td>
                                       <td><span>{{$listing->updated_at}}</span><span class="smaller lighter"></span></td>
                                       <td class="text-right">
                                                        <div class="actions">
                                                           

                                                            <a href="#" class="btn btn-sm bg-danger-light"
                                        onclick="event.preventDefault(); document.getElementById('listings-destroy-form-{{ $listing->id }}').submit();"
                            data-toggle="tooltip" data-placement="bottom" title="Delete User"><i class="fe fe-trash"></i>Delete</a></li>

                             <form action="#" method="post" id="listings-destroy-form-{{ $listing->id }}">
                                        {{ csrf_field() }}
                                        {{ method_field('DELETE') }}
                                    </form>
                                                        </div>
                                                    </td>
                                       
                                     
                                         <td class="cell-with-media"> <img alt="" src="/assets/images/badges/{{$listing->category->parent->icon}}.png" style="height: 25px;"><span>{{ $listing->category->name }} </span></td>

                                    </tr>
                                        @foreach ($listing->comments as $comment)
                                           <tr>
                                                <td>Withdrawal</td>
                                                <td>{{ $listing->user->area->unit}}{{$comment->split}}</td>
                                               
                                               
                                                <td>{{$comment->created_at}}</td>
                                                <td>@if ($comment->approvals->count())
                                                  <button type="submit" class="btn btn-success">Paid</button>
                                                @else
                                                      <button type="submit" class="btn btn-warning">Not Paid</button>
                                                @endif

                                            </td>
                                            <td>
                                              <a href="#" class="btn btn-sm bg-danger-light"
                                        onclick="event.preventDefault(); document.getElementById('comment-destroy-form-{{ $comment->id }}').submit();"
                            data-toggle="tooltip" data-placement="bottom" title="Reallocate Coins"><i class="fe fe-trash"></i>Reallocate</a></li>

                             <form action="{{route('admin.comment.destroy', [$comment->id])}}" method="post" id="comment-destroy-form-{{ $comment->id }}">
                                        {{ csrf_field() }}
                                        {{ method_field('DELETE') }}
                                    </form>


                                            </td>
                                             </tr>
                                           @endforeach
                                        @else



                
                                   @endif

                                        @endforeach

                                 </tbody>
                              </table>
                           </div>
                        </div>
                     </div>
                     
                  </div>
@endsection

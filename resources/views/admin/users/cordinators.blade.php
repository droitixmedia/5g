@extends('layouts.app')

@section('title', 'Dashboard')

@section('description')

@endsection

@section('content')

<div class="content-box">
                     
                    
                     <div class="element-wrapper">
                        <h6 class="element-header">Cordinators</h6>
                        <div class="element-box-tp">
                           <div class="table-responsive">
                              <table class="table table-padded">
                                 <thead>
                                    <tr>
                                       <th>Name</th>

                                       <th>Email</th>

                                       <th class="text-center">Level</th>
                                       
                                       <th>Country</th>
                                       <th>Contact</th>
                                        <th>Action</th>
                                      
                                       <th>Current Package</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    @foreach ($cordinators as $cordinator)
                                    <tr>
                                      
                                       <td class="cell-with-media">{{$cordinator->id}} <a href="{{route('profile.index', ['email'=>$cordinator->email])}}"><img alt="" src="/uploads/avatars/{{ $cordinator->avatar }}" style="height: 25px;"><span>{{ $cordinator->name }} {{ $cordinator->surname }}</span></a></td>
                                       <td><span>{{$cordinator->email}}</span><span class="smaller lighter"></span></td>
                                       <td class="text-center"><a class="badge badge-success" style="background-color: {{$cordinator->level->color}}" href="#">{{$cordinator->level->name}}</a></td>
                                         <td>{{$cordinator->area->parent->name}} <span class="flag-icon flag-icon-{{$cordinator->area->icon}}"></span></td>  
                                      
                                       <td><span>{{$cordinator->phone_number}}</span><span class="smaller lighter"></span></td>
                                       
                                       <td class="text-right">
                                                        <div class="actions">
                                                           

                                                            <a href="#" class="btn btn-sm bg-danger-light"
                                        onclick="event.preventDefault(); document.getElementById('listings-destroy-form-{{ $cordinator->id }}').submit();"
                            data-toggle="tooltip" data-placement="bottom" title="Delete User"><i class="fe fe-trash"></i>Delete</a></li>

                             <form action="{{route('admin.user.destroy', [$cordinator->id])}}" method="post" id="listings-destroy-form-{{ $cordinator->id }}">
                                        {{ csrf_field() }}
                                        {{ method_field('DELETE') }}
                                    </form>
                                                        </div>
                                                    </td>
                                       
                                       @foreach($cordinator->listings as $listing)
                                         @if($listing->type())
                                         @if($listing->matched())
                                         <td class="cell-with-media"> <img alt="" src="/assets/images/badges/{{$listing->category->parent->icon}}.png" style="height: 25px;"><span>{{ $listing->category->name }} </span></td>

                                         @endif

                                         @endif
                                          
                                       
                                 
                                         @endforeach
                                    </tr>
                                    @endforeach
                                 </tbody>
                              </table>
                           </div>
                        </div>
                     </div>
                     
                  </div>
@endsection

@extends('layouts.app')

@section('title', 'Dashboard')

@section('description')

@endsection

@section('content')

<div class="content-box">
                     
                    
                     <div class="element-wrapper">
                        <div class="element-box">
                           <h5 class="form-header">Admin impersonate Account</h5>
                           <div class="form-desc">Enter Account </div>
                     
                  <div class="row">
                        <div class="col-sm-12">
                           <div class="element-wrapper compact pt-4">
                               <div class="col-lg-6 col-xxl-7">
                           <div class="element-wrapper">
                              <div class="element-box">
                                    <form action="{{ route('admin.impersonate') }}" method="POST">
                            <div class="form-group">
                                <label for="exampleInputPassword1">Email Address</label>
                                <input type="email" class="form-control" name="email" id="email">
                            </div>

                            <button type="submit" class="btn btn-warning">Impersonate</button>
                            {{ csrf_field() }}
                        </form>
                              </div>
                           </div>
                        </div>
                           </div>
                           <div class="row">
                              <div class="col-12 col-xxl-8">
                                 <div class="element-wrapper compact pt-4">
                                    <div class="element-actions d-none d-sm-block">
                                      
                                    </div>
                                   
                                 </div>
                              </div>
                              
                           </div>
                        </div>
                       
                     </div>


                           </div>
                        </div>
                     </div>
                     
                              </div>
                           </div>
                        </div>
                     </div>
                    
                  </div>
@endsection

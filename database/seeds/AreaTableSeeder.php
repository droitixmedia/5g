<?php

use Illuminate\Database\Seeder;

class AreaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $areas = [
            [

             'name' => 'Afghanistan',
                'children' => [
                    [
                        'name' => 'af',
                        'icon' => 'af',
                        'unit' => '$',
                        'folder' => '1',
                        'pin' => '1',
                        'code' => '93',
                        'conversion' => '1',
                        'minimum' => '20',
                       
                       
                       
                    ],
                    
                ],

           

            ],
             [

             'name' => 'Albania',
                'children' => [
                    [
                        'name' => 'al',
                        'icon' => 'al',
                        'unit' => '$',
                        'folder' => '1',
                        'pin' => '1',
                        'code' => '355',
                        'conversion' => '1',
                        'minimum' => '20',
                       
                       
                       
                    ],
                    
                ],

           

            ],
             [

             'name' => 'Algeria',
                'children' => [
                    [
                        'name' => 'dz',
                        'icon' => 'dz',
                        'unit' => '$',
                        'folder' => '1',
                        'pin' => '1',
                        'code' => '213',
                        'conversion' => '1',
                        'minimum' => '20',
                       
                       
                       
                    ],
                    
                ],

           

            ],
             [

             'name' => 'American Samoa',
                'children' => [
                    [
                        'name' => 'as',
                        'icon' => 'as',
                        'unit' => '$',
                        'folder' => '1',
                        'pin' => '1',
                        'code' => '1',
                        'conversion' => '1',
                        'minimum' => '20',
                       
                       
                       
                    ],
                    
                ],

           

            ],
             [

             'name' => 'Andorra',
                'children' => [
                    [
                        'name' => 'ad',
                        'icon' => 'ad',
                        'unit' => '$',
                        'folder' => '1',
                        'pin' => '1',
                        'code' => '376',
                        'conversion' => '1',
                        'minimum' => '20',
                       
                       
                       
                    ],
                    
                ],

           

            ],
             [

             'name' => 'Angola',
                'children' => [
                    [
                        'name' => 'ao',
                        'icon' => 'ao',
                        'unit' => '$',
                        'folder' => '1',
                        'pin' => '1',
                        'code' => '244',
                        'conversion' => '1',
                        'minimum' => '20',
                       
                       
                       
                    ],
                    
                ],

           

            ],
             [

             'name' => 'Anguilla',
                'children' => [
                    [
                        'name' => 'al',
                        'icon' => 'al',
                        'unit' => '$',
                        'folder' => '1',
                        'pin' => '1',
                        'code' => '1',
                        'conversion' => '1',
                        'minimum' => '20',
                       
                       
                       
                    ],
                    
                ],

           

            ],
             [

             'name' => 'Argentina',
                'children' => [
                    [
                        'name' => 'ar',
                        'icon' => 'ar',
                        'unit' => '$',
                        'folder' => '1',
                        'pin' => '1',
                        'code' => '54',
                        'conversion' => '1',
                        'minimum' => '20',
                       
                       
                       
                    ],
                    
                ],

           

            ],
             [

             'name' => 'Armenia',
                'children' => [
                    [
                        'name' => 'am',
                        'icon' => 'am',
                        'unit' => '$',
                        'folder' => '1',
                        'pin' => '1',
                        'code' => '374',
                        'conversion' => '1',
                        'minimum' => '20',
                       
                       
                       
                    ],
                    
                ],

           

            ],
             [

             'name' => 'Aruba',
                'children' => [
                    [
                        'name' => 'aw',
                        'icon' => 'aw',
                        'unit' => '$',
                        'folder' => '1',
                        'pin' => '1',
                        'code' => '297',
                        'conversion' => '1',
                        'minimum' => '20',
                       
                       
                       
                    ],
                    
                ],

           

            ],
             [

             'name' => 'Australia',
                'children' => [
                    [
                        'name' => 'au',
                        'icon' => 'au',
                        'unit' => '$',
                        'folder' => '1',
                        'pin' => '1',
                        'code' => '61',
                        'conversion' => '1',
                        'minimum' => '20',
                       
                       
                       
                    ],
                    
                ],

           

            ],
             [

             'name' => 'Austria',
                'children' => [
                    [
                        'name' => 'at',
                        'icon' => 'at',
                        'unit' => '$',
                        'folder' => '1',
                        'pin' => '1',
                        'code' => '43',
                        'conversion' => '1',
                        'minimum' => '20',
                       
                       
                       
                    ],
                    
                ],

           

            ],
             [

             'name' => 'Bahamas',
                'children' => [
                    [
                        'name' => 'bs',
                        'icon' => 'bs',
                        'unit' => '$',
                        'folder' => '1',
                        'pin' => '1',
                        'code' => '1',
                        'conversion' => '1',
                        'minimum' => '20',
                       
                       
                       
                    ],
                    
                ],

           

            ],
             [

             'name' => 'Bangladesh',
                'children' => [
                    [
                        'name' => 'bd',
                        'icon' => 'bd',
                        'unit' => '$',
                        'folder' => '1',
                        'pin' => '1',
                        'code' => '880',
                        'conversion' => '1',
                        'minimum' => '20',
                       
                       
                       
                    ],
                    
                ],

           

            ],
             [

             'name' => 'Barbados',
                'children' => [
                    [
                        'name' => 'bb',
                        'icon' => 'bb',
                        'unit' => '$',
                        'folder' => '1',
                        'pin' => '1',
                        'code' => '1',
                        'conversion' => '1',
                        'minimum' => '20',
                       
                       
                       
                    ],
                    
                ],

           

            ],
             [

             'name' => 'Benin',
                'children' => [
                    [
                        'name' => 'bj',
                        'icon' => 'bj',
                        'unit' => '$',
                        'folder' => '1',
                        'pin' => '1',
                        'code' => '229',
                        'conversion' => '1',
                        'minimum' => '20',
                       
                       
                       
                    ],
                    
                ],

           

            ],
             [

             'name' => 'Botswana',
                'children' => [
                    [
                        'name' => 'bw',
                        'icon' => 'bw',
                        'unit' => 'P',
                        'folder' => '1',
                        'pin' => '2',
                        'code' => '267',
                        'conversion' => '1',
                        'minimum' => '500',
                       
                       
                       
                    ],
                    
                ],

           

            ],
             [

             'name' => 'Brazil',
                'children' => [
                    [
                        'name' => 'br',
                        'icon' => 'br',
                        'unit' => '$',
                        'folder' => '1',
                        'pin' => '1',
                        'code' => '55',
                        'conversion' => '1',
                        'minimum' => '20',
                       
                       
                       
                    ],
                    
                ],

           

            ],
             [

             'name' => 'Bulgaria',
                'children' => [
                    [
                        'name' => 'bg',
                        'icon' => 'bg',
                        'unit' => '$',
                        'folder' => '1',
                        'pin' => '1',
                        'code' => '359',
                        'conversion' => '1',
                        'minimum' => '20',
                       
                       
                       
                    ],
                    
                ],

           

            ],
             [

             'name' => 'Burkina Faso',
                'children' => [
                    [
                        'name' => 'bf',
                        'icon' => 'bf',
                        'unit' => '$',
                        'folder' => '1',
                        'pin' => '1',
                        'code' => '226',
                        'conversion' => '1',
                        'minimum' => '20',
                       
                       
                       
                    ],
                    
                ],

           

            ],
             [

             'name' => 'Burundi',
                'children' => [
                    [
                        'name' => 'bi',
                        'icon' => 'bi',
                        'unit' => '$',
                        'folder' => '1',
                        'pin' => '1',
                        'code' => '257',
                        'conversion' => '1',
                        'minimum' => '20',
                       
                       
                       
                    ],
                    
                ],

           

            ],
             [

             'name' => 'Cambodia',
                'children' => [
                    [
                        'name' => 'kh',
                        'icon' => 'kh',
                        'unit' => '$',
                        'folder' => '1',
                        'pin' => '1',
                        'code' => '855',
                        'conversion' => '1',
                        'minimum' => '20',
                       
                       
                       
                    ],
                    
                ],

           

            ],
             [

             'name' => 'Cameroon',
                'children' => [
                    [
                        'name' => 'cm',
                        'icon' => 'cm',
                        'unit' => '$',
                        'folder' => '1',
                        'pin' => '1',
                        'code' => '237',
                        'conversion' => '1',
                        'minimum' => '20',
                       
                       
                       
                    ],
                    
                ],

           

            ],
             [

             'name' => 'Canada',
                'children' => [
                    [
                        'name' => 'ca',
                        'icon' => 'ca',
                        'unit' => '$',
                        'folder' => '1',
                        'pin' => '1',
                        'code' => '1',
                        'conversion' => '1',
                        'minimum' => '20',
                       
                       
                       
                    ],
                    
                ],

           

            ],
             [

             'name' => 'Chad',
                'children' => [
                    [
                        'name' => 'td',
                        'icon' => 'td',
                        'unit' => '$',
                        'folder' => '1',
                        'pin' => '1',
                        'code' => '235',
                        'conversion' => '1',
                        'minimum' => '20',
                       
                       
                       
                    ],
                    
                ],

           

            ],
             [

             'name' => 'Congo',
                'children' => [
                    [
                        'name' => 'cd',
                        'icon' => 'cd',
                        'unit' => '$',
                        'folder' => '1',
                        'pin' => '1',
                        'code' => '243',
                        'conversion' => '1',
                        'minimum' => '20',
                       
                       
                       
                    ],
                    
                ],

           

            ],
             [

             'name' => 'Cyprus',
                'children' => [
                    [
                        'name' => 'cy',
                        'icon' => 'cy',
                        'unit' => '$',
                        'folder' => '1',
                        'pin' => '1',
                        'code' => '357',
                        'conversion' => '1',
                        'minimum' => '20',
                       
                       
                       
                    ],
                    
                ],

           

            ],
             [

             'name' => 'Ivory Coast',
                'children' => [
                    [
                        'name' => 'ci',
                        'icon' => 'ci',
                        'unit' => '$',
                        'folder' => '1',
                        'pin' => '1',
                        'code' => '225',
                        'conversion' => '1',
                        'minimum' => '20',
                       
                       
                       
                    ],
                    
                ],

           

            ],
             [

             'name' => 'Denmark',
                'children' => [
                    [
                        'name' => 'dk',
                        'icon' => 'dk',
                        'unit' => '$',
                        'folder' => '1',
                        'pin' => '1',
                        'code' => '45',
                        'conversion' => '1',
                        'minimum' => '20',
                       
                       
                       
                    ],
                    
                ],

           

            ],
             [

             'name' => 'Egypt',
                'children' => [
                    [
                        'name' => 'eg',
                        'icon' => 'eg',
                        'unit' => '$',
                        'folder' => '1',
                        'pin' => '1',
                        'code' => '20',
                        'conversion' => '1',
                        'minimum' => '20',
                       
                       
                       
                    ],
                    
                ],

           

            ],
             [

             'name' => 'Equatorial Guinea',
                'children' => [
                    [
                        'name' => 'gq',
                        'icon' => 'gq',
                        'unit' => '$',
                        'folder' => '1',
                        'pin' => '1',
                        'code' => '240',
                        'conversion' => '1',
                        'minimum' => '20',
                       
                       
                       
                    ],
                    
                ],

           

            ],
             [

             'name' => 'Eritrea',
                'children' => [
                    [
                        'name' => 'er',
                        'icon' => 'er',
                        'unit' => '$',
                        'folder' => '1',
                        'pin' => '1',
                        'code' => '291',
                        'conversion' => '1',
                        'minimum' => '20',
                       
                       
                       
                    ],
                    
                ],

           

            ],
             [

             'name' => 'Eswatini',
                'children' => [
                    [
                        'name' => 'sz',
                        'icon' => 'sz',
                        'unit' => 'R',
                        'folder' => '1',
                        'pin' => '3',
                        'code' => '268',
                        'conversion' => '1',
                        'minimum' => '20',
                       
                       
                       
                    ],
                    
                ],

           

            ],
             [

             'name' => 'Ethiopia',
                'children' => [
                    [
                        'name' => 'et',
                        'icon' => 'et',
                        'unit' => '$',
                        'folder' => '1',
                        'pin' => '1',
                        'code' => '251',
                        'conversion' => '1',
                        'minimum' => '20',
                       
                       
                       
                    ],
                    
                ],

           

            ],
             [

             'name' => 'Finland',
                'children' => [
                    [
                        'name' => 'fi',
                        'icon' => 'fi',
                        'unit' => '$',
                        'folder' => '1',
                        'pin' => '1',
                        'code' => '358',
                        'conversion' => '1',
                        'minimum' => '20',
                       
                       
                       
                    ],
                    
                ],

           

            ],
                    [

             'name' => 'France',
                'children' => [
                    [
                        'name' => 'fr',
                        'icon' => 'fr',
                        'unit' => '$',
                        'folder' => '1',
                        'pin' => '1',
                        'code' => '33',
                        'conversion' => '1',
                        'minimum' => '20',
                       
                       
                       
                    ],
                    
                ],

           

            ],
                    [

             'name' => 'Gabon',
                'children' => [
                    [
                        'name' => 'ga',
                        'icon' => 'ga',
                        'unit' => '$',
                        'folder' => '1',
                        'pin' => '1',
                        'code' => '241',
                        'conversion' => '1',
                        'minimum' => '20',
                       
                       
                       
                    ],
                    
                ],

           

            ],
                    [

             'name' => 'Gambia',
                'children' => [
                    [
                        'name' => 'gm',
                        'icon' => 'gm',
                        'unit' => '$',
                        'folder' => '1',
                        'pin' => '1',
                        'code' => '220',
                        'conversion' => '1',
                        'minimum' => '20',
                       
                       
                       
                    ],
                    
                ],

           

            ],
                    [

             'name' => 'Germany',
                'children' => [
                    [
                        'name' => 'de',
                        'icon' => 'de',
                        'unit' => '$',
                        'folder' => '1',
                        'pin' => '1',
                        'code' => '49',
                        'conversion' => '1',
                        'minimum' => '20',
                       
                       
                       
                    ],
                    
                ],

           

            ],
                    [

             'name' => 'Ghana',
                'children' => [
                    [
                        'name' => 'gh',
                        'icon' => 'gh',
                        'unit' => '$',
                        'folder' => '1',
                        'pin' => '1',
                        'code' => '233',
                        'conversion' => '1',
                        'minimum' => '20',
                       
                       
                       
                    ],
                    
                ],

           

            ],
                    [

             'name' => 'Guinea',
                'children' => [
                    [
                        'name' => 'gn',
                        'icon' => 'gn',
                        'unit' => '$',
                        'folder' => '1',
                        'pin' => '1',
                        'code' => '224',
                        'conversion' => '1',
                        'minimum' => '20',
                       
                       
                       
                    ],
                    
                ],

           

            ],
                    [

             'name' => 'India',
                'children' => [
                    [
                        'name' => 'in',
                        'icon' => 'in',
                        'unit' => '$',
                        'folder' => '1',
                        'pin' => '1',
                        'code' => '91',
                        'conversion' => '1',
                        'minimum' => '20',
                       
                       
                       
                    ],
                    
                ],

           

            ],
                    [

             'name' => 'Indonesia',
                'children' => [
                    [
                        'name' => 'id',
                        'icon' => 'id',
                        'unit' => '$',
                        'folder' => '1',
                        'pin' => '1',
                        'code' => '62',
                        'conversion' => '1',
                        'minimum' => '20',
                       
                       
                       
                    ],
                    
                ],

           

            ],
                    [

             'name' => 'Kenya',
                'children' => [
                    [
                        'name' => 'ke',
                        'icon' => 'ke',
                        'unit' => 'Ksh',
                        'folder' => '1',
                        'pin' => '1',
                        'code' => '254',
                        'conversion' => '1',
                        'minimum' => '20',
                       
                       
                       
                    ],
                    
                ],

           

            ],
                    [

             'name' => 'Kuwait',
                'children' => [
                    [
                        'name' => 'kw',
                        'icon' => 'kw',
                        'unit' => '$',
                        'folder' => '1',
                        'pin' => '1',
                        'code' => '965',
                        'conversion' => '1',
                        'minimum' => '20',
                       
                       
                       
                    ],
                    
                ],

           

            ],
                    [

             'name' => 'Lesotho',
                'children' => [
                    [
                        'name' => 'ls',
                        'icon' => 'ls',
                        'unit' => 'R',
                        'folder' => '1',
                        'pin' => '1',
                        'code' => '266',
                        'conversion' => '1',
                        'minimum' => '20',
                       
                       
                       
                    ],
                    
                ],

           

            ],
                    [

             'name' => 'Liberia',
                'children' => [
                    [
                        'name' => 'ly',
                        'icon' => 'ly',
                        'unit' => '$',
                        'folder' => '1',
                        'pin' => '1',
                        'code' => '231',
                        'conversion' => '1',
                        'minimum' => '20',
                       
                       
                       
                    ],
                    
                ],

           

            ],
                    [

             'name' => 'Libya',
                'children' => [
                    [
                        'name' => 'ly',
                        'icon' => 'ly',
                        'unit' => '$',
                        'folder' => '1',
                        'pin' => '1',
                        'code' => '218',
                        'conversion' => '1',
                        'minimum' => '20',
                       
                       
                       
                    ],
                    
                ],

           

            ],
                    [

             'name' => 'Madagascar',
                'children' => [
                    [
                        'name' => 'mg',
                        'icon' => 'mg',
                        'unit' => '$',
                        'folder' => '1',
                        'pin' => '1',
                        'code' => '261',
                        'conversion' => '1',
                        'minimum' => '20',
                       
                       
                       
                    ],
                    
                ],

           

            ],
                    [

             'name' => 'Malawi',
                'children' => [
                    [
                        'name' => 'mw',
                        'icon' => 'mw',
                        'unit' => '$',
                        'folder' => '1',
                        'pin' => '1',
                        'code' => '265',
                        'conversion' => '1',
                        'minimum' => '20',
                       
                       
                       
                    ],
                    
                ],

           

            ],
                    [

             'name' => 'Malaysia',
                'children' => [
                    [
                        'name' => 'my',
                        'icon' => 'my',
                        'unit' => '$',
                        'folder' => '1',
                        'pin' => '1',
                        'code' => '60',
                        'conversion' => '1',
                        'minimum' => '20',
                       
                       
                       
                    ],
                    
                ],

           

            ],
                    [

             'name' => 'Mali',
                'children' => [
                    [
                        'name' => 'ml',
                        'icon' => 'ml',
                        'unit' => '$',
                        'folder' => '1',
                        'pin' => '1',
                        'code' => '223',
                        'conversion' => '1',
                        'minimum' => '20',
                       
                       
                       
                    ],
                    
                ],

           

            ],
                    [

             'name' => 'Morocco',
                'children' => [
                    [
                        'name' => 'ma',
                        'icon' => 'ma',
                        'unit' => '$',
                        'folder' => '1',
                        'pin' => '1',
                        'code' => '212',
                        'conversion' => '1',
                        'minimum' => '20',
                       
                       
                       
                    ],
                    
                ],

           

            ],
                    [

             'name' => 'Mozambique',
                'children' => [
                    [
                        'name' => 'mz',
                        'icon' => 'mz',
                        'unit' => '$',
                        'folder' => '1',
                        'pin' => '1',
                        'code' => '258',
                        'conversion' => '1',
                        'minimum' => '20',
                       
                       
                       
                    ],
                    
                ],

           

            ],
                    [

             'name' => 'Namibia',
                'children' => [
                    [
                        'name' => 'fi',
                        'icon' => 'fi',
                        'unit' => 'N$',
                        'folder' => '1',
                        'pin' => '1',
                        'code' => '264',
                        'conversion' => '1',
                        'minimum' => '20',
                       
                       
                       
                    ],
                    
                ],

           

            ],
                    [

             'name' => 'Niger',
                'children' => [
                    [
                        'name' => 'ne',
                        'icon' => 'ne',
                        'unit' => '$',
                        'folder' => '1',
                        'pin' => '1',
                        'code' => '227',
                        'conversion' => '1',
                        'minimum' => '20',
                       
                       
                       
                    ],
                    
                ],

           

            ],
                    [

             'name' => 'Nigeria',
                'children' => [
                    [
                        'name' => 'ng',
                        'icon' => 'ng',
                        'unit' => '$',
                        'folder' => '1',
                        'pin' => '1',
                        'code' => '234',
                        'conversion' => '1',
                        'minimum' => '20',
                       
                       
                       
                    ],
                    
                ],

           

            ],
                    [

             'name' => 'Pakistan',
                'children' => [
                    [
                        'name' => 'pk',
                        'icon' => 'pk',
                        'unit' => '$',
                        'folder' => '1',
                        'pin' => '1',
                        'code' => '92',
                        'conversion' => '1',
                        'minimum' => '20',
                       
                       
                       
                    ],
                    
                ],

           

            ],
                    [

             'name' => 'Puerto Rico',
                'children' => [
                    [
                        'name' => 'pr',
                        'icon' => 'pr',
                        'unit' => '$',
                        'folder' => '1',
                        'pin' => '1',
                        'code' => '1',
                        'conversion' => '1',
                        'minimum' => '20',
                       
                       
                       
                    ],
                    
                ],

           

            ],
                    [

             'name' => 'Qatar',
                'children' => [
                    [
                        'name' => 'qa',
                        'icon' => 'qa',
                        'unit' => '$',
                        'folder' => '1',
                        'pin' => '1',
                        'code' => '974',
                        'conversion' => '1',
                        'minimum' => '20',
                       
                       
                       
                    ],
                    
                ],

           

            ],
                    [

             'name' => 'Russia',
                'children' => [
                    [
                        'name' => 'ru',
                        'icon' => 'ru',
                        'unit' => '$',
                        'folder' => '1',
                        'pin' => '1',
                        'code' => '7',
                        'conversion' => '1',
                        'minimum' => '20',
                       
                       
                       
                    ],
                    
                ],

           

            ],
                    [

             'name' => 'Rwanda',
                'children' => [
                    [
                        'name' => 'rw',
                        'icon' => 'rw',
                        'unit' => '$',
                        'folder' => '1',
                        'pin' => '1',
                        'code' => '250',
                        'conversion' => '1',
                        'minimum' => '20',
                       
                       
                       
                    ],
                    
                ],

           

            ],
                    [

             'name' => 'Saudi Arabia',
                'children' => [
                    [
                        'name' => 'sa',
                        'icon' => 'sa',
                        'unit' => '$',
                        'folder' => '1',
                        'pin' => '1',
                        'code' => '966',
                        'conversion' => '1',
                        'minimum' => '20',
                       
                       
                       
                    ],
                    
                ],

           

            ],
                    [

             'name' => 'Senegal',
                'children' => [
                    [
                        'name' => 'sn',
                        'icon' => 'sn',
                        'unit' => '$',
                        'folder' => '1',
                        'pin' => '1',
                        'code' => '221',
                        'conversion' => '1',
                        'minimum' => '20',
                       
                       
                       
                    ],
                    
                ],

           

            ],
                    [

             'name' => 'Sierra Leone',
                'children' => [
                    [
                        'name' => 'sl',
                        'icon' => 'sl',
                        'unit' => '$',
                        'folder' => '1',
                        'pin' => '1',
                        'code' => '232',
                        'conversion' => '1',
                        'minimum' => '20',
                       
                       
                       
                    ],
                    
                ],

           

            ],
                    [

             'name' => 'Singapore',
                'children' => [
                    [
                        'name' => 'sg',
                        'icon' => 'sg',
                        'unit' => '$',
                        'folder' => '1',
                        'pin' => '1',
                        'code' => '65',
                        'conversion' => '1',
                        'minimum' => '20',
                       
                       
                       
                    ],
                    
                ],

           

            ],
                    [

             'name' => 'Somalia',
                'children' => [
                    [
                        'name' => 'so',
                        'icon' => 'so',
                        'unit' => '$',
                        'folder' => '1',
                        'pin' => '1',
                        'code' => '252',
                        'conversion' => '1',
                        'minimum' => '20',
                       
                       
                       
                    ],
                    
                ],

           

            ],
            

            [

            'name' => 'South Africa',
                'children' => [
                    [
                        'name' => 'za',
                        'icon' => 'za',
                        'unit' => 'R',
                        'folder' => '1',
                        'conversion' => '0.063',
                        'pin' => '4',
                        'code' => '27',
                        'minimum' => '500',
                      
                       
                       
                    ],
                    
                ],

           

            ],
                      [

             'name' => 'South Sudan',
                'children' => [
                    [
                        'name' => 'ss',
                        'icon' => 'ss',
                        'unit' => '$',
                        'folder' => '1',
                        'pin' => '1',
                        'code' => '211',
                        'conversion' => '1',
                        'minimum' => '20',
                       
                       
                       
                    ],
                    
                ],

           

            ],
                      [

             'name' => 'Spain',
                'children' => [
                    [
                        'name' => 'es',
                        'icon' => 'es',
                        'unit' => '$',
                        'folder' => '1',
                        'pin' => '1',
                        'code' => '34',
                        'conversion' => '1',
                        'minimum' => '20',
                       
                       
                       
                    ],
                    
                ],

           

            ],
                      [

             'name' => 'Sri Lanka',
                'children' => [
                    [
                        'name' => 'lk',
                        'icon' => 'lk',
                        'unit' => '$',
                        'folder' => '1',
                        'pin' => '1',
                        'code' => '94',
                        'conversion' => '1',
                        'minimum' => '20',
                       
                       
                       
                    ],
                    
                ],

           

            ],
                      [

             'name' => 'Sudan',
                'children' => [
                    [
                        'name' => 'sd',
                        'icon' => 'sd',
                        'unit' => '$',
                        'folder' => '1',
                        'pin' => '1',
                        'code' => '249',
                        'conversion' => '1',
                        'minimum' => '20',
                       
                       
                       
                    ],
                    
                ],

           

            ],
                      [

             'name' => 'Tanzania',
                'children' => [
                    [
                        'name' => 'tz',
                        'icon' => 'tz',
                        'unit' => '$',
                        'folder' => '1',
                        'pin' => '1',
                        'code' => '255',
                        'conversion' => '1',
                        'minimum' => '20',
                       
                       
                       
                    ],
                    
                ],

           

            ],
                      [

             'name' => 'Thailand',
                'children' => [
                    [
                        'name' => 'th',
                        'icon' => 'th',
                        'unit' => '$',
                        'folder' => '1',
                        'pin' => '1',
                        'code' => '66',
                        'conversion' => '1',
                        'minimum' => '20',
                       
                       
                       
                    ],
                    
                ],

           

            ],
                      [

             'name' => 'Togo',
                'children' => [
                    [
                        'name' => 'tg',
                        'icon' => 'tg',
                        'unit' => '$',
                        'folder' => '1',
                        'pin' => '1',
                        'code' => '228',
                        'conversion' => '1',
                        'minimum' => '20',
                       
                       
                       
                    ],
                    
                ],

           

            ],
                      [

             'name' => 'Uganda',
                'children' => [
                    [
                        'name' => 'ug',
                        'icon' => 'ug',
                        'unit' => '$',
                        'folder' => '1',
                        'pin' => '1',
                        'code' => '256',
                        'conversion' => '1',
                        'minimum' => '20',
                       
                       
                       
                    ],
                    
                ],

            ],
                          [

             'name' => 'Ukraine',
                'children' => [
                    [
                        'name' => 'ua',
                        'icon' => 'ua',
                        'unit' => '$',
                        'folder' => '1',
                        'pin' => '1',
                        'code' => '380',
                        'conversion' => '1',
                        'minimum' => '20',
                       
                       
                       
                    ],
                    
                ],

           

            ],
                      [

             'name' => 'United Arab Emirates',
                'children' => [
                    [
                        'name' => 'ae',
                        'icon' => 'ae',
                        'unit' => '$',
                        'folder' => '1',
                        'pin' => '1',
                        'code' => '971',
                        'conversion' => '1',
                        'minimum' => '20',
                       
                       
                       
                    ],
                    
                ],

           

            ],
                      [

             'name' => 'United Kingdom',
                'children' => [
                    [
                        'name' => 'gb',
                        'icon' => 'gb',
                        'unit' => '$',
                        'folder' => '1',
                        'pin' => '1',
                        'code' => '44',
                        'conversion' => '1',
                        'minimum' => '20',
                       
                       
                       
                    ],
                    
                ],

           

            ],

                    [

             'name' => 'United States',
                'children' => [
                    [
                        'name' => 'us',
                        'icon' => 'us',
                        'unit' => '$',
                        'folder' => '1',
                        'pin' => '1',
                        'code' => '1',
                        'conversion' => '1',
                        'minimum' => '20',
                       
                       
                       
                    ],
                    
                ],

           

            ], 

          

               [

             'name' => 'Zambia',
                'children' => [
                    [
                        'name' => 'zm',
                        'icon' => 'zm',
                        'unit' => 'K',
                        'folder' => '2',
                        'pin' => '5',
                        'code' => '260',
                        'conversion' => '1',
                        'minimum' => '300',
                       
                       
                       
                    ],
                    
                ],

           

            ],


               [

            
             'name' => 'Zimbabwe',
                'children' => [
                    [
                        'name' => 'zw',
                        'icon' => 'zw',
                        'unit' => '$',
                        'folder' => '2',
                        'pin' => '6',
                        'code' => '263',
                        'conversion' => '1',
                        'minimum' => '20',
                        
                       
                    ],
                    
                ],

           

            ],  


        
        

        ];

        foreach ($areas as $area) {
            \openjobs\Area::create($area);
        }
    }
}

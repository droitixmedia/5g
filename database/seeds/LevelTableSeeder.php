<?php

use Illuminate\Database\Seeder;

class LevelTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $levels = [
            [

                'name' => 'Member',
                'badge' => 'member',
                'banner' => 'member',
                'color' => 'white',
                'folder' => 'member',
                'pay' => '0',
                'bonus_percent' => '0.06',
                
            ],

            [

                'name' => 'Agent',
                'badge' => 'agent',
                'banner' => 'agent',
                'color' => 'green',
                'folder' => 'agent',
                'pay' => '50',
                'bonus_percent' => '0.10',
               
            ],
             [
                'name' => 'Supervisor',
                'badge' => 'supervisor',
                'banner' => 'supervisor',
                'color' => 'lightgreen',
                'folder' => 'supervisor',
                'pay' => '150',
                'bonus_percent' => '0.13',
            ],
            [
                'name' => 'Cordinator',
                'badge' => 'cordinator',
                'banner' => 'cordinator',
                'color' => 'black',
                'folder' => 'cordinator',
                'pay' => '250',
                'bonus_percent' => '0.15',
                
            ],
            [
                'name' => 'Manager',
                'badge' => 'manager',
                'banner' => 'manager',
                'color' => 'blue',
                'folder' => 'manager',
                'pay' => '350',
                'bonus_percent' => '0.18',
                
            ],
             [
                'name' => 'Ambassador',
                'badge' => 'ambassador',
                'banner' => 'ambassador',
                'color' => 'gold',
                'folder' => 'ambassador',
                'pay' => '500',
                'bonus_percent' => '0.20',
                
            ],
            
      
            
          


        ];

        foreach ($levels as $level) {
            \openjobs\Level::create($level);
        }
    }
}

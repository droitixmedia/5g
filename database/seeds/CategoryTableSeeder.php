<?php

use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [
            [

                'name' => 'Starter Boost Package',
                'percent' => '0.017',
                'color' => '3',
                'bonusplus' => '1',
                'icon' => 'starter',
                'children' => [

                    ['name' => 'Starter Boost Package'],


                     ]
            ],
            [

                'name' => 'Bronze Boost Package',
                'percent' => '0.02',
                'color' => '3',
                'bonusplus' => '2',
                'icon' => 'bronze',
                'children' => [

                    ['name' => 'Bronze Boost Package'],


                     ]
            ],
             [

                'name' => 'Silver Boost Package',
                'percent' => '0.025',
                'color' => '3',
                'bonusplus' => '3',
                'icon' => 'silver',
                'children' => [

                    ['name' => 'Silver Boost Package'],


                     ]
            ],
            [

                'name' => 'Gold Boost Package',
                'percent' => '0.03',
                'color' => '3',
                'bonusplus' => '5',
                'icon' => 'gold',
                'children' => [

                    ['name' => 'Gold Boost Package'],


                     ]
            ],
            [

                'name' => 'Black Card',
                'percent' => '0.033',
                'color' => '3',
                'bonusplus' => '6',
                'icon' => 'black',
                'children' => [

                    ['name' => 'Black Card'],


                     ]
            ],
            [

                'name' => '5G Ultimate',
                'percent' => '0.041',
                'color' => '3',
                'bonusplus' => '7',
                'icon' => '5g',
                'children' => [

                    ['name' => '5G Ultimate'],


                     ]
            ],
            
          


        ];

        foreach ($categories as $category) {
            \openjobs\Category::create($category);
        }
    }
}

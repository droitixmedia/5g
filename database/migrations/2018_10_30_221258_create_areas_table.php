<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Kalnoy\Nestedset\NestedSet;

class CreateAreasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('areas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('code')->nullable();
            $table->string('unit')->nullable();
            $table->string('unitname')->nullable();
            $table->string('minimum')->nullable();
            $table->string('conversion')->nullable();
            $table->string('folder')->nullable();
            $table->string('payment1')->nullable();
            $table->string('payment2')->nullable();
            $table->string('bank1')->nullable();
            $table->string('bank2')->nullable();
            $table->string('bank3')->nullable();
            $table->string('bank4')->nullable();
            $table->string('acc1')->nullable();
            $table->string('acc2')->nullable();
            $table->string('acc3')->nullable();
            $table->string('acc4')->nullable();
            $table->string('skrill')->nullable();
            $table->string('bitcoin')->nullable();
            $table->string('paypal')->nullable();
            $table->string('slug')->unique();
            NestedSet::columns($table);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('areas');
    }
}
